<?php

use App\Main\Http\RequestManagement\RequestDispatcher;
use App\Main\Libraries\Log;

require_once "../App/init.php";

Log::info("Dispatching Request...");
RequestDispatcher::dispatchRequest();
