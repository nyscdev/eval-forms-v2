-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2019 at 08:38 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nysc_evaluation`
--

-- --------------------------------------------------------

--
-- Table structure for table `answer_options`
--

CREATE TABLE `answer_options` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `answer_option` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `answer_options`
--

INSERT INTO `answer_options` (`id`, `question_id`, `answer_option`) VALUES
(12, 25, 'Rural'),
(13, 25, 'Urban'),
(14, 26, 'Education'),
(15, 26, 'Health'),
(16, 26, 'Agriculture'),
(17, 26, 'Infrastructure'),
(18, 27, 'Public'),
(19, 27, 'Private'),
(20, 28, 'Federal owned'),
(21, 28, 'State owned'),
(22, 28, 'Local government owned'),
(23, 28, 'It wasn\'t public'),
(24, 29, 'Relevant'),
(25, 29, 'Not relevant'),
(26, 30, 'Yes'),
(27, 30, 'No'),
(28, 32, 'Yes'),
(29, 32, 'No'),
(30, 34, 'Yes'),
(31, 34, 'No'),
(32, 35, 'Interesting Nature of Assignment'),
(33, 35, 'Attitude of employer / head of department'),
(34, 35, 'Attitude of colleagues'),
(35, 35, 'My own resourcefulness'),
(36, 37, 'Yes'),
(37, 37, 'No'),
(38, 38, 'Very cordial'),
(39, 38, 'Cordial'),
(40, 38, 'Unfriendly'),
(41, 40, 'Very high'),
(42, 40, 'High'),
(43, 40, 'Low'),
(44, 41, 'Yes'),
(45, 41, 'No'),
(46, 42, 'Yes'),
(47, 42, 'No'),
(48, 43, 'Rented Accommodation'),
(49, 43, 'Stayed with a relation/friend'),
(50, 43, 'Corpers\' Lodge'),
(51, 43, 'Stayed in MCAN/NCCF/NACC lodge'),
(52, 45, 'Very effective'),
(53, 45, 'Effective'),
(54, 45, 'Not Effective'),
(55, 46, 'Very cordial'),
(56, 46, 'Cordial'),
(57, 46, 'Not Cordial'),
(58, 49, 'Yes'),
(59, 49, 'No'),
(60, 51, 'Supportive'),
(61, 51, 'Indifferent'),
(62, 51, 'Not supportive'),
(63, 53, 'Yes'),
(64, 53, 'No'),
(65, 54, 'Yes'),
(66, 54, 'No'),
(67, 57, 'Relevant to Nation Building'),
(68, 57, 'Not Relevant to Nation Building'),
(69, 58, 'Yes'),
(70, 58, 'No'),
(71, 68, 'Yes'),
(72, 68, 'No'),
(73, 70, 'Yes'),
(74, 70, 'No'),
(75, 81, 'Christianity'),
(76, 81, 'Islam'),
(77, 82, 'Male'),
(78, 82, 'Female'),
(81, 86, 'Yes'),
(82, 86, 'No'),
(85, 88, 'Single'),
(86, 88, 'Married'),
(87, 88, 'Divorced'),
(93, 89, 'Yes'),
(94, 89, 'No'),
(95, 90, 'I selected No above'),
(96, 90, 'Marriage'),
(97, 90, 'Work'),
(98, 90, 'Other'),
(99, 87, 'I selected No above'),
(100, 87, 'Place of birth though not an indigene'),
(101, 87, 'School there'),
(102, 91, 'Yes'),
(103, 91, 'No'),
(104, 78, 'First Day of Camp (March 27th)'),
(105, 78, 'Second Day of Camp'),
(106, 78, 'Third Day of Camp'),
(107, 78, 'Before the end of the first week'),
(108, 78, 'After the first week of camp'),
(109, 93, 'Eager'),
(110, 93, 'Indifferent'),
(111, 93, 'Reluctant'),
(112, 94, 'Very high'),
(113, 94, 'High'),
(114, 94, 'Low'),
(115, 94, 'Very Low'),
(116, 95, 'Collected from school'),
(117, 95, 'Printed on-line'),
(118, 96, 'On-line call up'),
(119, 96, 'Call-up from school'),
(120, 97, 'Not stressful'),
(121, 97, 'Stressful'),
(122, 98, '30 mins'),
(123, 98, '1 hour'),
(124, 98, 'More than 30 minutes but not up to 1 hour'),
(125, 99, 'Friendly'),
(126, 99, 'Not friendly'),
(127, 101, 'Yes'),
(128, 101, 'No'),
(129, 103, 'Yes'),
(130, 103, 'No'),
(131, 110, 'Yes'),
(132, 110, 'No'),
(133, 111, 'Very good'),
(134, 111, 'Good'),
(135, 111, 'Poor'),
(136, 112, 'Yes'),
(137, 112, 'No'),
(138, 139, 'Very Organized'),
(139, 139, 'Organized'),
(140, 139, 'Not Organized'),
(141, 139, 'Don\'t Know'),
(142, 140, 'Very Organized'),
(143, 140, 'Organized'),
(144, 140, 'Not Organized'),
(145, 140, 'Don\'t Know'),
(146, 141, 'Very Organized'),
(147, 141, 'Organized'),
(148, 141, 'Not Organized'),
(149, 141, 'Don\'t Know'),
(150, 142, 'Very Organized'),
(151, 142, 'Organized'),
(152, 142, 'Not Organized'),
(153, 142, 'Don\'t Know'),
(154, 143, 'Very Organized'),
(155, 143, 'Organized'),
(156, 143, 'Not Organized'),
(157, 143, 'Don\'t Know'),
(158, 144, 'Very Organized'),
(159, 144, 'Organized'),
(160, 144, 'Not Organized'),
(161, 144, 'Don\'t Know'),
(162, 156, 'Very Disciplined'),
(163, 156, 'Disciplined'),
(164, 156, 'Not Disciplined'),
(165, 157, 'Very Disciplined'),
(166, 157, 'Disciplined'),
(167, 157, 'Not Disciplined'),
(168, 158, 'Very Disciplined'),
(169, 158, 'Disciplined'),
(170, 158, 'Not Disciplined'),
(171, 159, 'Very Disciplined'),
(172, 159, 'Disciplined'),
(173, 159, 'Not Disciplined'),
(174, 160, 'Very Disciplined'),
(175, 160, 'Disciplined'),
(176, 160, 'Not Disciplined'),
(177, 161, 'Very Amendable'),
(178, 161, 'Amendable'),
(179, 161, 'Not Amendable'),
(180, 81, 'Traditional'),
(181, 81, 'Other'),
(182, 87, 'Worked there before'),
(183, 90, 'Health');

-- --------------------------------------------------------

--
-- Table structure for table `corpers`
--

CREATE TABLE `corpers` (
  `user_id` int(11) NOT NULL,
  `call_up_number` varchar(25) NOT NULL,
  `state_code` varchar(25) NOT NULL,
  `platoon_id` int(11) NOT NULL,
  `date_of_birth` date NOT NULL,
  `institution_name` varchar(255) NOT NULL,
  `qualification` varchar(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forms`
--

CREATE TABLE `forms` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forms`
--

INSERT INTO `forms` (`id`, `title`, `description`) VALUES
(5, 'Form 4B', 'This form will be used by corpers that are passing out'),
(9, 'Form 4A', 'NYSC Evaluation Form 4A');

-- --------------------------------------------------------

--
-- Table structure for table `forms_status`
--

CREATE TABLE `forms_status` (
  `id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forms_status`
--

INSERT INTO `forms_status` (`id`, `form_id`, `role_id`, `active`) VALUES
(6, 5, 29, 0),
(7, 9, 29, 1);

-- --------------------------------------------------------

--
-- Table structure for table `form_response`
--

CREATE TABLE `form_response` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `form_response`
--

INSERT INTO `form_response` (`id`, `user_id`, `form_id`) VALUES
(330, 2764, 9),
(325, 2766, 9),
(445, 2776, 9),
(91, 2779, 9),
(430, 2787, 9),
(176, 2800, 9),
(446, 2802, 9),
(378, 2805, 9),
(436, 2807, 9),
(25, 2809, 9),
(85, 2819, 9),
(395, 2836, 9),
(249, 2838, 9),
(293, 2844, 9),
(190, 2847, 9),
(369, 2850, 9),
(419, 2852, 9),
(240, 2884, 9),
(29, 2898, 9),
(168, 2918, 9),
(257, 2926, 9),
(268, 2943, 9),
(310, 2946, 9),
(265, 2947, 9),
(114, 2951, 9),
(118, 2952, 9),
(314, 2955, 9),
(444, 2956, 9),
(134, 2970, 9),
(416, 2974, 9),
(317, 2977, 9),
(218, 2985, 9),
(32, 2987, 9),
(281, 2997, 9),
(15, 2999, 9),
(129, 3000, 9),
(155, 3007, 9),
(103, 3013, 9),
(253, 3024, 9),
(105, 3028, 9),
(312, 3049, 9),
(179, 3065, 9),
(187, 3067, 9),
(362, 3068, 9),
(231, 3072, 9),
(95, 3078, 9),
(360, 3080, 9),
(31, 3084, 9),
(264, 3094, 9),
(120, 3103, 9),
(225, 3104, 9),
(452, 3105, 9),
(255, 3106, 9),
(271, 3109, 9),
(233, 3116, 9),
(380, 3127, 9),
(148, 3136, 9),
(26, 3141, 9),
(289, 3143, 9),
(172, 3146, 9),
(104, 3148, 9),
(41, 3169, 9),
(250, 3177, 9),
(307, 3179, 9),
(192, 3182, 9),
(448, 3184, 9),
(24, 3191, 9),
(303, 3197, 9),
(177, 3222, 9),
(83, 3242, 9),
(238, 3286, 9),
(132, 3292, 9),
(82, 3293, 9),
(167, 3299, 9),
(219, 3311, 9),
(316, 3312, 9),
(113, 3320, 9),
(375, 3339, 9),
(111, 3346, 9),
(363, 3354, 9),
(70, 3360, 9),
(211, 3375, 9),
(46, 3376, 9),
(140, 3378, 9),
(415, 3394, 9),
(16, 3396, 9),
(230, 3404, 9),
(324, 3407, 9),
(76, 3415, 9),
(270, 3422, 9),
(186, 3423, 9),
(278, 3425, 9),
(305, 3430, 9),
(396, 3445, 9),
(292, 3449, 9),
(288, 3456, 9),
(213, 3460, 9),
(151, 3461, 9),
(425, 3472, 9),
(245, 3479, 9),
(454, 3481, 9),
(92, 3495, 9),
(381, 3499, 9),
(349, 3507, 9),
(36, 3513, 9),
(45, 3517, 9),
(392, 3538, 9),
(117, 3539, 9),
(321, 3547, 9),
(153, 3554, 9),
(68, 3562, 9),
(379, 3563, 9),
(342, 3574, 9),
(49, 3578, 9),
(19, 3580, 9),
(246, 3599, 9),
(48, 3600, 9),
(301, 3601, 9),
(228, 3610, 9),
(33, 3614, 9),
(244, 3617, 9),
(87, 3624, 9),
(237, 3628, 9),
(304, 3634, 9),
(169, 3637, 9),
(256, 3658, 9),
(62, 3660, 9),
(204, 3661, 9),
(205, 3664, 9),
(367, 3668, 9),
(327, 3672, 9),
(137, 3674, 9),
(222, 3701, 9),
(282, 3717, 9),
(455, 3729, 9),
(412, 3737, 9),
(352, 3740, 9),
(13, 3743, 9),
(203, 3775, 9),
(86, 3784, 9),
(386, 3786, 9),
(259, 3791, 9),
(453, 3794, 9),
(248, 3795, 9),
(217, 3800, 9),
(170, 3802, 9),
(89, 3817, 9),
(125, 3824, 9),
(58, 3828, 9),
(47, 3831, 9),
(141, 3838, 9),
(59, 3856, 9),
(334, 3864, 9),
(63, 3883, 9),
(112, 3888, 9),
(194, 3893, 9),
(372, 3907, 9),
(199, 3910, 9),
(53, 3915, 9),
(421, 3918, 9),
(44, 3925, 9),
(88, 3930, 9),
(224, 3949, 9),
(356, 3970, 9),
(64, 3972, 9),
(214, 3974, 9),
(263, 3983, 9),
(221, 3988, 9),
(206, 3989, 9),
(442, 3991, 9),
(43, 3992, 9),
(269, 3998, 9),
(94, 4007, 9),
(50, 4008, 9),
(450, 4015, 9),
(66, 4020, 9),
(133, 4022, 9),
(429, 4027, 9),
(146, 4032, 9),
(229, 4039, 9),
(144, 4056, 9),
(161, 4059, 9),
(366, 4069, 9),
(171, 4092, 9),
(106, 4098, 9),
(18, 4100, 9),
(340, 4113, 9),
(55, 4119, 9),
(42, 4125, 9),
(110, 4128, 9),
(108, 4130, 9),
(35, 4136, 9),
(185, 4142, 9),
(337, 4147, 9),
(20, 4155, 9),
(280, 4176, 9),
(338, 4182, 9),
(267, 4185, 9),
(210, 4186, 9),
(397, 4187, 9),
(242, 4193, 9),
(38, 4194, 9),
(80, 4196, 9),
(313, 4197, 9),
(400, 4218, 9),
(72, 4228, 9),
(297, 4238, 9),
(180, 4239, 9),
(424, 4247, 9),
(207, 4248, 9),
(126, 4262, 9),
(374, 4270, 9),
(102, 4274, 9),
(122, 4276, 9),
(37, 4290, 9),
(299, 4291, 9),
(100, 4293, 9),
(209, 4308, 9),
(361, 4312, 9),
(198, 4313, 9),
(391, 4322, 9),
(285, 4331, 9),
(241, 4334, 9),
(266, 4338, 9),
(382, 4344, 9),
(160, 4349, 9),
(28, 4366, 9),
(370, 4371, 9),
(201, 4375, 9),
(109, 4386, 9),
(14, 4387, 9),
(235, 4388, 9),
(220, 4399, 9),
(357, 4401, 9),
(188, 4411, 9),
(202, 4413, 9),
(236, 4418, 9),
(286, 4424, 9),
(300, 4430, 9),
(320, 4432, 9),
(196, 4449, 9),
(350, 4463, 9),
(365, 4465, 9),
(355, 4481, 9),
(67, 4486, 9),
(101, 4487, 9),
(298, 4495, 9),
(290, 4500, 9),
(435, 4508, 9),
(193, 4517, 9),
(131, 4521, 9),
(344, 4522, 9),
(75, 4529, 9),
(156, 4532, 9),
(422, 4534, 9),
(107, 4537, 9),
(178, 4540, 9),
(232, 4544, 9),
(208, 4545, 9),
(124, 4559, 9),
(441, 4568, 9),
(84, 4569, 9),
(152, 4600, 9),
(410, 4604, 9),
(65, 4609, 9),
(135, 4616, 9),
(339, 4617, 9),
(411, 4620, 9),
(384, 4621, 9),
(276, 4626, 9),
(273, 4627, 9),
(407, 4630, 9),
(254, 4631, 9),
(98, 4632, 9),
(215, 4639, 9),
(158, 4647, 9),
(449, 4651, 9),
(309, 4683, 9),
(315, 4686, 9),
(93, 4687, 9),
(437, 4696, 9),
(262, 4706, 9),
(328, 4709, 9),
(418, 4715, 9),
(181, 4716, 9),
(149, 4723, 9),
(234, 4727, 9),
(143, 4739, 9),
(115, 4742, 9),
(353, 4749, 9),
(402, 4752, 9),
(413, 4754, 9),
(414, 4768, 9),
(275, 4769, 9),
(78, 4771, 9),
(243, 4774, 9),
(145, 4781, 9),
(27, 4796, 9),
(322, 4798, 9),
(390, 4807, 9),
(173, 4812, 9),
(212, 4819, 9),
(406, 4848, 9),
(331, 4863, 9),
(348, 4883, 9),
(39, 4892, 9),
(22, 4901, 9),
(318, 4911, 9),
(99, 4924, 9),
(51, 4929, 9),
(40, 4932, 9),
(74, 4953, 9),
(165, 4961, 9),
(182, 4962, 9),
(136, 4963, 9),
(399, 4964, 9),
(226, 4966, 9),
(287, 4972, 9),
(197, 5030, 9),
(417, 5038, 9),
(183, 5042, 9),
(73, 5050, 9),
(30, 5052, 9),
(306, 5064, 9),
(191, 5065, 9),
(57, 5073, 9),
(81, 5079, 9),
(97, 5087, 9),
(359, 5095, 9),
(12, 5096, 9),
(71, 5098, 9),
(223, 5101, 9),
(387, 5107, 9),
(164, 5117, 9),
(34, 5118, 9),
(142, 5131, 9),
(154, 5144, 9),
(61, 5148, 9),
(147, 5149, 9),
(54, 5150, 9),
(121, 5151, 9),
(283, 5154, 9),
(398, 5157, 9),
(116, 5172, 9),
(79, 5180, 9),
(296, 5183, 9),
(150, 5192, 9),
(200, 5193, 9),
(130, 5197, 9),
(69, 5199, 9),
(60, 5200, 9),
(277, 5207, 9),
(17, 5214, 9),
(427, 5218, 9),
(166, 5230, 9),
(138, 5231, 9),
(247, 5250, 9),
(52, 5261, 9),
(189, 5268, 9),
(23, 5275, 9),
(423, 5284, 9),
(260, 5292, 9),
(341, 5298, 9),
(443, 5304, 9),
(119, 5316, 9),
(405, 5318, 9),
(123, 5322, 9),
(377, 5337, 9),
(127, 5350, 9),
(162, 5361, 9),
(139, 5365, 9),
(239, 5371, 9),
(56, 5390, 9),
(284, 5397, 9),
(354, 5398, 9),
(21, 5407, 9),
(159, 5411, 9),
(371, 5421, 9),
(420, 5427, 9),
(195, 5436, 9),
(404, 5462, 9),
(77, 5466, 9),
(403, 5498, 9),
(428, 5515, 9);

-- --------------------------------------------------------

--
-- Table structure for table `livestats`
--

CREATE TABLE `livestats` (
  `id` int(11) NOT NULL,
  `state` varchar(60) NOT NULL,
  `form_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answer_format` enum('RADIO_BOX','EMPTY_TEXT_BOX','DROP_DOWN','SIGNATURE','EFFECTIVENESS','SATISFACTION','STATE_DROP_DOWN') NOT NULL,
  `parent_question_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `section_id`, `question`, `answer_format`, `parent_question_id`) VALUES
(22, 13, 'State of Origin', 'EMPTY_TEXT_BOX', NULL),
(23, 13, 'State of Deployment', 'EMPTY_TEXT_BOX', NULL),
(24, 13, 'Place of Primary Assignment', 'EMPTY_TEXT_BOX', NULL),
(25, 14, 'Location of Primary Assignment', 'RADIO_BOX', NULL),
(26, 14, 'In which sector?', 'RADIO_BOX', NULL),
(27, 14, 'Was it in a public or private sector? ', 'RADIO_BOX', NULL),
(28, 14, 'If public was it', 'RADIO_BOX', NULL),
(29, 14, 'How relevant was the field of study to the place of primary assignment? ', 'RADIO_BOX', NULL),
(30, 14, 'Did you have any objection to the nature of primary assignment?', 'RADIO_BOX', NULL),
(31, 14, 'If \'Yes\' to question 6, give reasons for your objections', 'EMPTY_TEXT_BOX', NULL),
(32, 14, 'Did you get a satisfactory change effected?', 'RADIO_BOX', NULL),
(33, 14, 'If not, what reason were you given for it?', 'EMPTY_TEXT_BOX', NULL),
(34, 14, 'Did you change your opinion about the objection by the end of the service year?', 'RADIO_BOX', NULL),
(35, 14, 'If \'yes\' to question 10, give reasons for the change', 'RADIO_BOX', NULL),
(36, 14, 'Continued from Question 11 (Other reasons, please specify)', 'EMPTY_TEXT_BOX', NULL),
(37, 14, 'Were you treated like a permanent staff during your service?', 'RADIO_BOX', NULL),
(38, 14, 'How will you access the attitude of the permanent staff to you as corps member?', 'RADIO_BOX', NULL),
(39, 14, 'Give reason for your response above', 'EMPTY_TEXT_BOX', NULL),
(40, 14, 'What degree of job satisfaction did you derive from primary assignment?', 'RADIO_BOX', NULL),
(41, 14, 'Were you effectively utilised at your place of primary assignment?', 'RADIO_BOX', NULL),
(42, 14, 'Were you given accomodation by your employer?', 'RADIO_BOX', NULL),
(43, 14, 'If \'No\', how were you accommodated?', 'RADIO_BOX', NULL),
(44, 15, 'Which CDS group did you belong? (Specify)', 'EMPTY_TEXT_BOX', NULL),
(45, 15, 'How effective was your CDS group to the host community?', 'RADIO_BOX', NULL),
(46, 15, 'How will you rate the interaction between the group members towards the CDS programmes/projects?', 'RADIO_BOX', NULL),
(47, 15, 'Give reason for your response above', 'EMPTY_TEXT_BOX', NULL),
(48, 15, 'What suggestions will you offer to assist the scheme on group community development service?', 'EMPTY_TEXT_BOX', NULL),
(49, 15, 'Did you embark on any personal CDS projects?', 'RADIO_BOX', NULL),
(50, 15, 'If \'Yes\' specify the type of project and if \'No\' why', 'EMPTY_TEXT_BOX', NULL),
(51, 15, 'How supportive was the host community to the project?', 'RADIO_BOX', NULL),
(52, 16, 'Specify the skill you acquired during the service year', 'EMPTY_TEXT_BOX', NULL),
(53, 16, 'Do you intend to sustain the skills acquired?', 'RADIO_BOX', NULL),
(54, 16, 'Do you have access to any financial support?', 'RADIO_BOX', NULL),
(55, 16, 'If Yes, please specify', 'EMPTY_TEXT_BOX', NULL),
(56, 16, 'If No, what were the challenges you encountered', 'EMPTY_TEXT_BOX', NULL),
(57, 17, 'What was your opinion about NYSC scheme before you were mobilized for National Service?', 'RADIO_BOX', NULL),
(58, 17, 'As a result of your participation in the scheme, has your opinion changed about the scheme?', 'RADIO_BOX', NULL),
(59, 17, 'Judging from your experience during the service year, how will you assess the following NYSC objectives: DISCIPLINE AMONG COPRS MEMBERS', 'EFFECTIVENESS', NULL),
(60, 17, 'PATRIOTISM', 'EFFECTIVENESS', NULL),
(61, 17, 'ADAPTABILITY', 'EFFECTIVENESS', NULL),
(62, 17, 'TEAM SPIRIT', 'EFFECTIVENESS', NULL),
(63, 17, 'INTEGRATION', 'EFFECTIVENESS', NULL),
(64, 17, 'NATIONAL GROWTH', 'EFFECTIVENESS', NULL),
(65, 17, 'SELF RELIANCE', 'EFFECTIVENESS', NULL),
(66, 17, 'NATIONAL UNITY', 'EFFECTIVENESS', NULL),
(67, 17, 'Suggest how to improve the realization of these objectives', 'EMPTY_TEXT_BOX', NULL),
(68, 17, 'Are you willing to stay back in your state of deployment?', 'RADIO_BOX', NULL),
(69, 17, 'Give reasons', 'EMPTY_TEXT_BOX', NULL),
(70, 17, 'Are you willing to remain in the state if your employer retains you?', 'RADIO_BOX', NULL),
(71, 17, 'Give reasons', 'EMPTY_TEXT_BOX', NULL),
(72, 17, 'Give any other suggestions to improve on the scheme\'s activities based on your personal experience', 'EMPTY_TEXT_BOX', NULL),
(77, 19, 'State of Deployment', 'STATE_DROP_DOWN', NULL),
(78, 19, 'Date of Registration in Camp?', 'RADIO_BOX', NULL),
(79, 19, 'State of Origin', 'STATE_DROP_DOWN', NULL),
(80, 19, 'Age', 'EMPTY_TEXT_BOX', NULL),
(81, 19, 'Religion', 'RADIO_BOX', NULL),
(82, 19, 'Sex', 'RADIO_BOX', NULL),
(83, 19, 'Institution of Graduation', 'EMPTY_TEXT_BOX', NULL),
(84, 19, 'Qualification (e.g BSC.)', 'EMPTY_TEXT_BOX', NULL),
(85, 19, 'Area of Specialiazation (e.g Business Administration)', 'EMPTY_TEXT_BOX', NULL),
(86, 19, 'Have you ever visited your state of deployment before National Service?', 'RADIO_BOX', NULL),
(87, 19, 'If Yes, in what capacity', 'DROP_DOWN', NULL),
(88, 19, 'Marital Status', 'RADIO_BOX', NULL),
(89, 19, 'Are you on concessional deployment? (Concessional deployment means your deployment was influenced by you or any NYSC staff)', 'RADIO_BOX', NULL),
(90, 19, 'If yes, on what ground?', 'DROP_DOWN', NULL),
(91, 19, 'Did you attend any briefing conducted by NYSC officials in your institution prior to mobilization?', 'RADIO_BOX', NULL),
(92, 19, 'If no, give reason', 'EMPTY_TEXT_BOX', NULL),
(93, 19, 'How do you feel about your mobilization for National Service?', 'RADIO_BOX', NULL),
(94, 19, 'How do you rate the purpose for establishing NYSC Scheme', 'RADIO_BOX', NULL),
(95, 21, 'You must have registered on-line for the NYSC. Did you collect your call-up from school or you printed on-line?', 'RADIO_BOX', NULL),
(96, 21, 'Which of these two processes do you think is better?', 'RADIO_BOX', NULL),
(97, 21, 'How was the registration on camp?', 'RADIO_BOX', NULL),
(98, 21, 'How long did it take for you to collect your kits?', 'RADIO_BOX', NULL),
(99, 21, 'How can you describe the reception you were given on arrival at the camp?', 'RADIO_BOX', NULL),
(100, 22, 'What role did you play/contribute to the success of the orientation course', 'EMPTY_TEXT_BOX', NULL),
(101, 22, 'Were you satisfied with the kit items given to you?', 'RADIO_BOX', NULL),
(102, 22, 'If no, state your reasons', 'EMPTY_TEXT_BOX', NULL),
(103, 22, 'Were you given your right sizes?', 'RADIO_BOX', NULL),
(104, 23, 'HOW SATISFIED WERE YOU WITH THE FOLLOWING?', 'RADIO_BOX', NULL),
(105, 23, 'Feeding arrangment in general', 'SATISFACTION', NULL),
(106, 23, 'Quantity of food', 'SATISFACTION', NULL),
(107, 23, 'Quality of food served', 'SATISFACTION', NULL),
(108, 23, 'Involvement of corps members in preparation of food', 'SATISFACTION', NULL),
(109, 23, 'Mode of serving food', 'SATISFACTION', NULL),
(110, 24, 'Was there a functional camp clinic?', 'RADIO_BOX', NULL),
(111, 24, 'How do you rate the performance of the clinic?', 'RADIO_BOX', NULL),
(112, 25, 'Were corps members co-opted into Camp Committees?', 'RADIO_BOX', NULL),
(113, 25, 'HOW EFFECTIVE WERE THE FOLLOWING CAMP COMMITTES?', 'RADIO_BOX', NULL),
(114, 25, 'Registration', 'EFFECTIVENESS', NULL),
(115, 25, 'Food', 'EFFECTIVENESS', NULL),
(116, 25, 'Health', 'EFFECTIVENESS', NULL),
(117, 25, 'Socials', 'EFFECTIVENESS', NULL),
(118, 25, 'Disciplinary', 'EFFECTIVENESS', NULL),
(119, 25, 'Maintenance', 'EFFECTIVENESS', NULL),
(120, 25, 'Lecture', 'EFFECTIVENESS', NULL),
(121, 25, 'Security', 'EFFECTIVENESS', NULL),
(122, 25, 'Camp Market', 'EFFECTIVENESS', NULL),
(123, 25, 'Stores', 'EFFECTIVENESS', NULL),
(124, 25, 'OBS', 'EFFECTIVENESS', NULL),
(125, 25, 'Sports', 'EFFECTIVENESS', NULL),
(126, 25, 'ASSESS THE LEVEL OF CORP MEMBERS\' PARTICIPATION IN THE FOLLOWING CAMP ACTIVITIES', 'RADIO_BOX', NULL),
(127, 25, 'Food', 'EFFECTIVENESS', NULL),
(128, 25, 'Health', 'EFFECTIVENESS', NULL),
(129, 25, 'Socials', 'EFFECTIVENESS', NULL),
(130, 25, 'Disciplinary', 'EFFECTIVENESS', NULL),
(131, 25, 'Maintenance', 'EFFECTIVENESS', NULL),
(132, 25, 'Lecture', 'EFFECTIVENESS', NULL),
(133, 25, 'Security', 'EFFECTIVENESS', NULL),
(134, 25, 'OBS', 'EFFECTIVENESS', NULL),
(135, 25, 'Sports', 'EFFECTIVENESS', NULL),
(136, 25, 'Camp Market', 'EFFECTIVENESS', NULL),
(137, 25, 'Stores', 'EFFECTIVENESS', NULL),
(138, 25, 'HOW ORGANIZED WERE THE FOLLOWING? PLEASE RATE ACCORDINGLY', 'RADIO_BOX', NULL),
(139, 25, 'Directional Signpost', 'RADIO_BOX', NULL),
(140, 25, 'Reception at the gate', 'RADIO_BOX', NULL),
(141, 25, 'Registration', 'RADIO_BOX', NULL),
(142, 25, 'Accommodation', 'RADIO_BOX', NULL),
(143, 25, 'Feeding', 'RADIO_BOX', NULL),
(144, 25, 'Kitting', 'RADIO_BOX', NULL),
(145, 25, 'HOW EFFECTIVE WERE THE FOLLOWING?', 'RADIO_BOX', NULL),
(146, 25, 'Medical Care', 'EFFECTIVENESS', NULL),
(147, 25, 'Physical Training', 'EFFECTIVENESS', NULL),
(148, 25, 'Drills and Parade', 'EFFECTIVENESS', NULL),
(149, 25, 'Man O\' War', 'EFFECTIVENESS', NULL),
(150, 25, 'Lectures', 'EFFECTIVENESS', NULL),
(151, 25, 'Professional Lectures', 'EFFECTIVENESS', NULL),
(152, 25, 'SAED Activities', 'EFFECTIVENESS', NULL),
(153, 25, 'Games & Sport', 'EFFECTIVENESS', NULL),
(154, 25, 'Orientation Course in General', 'EFFECTIVENESS', NULL),
(155, 25, 'ASSESS CAMP DISCIPLINE', 'RADIO_BOX', NULL),
(156, 25, 'NYSC Officials', 'RADIO_BOX', NULL),
(157, 25, 'Soldiers', 'RADIO_BOX', NULL),
(158, 25, 'Police / NSCDC', 'RADIO_BOX', NULL),
(159, 25, 'Man O\' War', 'RADIO_BOX', NULL),
(160, 25, 'Others', 'RADIO_BOX', NULL),
(161, 25, 'What extent has the orientation course made you amendable to National Service?', 'RADIO_BOX', NULL),
(162, 25, 'Which aspect of the orientation course did you find boring?', 'EMPTY_TEXT_BOX', NULL),
(163, 25, 'What aspect of the orientation course program did you find most interesting?', 'EMPTY_TEXT_BOX', NULL),
(164, 25, 'What suggestions would you give for improving the orientation course programme?', 'EMPTY_TEXT_BOX', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `question_responses`
--

CREATE TABLE `question_responses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `response` varchar(255) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `role` varchar(25) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `role`, `description`) VALUES
(28, 'Admin', 'A default form role for admins, I don\'t think admins will ever have to fill a form though. '),
(29, 'Corper', 'The corper');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `form_id`, `role_id`, `title`, `description`) VALUES
(13, 5, 29, 'Demographic Data', 'Demographic Data'),
(14, 5, 29, 'Place Of Primary Assignment', 'Place of Primary Assignment'),
(15, 5, 29, 'Community Development Service', 'CDS'),
(16, 5, 29, 'Skill Acquisition and Enterpreneurship Development', 'SAED'),
(17, 5, 29, 'Service Objective', 'Objective of NYSC'),
(19, 9, 29, 'Part 1', 'Part 1'),
(21, 9, 29, 'Part 2', 'Registration'),
(22, 9, 29, 'Part 3', 'Orientation course'),
(23, 9, 29, 'Part 4', 'Feeding'),
(24, 9, 29, 'Part 5', 'Health Care'),
(25, 9, 29, 'Part 6', 'Camp Administration');

-- --------------------------------------------------------

--
-- Table structure for table `signatures`
--

CREATE TABLE `signatures` (
  `user_id` int(11) NOT NULL,
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `user_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `phone_number` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`user_id`, `first_name`, `last_name`, `phone_number`) VALUES
(2761, 'Evaluation', 'Administrator', '08169312496'),
(5512, 'Funaya', 'Peters', '023423434444'),
(5513, 'Isiwat', 'Adenopo', '324234324434'),
(5514, 'Analyst', 'Analyst', '32434324234324'),
(5515, 'Corper', 'Corper', '32434234234'),
(5517, 'Moses', 'NDHQ', '08038336092');

-- --------------------------------------------------------

--
-- Table structure for table `timestamp`
--

CREATE TABLE `timestamp` (
  `id` int(11) NOT NULL,
  `state` varchar(60) NOT NULL,
  `form_id` int(11) NOT NULL,
  `timestamp` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role_id` int(11) NOT NULL,
  `state` enum('Abia','Adamawa','Akwa Ibom','Anambra','Bauchi','Bayelsa','Benue','Borno','Cross River','Delta','Ebonyi','Edo','Ekiti','Enugu','Federal Capital Territory','Gombe','Imo','Jigawa','Kaduna','Kano','Katsina','Kebbi','Kogi','Kwara','Lagos','Nasarawa','Niger','Ogun','Ondo','Osun','Oyo','Plateau','Rivers','Sokoto','Taraba','Yobe','Zamfara') NOT NULL DEFAULT 'Lagos',
  `user_type` enum('Corper','Staff') NOT NULL,
  `access_role` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role_id`, `state`, `user_type`, `access_role`) VALUES
(2761, 'evaluationadmin', 'evaluationadmin**admin', 28, 'Lagos', 'Corper', 'Superadmin'),
(5512, 'funaya', 'funaya', 28, 'Lagos', 'Corper', 'Admin'),
(5513, 'isiwat', 'isiwat', 28, 'Lagos', 'Corper', 'Admin'),
(5514, 'analyst', 'analyst', 28, 'Lagos', 'Corper', 'Analyst'),
(5515, 'corper', 'corper', 29, 'Lagos', 'Corper', 'Corper'),
(5517, 'evaluationndhq', 'moses', 28, 'Lagos', 'Corper', 'Superadmin');

-- --------------------------------------------------------

--
-- Table structure for table `website_logs`
--

CREATE TABLE `website_logs` (
  `id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `http_method` varchar(11) NOT NULL DEFAULT 'GET',
  `http_response_code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answer_options`
--
ALTER TABLE `answer_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `corpers`
--
ALTER TABLE `corpers`
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD UNIQUE KEY `call_up_number` (`call_up_number`),
  ADD UNIQUE KEY `state_code` (`state_code`);

--
-- Indexes for table `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Indexes for table `forms_status`
--
ALTER TABLE `forms_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `form_id` (`form_id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `form_response`
--
ALTER TABLE `form_response`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`form_id`);

--
-- Indexes for table `livestats`
--
ALTER TABLE `livestats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `section_id` (`section_id`);

--
-- Indexes for table `question_responses`
--
ALTER TABLE `question_responses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id_2` (`user_id`,`question_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role` (`role`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `form_id` (`form_id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `signatures`
--
ALTER TABLE `signatures`
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD UNIQUE KEY `link` (`link`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `timestamp`
--
ALTER TABLE `timestamp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `state` (`state`,`form_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `website_logs`
--
ALTER TABLE `website_logs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answer_options`
--
ALTER TABLE `answer_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;

--
-- AUTO_INCREMENT for table `forms`
--
ALTER TABLE `forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `forms_status`
--
ALTER TABLE `forms_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `form_response`
--
ALTER TABLE `form_response`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=456;

--
-- AUTO_INCREMENT for table `livestats`
--
ALTER TABLE `livestats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=165;

--
-- AUTO_INCREMENT for table `question_responses`
--
ALTER TABLE `question_responses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118745;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `timestamp`
--
ALTER TABLE `timestamp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5518;

--
-- AUTO_INCREMENT for table `website_logs`
--
ALTER TABLE `website_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answer_options`
--
ALTER TABLE `answer_options`
  ADD CONSTRAINT `answer_options_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`);

--
-- Constraints for table `corpers`
--
ALTER TABLE `corpers`
  ADD CONSTRAINT `corpers_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `forms_status`
--
ALTER TABLE `forms_status`
  ADD CONSTRAINT `forms_status_ibfk_1` FOREIGN KEY (`form_id`) REFERENCES `forms` (`id`),
  ADD CONSTRAINT `forms_status_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`);

--
-- Constraints for table `question_responses`
--
ALTER TABLE `question_responses`
  ADD CONSTRAINT `question_responses_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `question_responses_ibfk_2` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`);

--
-- Constraints for table `sections`
--
ALTER TABLE `sections`
  ADD CONSTRAINT `sections_ibfk_1` FOREIGN KEY (`form_id`) REFERENCES `forms` (`id`),
  ADD CONSTRAINT `sections_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `signatures`
--
ALTER TABLE `signatures`
  ADD CONSTRAINT `signatures_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `staff`
--
ALTER TABLE `staff`
  ADD CONSTRAINT `staff_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
