<?php

namespace App;

require 'vendor/autoload.php';
require_once "Main/Config/config.php";

// Require Helpers
require_once "Main/Helpers/url_helper.php";
require_once "Main/Helpers/session_helper.php";
require_once "Main/Helpers/service_registry.php";
require_once  'Main/Helpers/utils.php';


require_once 'Main/Config/auth.php';
require_once "Main/Config/routes.php";
