<?php

namespace App\Main\Helpers;

function ends_with($string, $test)
{
    $str_len = strlen($string);
    $test_len = strlen($test);
    if ($test_len > $str_len) {
        return false;
    }

    return substr_compare($string, $test, $str_len - $test_len, $test_len) === 0;
}

function get_short_class($object)
{
    try {
        return (new \ReflectionClass($object))->getShortName();
    } catch (\ReflectionException $e) {
        return false;
    }
}

function removeGenericParameters($params)
{
    unset($params["message"]);
    return $params;
}

/**
 * Polyfill for array_key_last() function added in PHP 7.3.
 *
 * Get the last key of the given array without affecting
 * the internal array pointer.
 *
 * @param array $array An array
 *
 * @return mixed The last key of array if the array is not empty; NULL otherwise.
 */
function get_array_key_last($array)
{
    if (function_exists('array_key_last')) {
        return array_key_last($array);
    }

    $key = null;

    if (is_array($array)) {
        end($array);
        $key = key($array);
    }

    return $key;
}

function normalize_path($path)
{
    $path = str_replace('\\', '/', $path);
    $path = preg_replace('|(?<=.)/+|', '/', $path);
    if (':' === substr($path, 1, 1)) {
        $path = ucfirst($path);
    }
    return $path;
}
