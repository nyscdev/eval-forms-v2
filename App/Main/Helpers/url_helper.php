<?php

namespace App\Main\Helpers;

use App\Main\Exceptions\NotFoundException;
use App\Main\Exceptions\ServiceNotFoundException;
use App\Main\Exceptions\UnauthorizedAccessException;
use App\Main\Http\RequestManagement\Request;
use App\Main\Http\Routing\Router;
use App\Main\Libraries\Log;
use RuntimeException;

function redirect($url)
{
    if (MODE != 'PRODUCTION') {
        try {
            list($url, $requestParams) = get_url_parts($url);
            $request = new Request($url);
            $request->setParams($requestParams);
            Router::routeRequest($request);
            return;
        } catch (NotFoundException|ServiceNotFoundException|UnauthorizedAccessException $e) {
            Log::exception($e);
            throw new RuntimeException($e->getMessage(), $e->getCode(), $e);
        }
    }

    echo "<script type='text/javascript'>window.location.replace('$url');</script>";
}

/**
 * @param $url
 * @return array
 */
function get_url_parts($url): array
{
    $url = str_replace(URL_ROOT, '', $url);

    $withParams = explode('?', $url);
    $requestParams = [];
    if (count($withParams) == 2) {
        $params = explode('&', $withParams[1]);
        foreach ($params as $param) {
            list($key, $value) = explode('=', $param);
            $requestParams[$key] = $value;
        }

    }
    return array($withParams[0], $requestParams);
}

function phpRedirect($url, $permanent = false)
{
    header('Location: ' . $url, true, $permanent ? 301 : 302);

    exit();
}

function sanitize_url($url)
{
    $url = rtrim($url, "/");
    $url = ltrim($url, '/');
    $url = filter_var($url, FILTER_SANITIZE_URL);
    return $url;
}
