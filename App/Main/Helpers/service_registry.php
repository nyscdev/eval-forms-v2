<?php

namespace App\Main\Helpers;

use App\Main\Features\DataManagement\CloudServiceProviders\CloudServiceProvider;
use App\Main\Features\DataManagement\CloudServiceProviders\DropBox\Clients\DropBoxClient;
use App\Main\Features\DataManagement\CloudServiceProviders\DropBox\Clients\KunnuSdkDropBoxClient;
use App\Main\Features\DataManagement\CloudServiceProviders\FileCloudServiceProvider;
use App\Main\Features\DataManagement\CloudServiceProviders\DropBox\DropBoxCloudServiceProvider;
use App\Main\Http\Session\HttpSessionManager;
use App\Main\Http\Session\MockSessionManager;
use App\Main\Http\Session\SessionManager;
use App\Main\Libraries\Rendering\Renderer;
use App\Main\Libraries\Rendering\Twig\TwigRenderer;
use App\Main\Libraries\ServiceLocator;
use App\Main\Repositories\Defaults\DefaultFormRepository;
use App\Main\Repositories\Defaults\DefaultUserRepository;
use App\Main\Repositories\Defaults\DefaultCorperRepository;
use App\Main\Repositories\Defaults\DefaultRoleRepository;
use App\Main\Repositories\Defaults\DefaultSectionRepository;
use App\Main\Repositories\Defaults\DefaultQuestionRepository;
use App\Main\Repositories\Defaults\DefaultAnswerOptionRepository;
use App\Main\Repositories\Defaults\DefaultStaffRepository;
use App\Main\Repositories\Defaults\DefaultQuestionResponseRepository;
use App\Main\Repositories\Defaults\DefaultFormStatusRepository;
use App\Main\Repositories\Defaults\DefaultAnalysisRepository;
use App\Main\Repositories\Defaults\DefaultFormResponseRepository;
use App\Main\Repositories\Defaults\DefaultLiveStatsRepository;
use App\Main\Repositories\Defaults\DefaultTimestampRepository;
use App\Main\Repositories\FormRepository;
use App\Main\Repositories\FormResponseRepository;
use App\Main\Repositories\AnalysisRepository;
use App\Main\Repositories\LiveStatsRepository;
use App\Main\Repositories\TimestampRepository;
use App\Main\Repositories\FormStatusRepository;
use App\Main\Repositories\CorperRepository;
use App\Main\Repositories\UserRepository;
use App\Main\Repositories\RoleRepository;
use App\Main\Repositories\SectionRepository;
use App\Main\Repositories\QuestionRepository;
use App\Main\Repositories\QuestionResponseRepository;
use App\Main\Repositories\AnswerOptionRepository;
use App\Main\Repositories\StaffRepository;
use App\Main\Libraries\Navigation\NavigationBuilder;
use App\Main\Libraries\Navigation\XMLNavigation\XMLNavigationBuilder;

use Twig_Loader_Filesystem;
use Twig_LoaderInterface;

ServiceLocator::registerImplementations([
    // Add any interface => implementation mapping here.

    //================== Repositories ===================//
    UserRepository::class => DefaultUserRepository::class,
    CorperRepository::class => DefaultCorperRepository::class,
    FormRepository::class => DefaultFormRepository::class,
    RoleRepository::class => DefaultRoleRepository::class,
    SectionRepository::class => DefaultSectionRepository::class,
    QuestionRepository::class => DefaultQuestionRepository::class,
    AnswerOptionRepository::class => DefaultAnswerOptionRepository::class,
    NavigationBuilder::class => XMLNavigationBuilder::class,
    StaffRepository::class => DefaultStaffRepository::class,
    QuestionResponseRepository::class => DefaultQuestionResponseRepository::class,
    FormStatusRepository::class => DefaultFormStatusRepository::class,
    AnalysisRepository::class => DefaultAnalysisRepository::class,
    FormResponseRepository::class => DefaultFormResponseRepository::class,
    LiveStatsRepository::class => DefaultLiveStatsRepository::class,
    TimestampRepository::class => DefaultTimestampRepository::class,
    
    //================== Rendering =====================//
    Renderer::class => TwigRenderer::class,

    //================== Session =======================//
       SessionManager::class => MockSessionManager::class, //Make sure to comment this out when deploying to production
    // SessionManager::class => HttpSessionManager::class, Make sure to uncomment this when deploying to production

    //================== Cloud Service Provider Default//
    CloudServiceProvider::class => DropBoxCloudServiceProvider::class,

    //================= DropBox Client ===================//
    DropBoxClient::class => KunnuSdkDropBoxClient::class,
]);

ServiceLocator::registerInstances([
    //Add any instance mappings here

    Twig_LoaderInterface::class => new Twig_Loader_Filesystem(VIEW_ROOT),
]);
