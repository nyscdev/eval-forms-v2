<?php

namespace App\Main\Helpers;

// Handle sessions

session_start();

// Create new flash message
function newFlash(String $name, String $message, $classname = "alert alert-success")
{
    $_SESSION["flash_" . $name] = $message;
    $_SESSION["flash_" . $name . "_class"] = $classname;
}

// Flash message to screen
function flash(String $name)
{
    if (!empty($_SESSION["flash_" . $name])) {
        $message = "<div class='" . $_SESSION["flash_" . $name . "_class"] . "'>" . $_SESSION['flash_' . $name] . ".</div>";
    }
    unset($_SESSION["flash_" . $name]);
    unset($_SESSION["flash_" . $name . "_class"]);

    if (isset($message)) {
        echo $message;
    }
}
