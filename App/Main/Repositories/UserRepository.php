<?php

namespace App\Main\Repositories;

use App\Main\Models\User;
use App\Main\Repositories\Base\Repository;

interface UserRepository extends Repository
{
    /**
     * @param $username
     * @return User|null
     */
    public function findByUsername($username);

    /**
     * @param $username
     * @param $password
     * @return User|null
     */
    public function findByUsernameAndPassword($username, $password);

    /**
     * @param $roleId
     * @return User[]
     */
    public function findByRoleId($roleId);

    public function findByState($state);

    public function findByAccessRole($access_role);
}
