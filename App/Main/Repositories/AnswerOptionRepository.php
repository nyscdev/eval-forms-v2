<?php

namespace App\Main\Repositories;

use App\Main\Repositories\Base\Repository;

interface AnswerOptionRepository extends Repository
{
    public function findByQuestionId($questionId);

    public function findByAnswerOption($option);

    public function findByQuestionIdAndAnswerOption($questionId, $option);

    public function deleteByQuestionId($questionId);
}
