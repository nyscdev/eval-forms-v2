<?php

namespace App\Main\Repositories;

use App\Main\Repositories\Base\Repository;

interface QuestionResponseRepository extends Repository
{
    public function findByUserIdAndQuestionId(int $userId, int $questionId);

    public function findByResponse(string $response);

    public function findByUserId(int $userId);

    public function findByQuestionId(int $questionId);
}
