<?php

namespace App\Main\Repositories;

use App\Main\Repositories\Base\Repository;

interface FormRepository extends Repository
{
    public function findByTitle($formTitle);

    public function findActiveFormsForRole($roleId);
}
