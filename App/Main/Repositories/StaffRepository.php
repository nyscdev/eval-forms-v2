<?php

namespace App\Main\Repositories;

use App\Main\Features\StaffManagement\StaffViewModel;
use App\Main\Repositories\Base\Repository;

interface StaffRepository extends Repository
{
    //Nothing yet
    public function findByFirstName($firstName);

    public function findByLastName($lastName);

    public function findByPhoneNumber($phoneNumber);

    //=========These methods of the repository will retrieve a StaffViewModel as opposed to a Staff MOdel=================//
    public function findAllStaff();
    
    public function findAllStaffForState($state);

    /**
     * This is not to be confused with findById which returns the Staff model.
     * This method returns a StaffViewModel
     * @param string $staffId
     * @return StaffViewModel
     */
    public function findStaffById($staffId);
}
