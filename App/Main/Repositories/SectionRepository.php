<?php

namespace App\Main\Repositories;

use App\Main\Repositories\Base\Repository;

interface SectionRepository extends Repository
{
    public function findByFormId($formId);

    /**
     * Returns the sections of a particular form that are meant to be filled by corpers
     */
    public function findByFormIdForCorper($formId);
}
