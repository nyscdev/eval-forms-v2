<?php

namespace App\Main\Repositories;

use App\Main\Models\Role;
use App\Main\Repositories\Base\Repository;

interface RoleRepository extends Repository
{
    /**
     * @param $role
     * @return Role
     */
    public function findByRole($role);

    /**
     * @param $formId
     * @return array[Role]
     */
    public function findRolesForForm($formId);
}
