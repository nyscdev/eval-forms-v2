<?php

namespace App\Main\Repositories;

use App\Main\Repositories\Base\Repository;

interface LiveStatsRepository extends Repository
{
    public function findByStateAndFormId($state, $formId);
}
