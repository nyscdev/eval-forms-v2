<?php

namespace App\Main\Repositories\Defaults;

use App\Main\Models\QuestionResponse;
use App\Main\Repositories\Base\DatabaseRepository;
use App\Main\Repositories\QuestionResponseRepository;

class DefaultQuestionResponseRepository extends DatabaseRepository implements QuestionResponseRepository
{
    protected $tableName = 'question_responses';

    protected $model = QuestionResponse::class;

    public function findByUserIdAndQuestionId(int $userId, int $questionId)
    {
        return $this->findByMultiple(['user_id' => $userId, 'question_id' => $questionId]);
    }

    public function findByResponse(string $response)
    {
        return $this->findBy('response', $response);
    }

    public function findByUserId(int $userId)
    {
        return $this->findBy('user_id', $userId);
    }

    public function findByQuestionId(int $questionId)
    {
        return $this->findBy('question_id', $questionId);
    }
}
