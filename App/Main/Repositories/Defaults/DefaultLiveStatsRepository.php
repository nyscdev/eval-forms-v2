<?php

namespace App\Main\Repositories\Defaults;

use App\Main\Models\LiveStats;
use App\Main\Repositories\Base\DatabaseRepository;
use App\Main\Repositories\LiveStatsRepository;

class DefaultLiveStatsRepository extends DatabaseRepository implements LiveStatsRepository
{
    protected $tableName = "livestats";

    protected $model = LiveStats::class;

    public function findByStateAndFormId($state, $formId)
    {
        return $this->findByMultiple(['state' => $state, 'form_id' => $formId]);
    }
}
