<?php

namespace App\Main\Repositories\Defaults;

use App\Main\Models\Timestamp;
use App\Main\Repositories\Base\DatabaseRepository;
use App\Main\Repositories\TimestampRepository;

class DefaultTimestampRepository extends DatabaseRepository implements TimestampRepository
{
    protected $tableName = "timestamp";

    protected $model = Timestamp::class;

    public function findByStateAndFormId($state, $formId)
    {
        return $this->findByMultiple(['state' => $state, 'form_id' => $formId]);
    }
}
