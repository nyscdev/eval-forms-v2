<?php

namespace App\Main\Repositories\Defaults;

use App\Main\Models\Corper;
use App\Main\Repositories\Base\DatabaseRepository;
use App\Main\Repositories\CorperRepository;

class DefaultCorperRepository extends DatabaseRepository implements CorperRepository
{
    protected $tableName = 'corpers';

    protected $model = Corper::class;

    public function findByCallUpNumber($callUpNumber)
    {
        return $this->findBy('call_up_number', $callUpNumber);
    }

    public function findByStateCode($stateCode)
    {
        return $this->findBy('state_code', $stateCode);
    }

    public function findByDateOfBirth($birthDate)
    {
        return $this->findBy('date_of_birth', $birthDate);
    }

    public function findByInstitutionName($institutionName)
    {
        return $this->findBy('institution_name', $institutionName);
    }

    public function findByQualification($qualification)
    {
        return $this->findBy('qualification', $qualification);
    }

    public function findByPlatoonId($platoonId)
    {
        return $this->findBy('platoon_id', $platoonId);
    }

    // override findById($id) method from the base pending refactor of that method
    public function findById($user_id)
    {
        return $this->findBy('user_id', $user_id);
    }
}
