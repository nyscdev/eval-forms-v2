<?php

namespace App\Main\Repositories\Defaults;

use App\Main\Models\Role;
use App\Main\Repositories\Base\DatabaseRepository;
use App\Main\Repositories\RoleRepository;

class DefaultRoleRepository extends DatabaseRepository implements RoleRepository
{
    protected $tableName = "roles";

    protected $model = Role::class;

    public function findByRole($role)
    {
        return $this->findOneBy('role', $role);
    }

    public function findRolesForForm($formId)
    {
        $query = "SELECT roles.id, roles.role, roles.description from forms join sections ON forms.id = sections.form_id join roles on sections.role_id = roles.id where forms.id = $formId GROUP BY roles.role";

        return $this->retrieveAll($query);
    }
}
