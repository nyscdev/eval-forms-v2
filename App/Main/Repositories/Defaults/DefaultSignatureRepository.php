<?php

namespace App\Main\Repositories\Defaults;

use App\Main\Models\Signature;
use App\Main\Repositories\Base\DatabaseRepository;
use App\Main\Repositories\SignatureRepository;

class DefaultSignatureRepository extends DatabaseRepository implements SignatureRepository
{
    protected $tableName = 'signatures';

    protected $model = Signature::class;

    // overrides findById() since it uses user_id
    // I think it's possible to refactor the findById() method using the getIdColumnName() of the model object in the base so it works for everything but, for now an override is required.
    public function findById($userId)
    {
        return $this->findOneBy('user_id', $userId);
    }
}
