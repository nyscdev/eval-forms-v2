<?php

namespace App\Main\Repositories\Defaults;

use App\Main\Repositories\Base\DatabaseRepository;
use App\Main\Repositories\SectionRepository;
use App\Main\Models\Section;

class DefaultSectionRepository extends DatabaseRepository implements SectionRepository
{
    protected $tableName = 'sections';

    protected $model = Section::class;

    public function findByFormId($formId)
    {
        return $this->findBy('form_id', $formId);
    }

    public function findByFormIdForCorper($formId)
    {
        $query = "SELECT sections.id, sections.form_id, sections.role_id, sections.title, sections.description FROM `sections` join roles ON sections.role_id = roles.id WHERE sections.form_id = '$formId' and (roles.role = 'Corper' or roles.role = 'corper')";

        return $this->retrieveAll($query);
    }
}
