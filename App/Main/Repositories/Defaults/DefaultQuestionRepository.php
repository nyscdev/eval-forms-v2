<?php

namespace App\Main\Repositories\Defaults;

use App\Main\Models\Question;
use App\Main\Repositories\Base\DatabaseRepository;
use App\Main\Repositories\QuestionRepository;

class DefaultQuestionRepository extends DatabaseRepository implements QuestionRepository
{
    protected $tableName = 'questions';

    protected $model = Question::class;

    public function findBySectionId($sectionId)
    {
        return $this->findBy('section_id', $sectionId);
    }

    public function findByAnswerFormat($answerFormat)
    {
        return $this->findBy('answer_format', $answerFormat);
    }

    public function findByParentQuestionId($questionId)
    {
        return $this->findBy('parent_question_id', $questionId);
    }
}
