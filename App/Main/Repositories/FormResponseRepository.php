<?php

namespace App\Main\Repositories;

use App\Main\Repositories\Base\Repository;

interface FormResponseRepository extends Repository
{
    public function findByUserId($userId);
}
