<?php

namespace App\Main\Repositories;

use App\Main\Repositories\Base\Repository;

interface FormStatusRepository extends Repository
{
    public function findByFormIdAndRoleId($formId, $roleId);

    public function findByFormId($formId);

    public function findByRoleId($roleId);

    public function findByActive($isActive);
}
