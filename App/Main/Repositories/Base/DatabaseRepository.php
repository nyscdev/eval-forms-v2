<?php /** @noinspection SqlDialectInspection */

namespace App\Main\Repositories\Base;

use App\Main\Libraries\Database;
use App\Main\Libraries\Log;
use App\Main\Models\Base\Model;
use PDO;
use PDOException;
use PDOStatement;

abstract class DatabaseRepository implements Repository
{
    /**
     * The name of the Model class used by the repository.
     *
     * @var string The class name for the Model.
     */
    protected $model;

    /**
     * The name of the table used by the repository.
     *
     * @var string
     */
    protected $tableName;

    /**
     * An instance of the database.
     *
     * @var Database
     */
    private $database;

    public function __construct(Database $database)
    {
        $this->database = $database;
    }

    public function save(Model $object)
    {
        Log::info("Saving object...");
        Log::info($object);

        $propertiesMap = [];

        $sql = "INSERT INTO " . $this->tableName . " " . $object->getPDOReadyInsertSyntax();
        $sql .= " ON DUPLICATE KEY UPDATE " . $object->getGenericUpdateSyntax(true);
        $propertiesMap = array_merge($object->getPDOReadyPropertiesMap(), $propertiesMap);
        $this->executeQuery($sql, $propertiesMap);

        if ($object->getId() == null && ($object->getIdColumnName() === 'id')) {
            //update id for auto-incremented tables.
            $newId = $this->database->getConnection()->lastInsertId($this->tableName);
            $object->setId($newId);
        }

        return $object;
    }

    /**
     * Inserts or updates the database table with the objects specified.
     *
     * @param array $objects The Model objects to insert
     * @param bool $updateIdColumnOnDuplicate If true, the ID column will attempt to update. This may lead to constraint violations.
     * @return bool True if successful, false otherwise
     */
    public function saveMultiple(array $objects, $updateIdColumnOnDuplicate = false)
    {
        $first = $objects[0];
        assert($first instanceof Model);

        $sql = "INSERT INTO " . $this->tableName . " " . $first->getInsertKeySyntax() . " VALUES";
        $exclude = $updateIdColumnOnDuplicate ? [] : [$first->getIdColumnName()];
        $updateSyntax = $first->getGenericUpdateSyntax(false, $exclude);

        $isFirst = true;
        foreach ($objects as $object) {
            assert($object instanceof Model);

            if ($object->getId() == null && $object->getIdColumnName() != "id") {
                $id = $this->database->getConnection()->lastInsertId($this->tableName);
                $id = isset($id) ? 0 : $id;
                $id = is_int($id) ? $id + 1 : $id;
                $object->setId($id);
            }

            $delimiter = $isFirst ? ' ' : ', ';
            $insertValues = $object->getPropertiesMap();
            
            $insertValues = $this->getQuotedValuesForInsert($insertValues);
            $sql .= $delimiter . $object->getInsertSyntaxForValues($insertValues);
            $isFirst = false;
        }

        $sql .= " ON DUPLICATE KEY UPDATE " . $updateSyntax;
        $result = $this->executeQuery($sql);

        return $result instanceof PDOStatement || $result;
    }

    /**
     * Queries the DB using the columnName and value, returning a single object.
     *
     * @param $columnName
     * @param $value
     * @return mixed|null
     */
    protected function findOneBy($columnName, $value)
    {
        $value = is_string($value) ? "'" . $value . "'" : $value;
        $query = "SELECT * FROM " . $this->tableName . " WHERE " . $columnName . "=" . (isset($value) ? $value : 'null');
        return $this->retrieveOne($query);
    }

    /**
     * Executes the pending statement and returns the first object, or null if none.
     *
     * @param string|null $query
     * @param string $model
     * @return mixed|null
     */
    protected function retrieveOne(string $query, $model = "")
    {
        if ($model == "" && !isset($this->model)) {
            throw new \RuntimeException("Model not provided for Repository.");
        }

        $model = $model == "" ? $this->model : $model;
        $result = $this->executeQuery($query)->fetchObject($model);
        return is_bool($result) ? null : $result;
    }

    /**
     * Executes the query.
     *
     * @param $query string. The query to execute.
     * @param $statementParameters . An array containing values mapped to the named conditionals in the query string
     * @see PDOStatement::execute()
     * @return bool|PDOStatement
     */
    protected function executeQuery($query, $statementParameters = null)
    {
        $statement = $this->buildSQLStatement($query);
        Log::info("Executing statement...");

        if ($statementParameters != null) {
            Log::info(array_keys($statementParameters));
            Log::info(array_values($statementParameters));
        }

        try {
            $result = $statement->execute($statementParameters);
            return !$result ? false : $statement;
        } catch (PDOException $e) {
            Log::exception($e);
            throw $e;
        }
    }

    /**
     * Prepares a statement with query.
     *
     * @param $query
     * @return bool|PDOStatement
     */
    private function buildSQLStatement($query)
    {
        Log::info("Building SQL query: " . $query);
        return $this->database->getConnection()->prepare($query);
    }

    public function delete(Model $object)
    {
        $id = is_null($object->getId()) ? 'null' : $object->getId();
        $sql = 'DELETE FROM ' . $this->tableName . ' WHERE ' . $object->getIdColumnName() . '=' . $id;
        $result = $this->executeQuery($sql);
        return $result instanceof PDOStatement;
    }

    public function deleteById($id)
    {
        $sql = 'DELETE FROM ' . $this->tableName . ' WHERE ' . 'id=' . $id;
        $result = $this->executeQuery($sql);
        return $result instanceof PDOStatement;
    }

    public function deleteAll()
    {
        $sql = "DELETE FROM $this->tableName"; 
        $this->executeQuery($sql); 
    }

    public function findById($id)
    {
        $query = "SELECT * FROM " . $this->tableName . " WHERE id=" . (isset($id) ? $id : 'null');
        return $this->retrieveOne($query);
    }

    public function findAll()
    {
        $query = "SELECT * FROM " . $this->tableName;
        return $this->retrieveAll($query);
    }

    /**
     * Executes the pending statement and returns all rows as an array of objects.
     *
     * @param string|null $query
     * @param string $model
     * @return array An array of Models.
     */
    protected function retrieveAll(string $query, $model = "")
    {
        if ($model == "" && !isset($this->model)) {
            throw new \RuntimeException("Model not provided for Repository.");
        }

        if ($model == "") {
            $model = $this->model;
        }
        
        return $this->executeQuery($query)->fetchAll(PDO::FETCH_CLASS, $model);
    }

    /**
     * Queries the DB using the columnName and value, returning a collection.
     *
     * @param $columnName
     * @param $value
     * @return array
     */
    protected function findBy($columnName, $value)
    {
        $value = is_string($value) ? "'" . $value . "'" : $value;
        $query = "SELECT * FROM " . $this->tableName . " WHERE " . $columnName . "=" . (isset($value) ? $value : 'null');
        return $this->retrieveAll($query);
    }

    /**
     * Deletes any objects in the table that matches the specified columnName and value
     *
     * @param $columnName
     * @param $value
     * @return bool
     */
    protected function deleteBy($columnName, $value)
    {
        $sql = 'DELETE FROM ' . $this->tableName . ' WHERE ' . $columnName . '=' . $value;
        $result = $this->executeQuery($sql);
        return $result instanceof PDOStatement;
    }

    /**
     * Queries the DB using the 'AND' combination from the array, returning a collection.
     *
     * @param array $array
     * @return array
     */
    protected function findByMultiple(array $array)
    {
        $query = $this->getCombinedQueryFromArray($array);
        return $this->retrieveAll($query);
    }

    /**
     * Builds an SQL SELECT query string using key => values in the array.
     *
     * @param array $array
     * @return string
     */
    private function getCombinedQueryFromArray(array $array): string
    {
        $query = "SELECT * FROM " . $this->tableName . " WHERE ";
        foreach ($array as $columnName => $value) {
            $isLast = $columnName === array_keys($array)[count($array) - 1];
            $value = is_string($value) ? "'" . $value . "'" : $value;
            $query = $query . $columnName . '=' . (isset($value) ? $value : 'null') . ($isLast ? '' : 'AND ');
        }

        return $query;
    }

    /**
     * Puts quotes around and escapes special characters in the values it receives.
     * This will ensure safer insert and update operations
     *
     * @param array $values
     * @return array $values
     */
    private function getQuotedValuesForInsert($values)
    {
        foreach ($values as $key => $value) {
            if (!isset($value)) {
                $value = null;
            } else {
                $value = $this->database->getConnection()->quote($value);
            }
            
            $values[$key] = $value;
        }
        return $values;
    }

    /**
     * Queries the DB using the 'AND' combination from the array, returning a single object.
     *
     * @param array $array
     * @return mixed|null
     */
    protected function findOneByMultiple(array $array)
    {
        $query = $this->getCombinedQueryFromArray($array);
        return $this->retrieveOne($query);
    }
}
