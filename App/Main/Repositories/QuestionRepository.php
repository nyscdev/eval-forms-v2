<?php

namespace App\Main\Repositories;

use App\Main\Repositories\Base\Repository;

interface QuestionRepository extends Repository
{
    public function findBySectionId($sectionId);

    public function findByAnswerFormat($answerFormat);

    public function findByParentQuestionId($questionId);
}
