<?php

namespace App\Main\Repositories\Base;

use App\Main\Models\Base\Model;

interface Repository
{
    /**
     * @param $object Model The model to save
     */
    public function save(Model $object);

    /**
     * Inserts or updates the database table with the objects specified.
     *
     * @param array $objects The Model objects to insert
     * @param bool $updateIdColumnOnDuplicate If true, the ID column will attempt to update. This may lead to constraint violations.
     * @return bool True if successful, false otherwise
     */
    public function saveMultiple(array $objects, $updateIdColumnOnDuplicate = false);

    /**
     * @param Model $object The object to delete.
     * @return bool true if successful. false otherwise.
     */
    public function delete(Model $object);

    /**
     * Delete an object with the specified ID.
     *
     * @param $id mixed The id of the object to delete
     * @return bool true if successful. false otherwise.
     */
    public function deleteById($id);

    /**
     * @param $id
     * @return Model
     */
    public function findById($id);

    /**
     * @return Model[] An array of all elements in the corresponding data source
     */
    public function findAll();

    /**
     * Delete all the elements in the corresponding data source
     */
    public function deleteAll();
}
