<?php

namespace App\Main\Repositories;


use App\Main\Features\DataAnalysis\Aggregators\Aggregator;

interface AnalysisRepository
{
    /**
     * Queries the database for the data as specified by the aggregator
     *
     * @param Aggregator $aggregator
     * @param string|null $model Optional. A class name to map the resulting data to.
     * @return mixed The aggregated data
     */
    public function getAggregatedData(Aggregator $aggregator,  string $model = null);

    /**
     * Queries the database for the data as specified by the query string
     *
     * @param string $sql
     * @param string|null $model Optional. A class name to map the resulting data to.
     * @return mixed
     */
    public function getSqlAggregatedData(string $sql,  string $model = null);
}