<?php

namespace App\Main\Repositories\Defaults;


use App\Main\Features\DataAnalysis\Aggregators\Aggregator;
use App\Main\Libraries\Log;
use App\Main\Repositories\AnalysisRepository;
use App\Main\Repositories\Base\DatabaseRepository;
use PDO;

class DefaultAnalysisRepository extends DatabaseRepository implements AnalysisRepository
{

    /**
     * Queries the database for the data as specified by the aggregator
     *
     * @param Aggregator $aggregator
     * @param string|null $model Optional. A class name to map the resulting data to.
     * @return mixed The aggregated data
     */
    public function getAggregatedData(Aggregator $aggregator, string $model = null)
    {
        $query = $aggregator->getQuery(true);
        $statementParameters = $aggregator->getQueryParameters();
        $statement = $this->executeQuery($query, $statementParameters);

        return $model == null ? $statement->fetchAll(PDO::FETCH_ASSOC) : $statement->fetchAll(PDO::FETCH_CLASS, $model);
    }

    /**
     * Queries the database for the data as specified by the query string
     *
     * @param string $sql
     * @param string|null $model Optional. A class name to map the resulting data to.
     * @return mixed
     */
    public function getSqlAggregatedData(string $sql, string $model = null)
    {
        $statement = $this->executeQuery($sql);
        return $model == null ? $statement->fetchAll(PDO::FETCH_ASSOC) : $statement->fetchAll(PDO::FETCH_CLASS, $model);
    }
}