<?php

namespace App\Main\Repositories\Defaults;

use App\Main\Models\FormResponse;
use App\Main\Repositories\Base\DatabaseRepository;
use App\Main\Repositories\FormResponseRepository;

class DefaultFormResponseRepository extends DatabaseRepository implements FormResponseRepository
{
    protected $tableName = 'form_response';

    protected $model = FormResponse::class;

    public function findByUserId($userId)
    {
        return $this->findBy('user_id', $userId);
    }
}
