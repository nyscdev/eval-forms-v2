<?php

namespace App\Main\Repositories\Defaults;

use App\Main\Models\Form;
use App\Main\Repositories\Base\DatabaseRepository;
use App\Main\Repositories\FormRepository;

class DefaultFormRepository extends DatabaseRepository implements FormRepository
{
    protected $tableName = 'forms';

    protected $model = Form::class;

    public function findByTitle($formTitle)
    {
        return $this->findOneBy('title', $formTitle);
    }

    /**
     * Select the forms that are currently active for a particular user role. That is, forms that have been activated by the admin for a particular form_role to fill
     */
    public function findActiveFormsForRole($roleId)
    {
        $query = "SELECT forms.id, forms.title, forms.description from forms join forms_status on forms.id = forms_status.form_id where forms_status.role_id = $roleId and forms_status.active = true";

        return $this->retrieveAll($query);
    }
}
