<?php

namespace App\Main\Repositories\Defaults;

use App\Main\Models\FormStatus;
use App\Main\Repositories\Base\DatabaseRepository;
use App\Main\Repositories\FormStatusRepository;

class DefaultFormStatusRepository extends DatabaseRepository implements FormStatusRepository
{
    protected $tableName = "forms_status";

    protected $model = FormStatus::class;

    public function findByFormIdAndRoleId($formId, $roleId)
    {
        return $this->findByMultiple(['form_id' => $formId, 'role_id' => $roleId]);
    }

    public function findByFormId($formId)
    {
        return $this->findBy('form_id', $formId);
    }

    public function findByRoleId($roleId)
    {
        return $this->findBy('role_id', $roleId);
    }

    public function findByActive($isActive)
    {
        return $this->findBy('active', $isActive);
    }
}
