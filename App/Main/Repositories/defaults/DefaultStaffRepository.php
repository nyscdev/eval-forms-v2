<?php

namespace App\Main\Repositories\Defaults;

use App\Main\Models\Staff;
use App\Main\Features\StaffManagement\StaffViewModel;
use App\Main\Repositories\Base\DatabaseRepository;
use App\Main\Repositories\StaffRepository;

class DefaultStaffRepository extends DatabaseRepository implements StaffRepository
{
    protected $tableName = 'staff';

    protected $model = Staff::class;

    // overrides findById() since it uses user_id
    // I think it's possible to refactor the findById() method using the getIdColumnName() of the model object in the base so it works for everything but, for now an override is required.
    public function findById($userId)
    {
        return $this->findOneBy('user_id', $userId);
    }

    public function deleteById($id)
    {
        return $this->deleteBy('user_id', $id);
    }

    public function findByFirstName($firstName)
    {
        return $this->findBy('first_name', $firstName);
    }

    public function findByLastName($lastName)
    {
        return $this->findBy('last_name', $lastName);
    }

    public function findByPhoneNumber($phoneNumber)
    {
        return $this->findOneBy('phone_number', $phoneNumber);
    }

    public function findAllStaff()
    {
        $query = "SELECT user_id, first_name, last_name, phone_number, username, password, access_role, role_id, role, state from users join staff on users.id = staff.user_id join roles on roles.id = role_id";

        return $this->retrieveAll($query, StaffViewModel::class);
    }

    public function findAllStaffForState($state)
    {
        $query = "SELECT user_id, first_name, last_name, phone_number, username, access_role, password, role_id, role, state from users join staff on users.id = staff.user_id join roles on roles.id = role_id where state = '$state'";

        return $this->retrieveAll($query, StaffViewModel::class);
    }

    public function findStaffById($staffId)
    {
        $query = "SELECT user_id, first_name, last_name, phone_number, username, access_role, password, role_id, role, state from users join staff on users.id = staff.user_id join roles on roles.id = role_id where user_id = $staffId";

        return $this->retrieveOne($query, StaffViewModel::class);
    }
}
