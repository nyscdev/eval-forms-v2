<?php /** @noinspection SqlDialectInspection */

namespace App\Main\Repositories\Defaults;

use App\Main\Models\User;
use App\Main\Repositories\Base\DatabaseRepository;
use App\Main\Repositories\UserRepository;

class DefaultUserRepository extends DatabaseRepository implements UserRepository
{
    protected $tableName = "users";

    protected $model = User::class;

    public function findByUsername($username)
    {
        return $this->findOneBy('username', $username);
    }

    public function findByUsernameAndPassword($username, $password)
    {
        return $this->findOneByMultiple(['username' => $username, 'password' => $password]);
    }

    public function findByRoleId($roleId)
    {
        return $this->findBy('role_id', $roleId);
    }

    public function findByState($state)
    {
        return $this->findOneBy('state', $state);
    }

    public function findByAccessRole($access_role)
    {
        return $this->findBy('access_role', $access_role);
    }
}
