<?php

namespace App\Main\Repositories\Defaults;

use App\Main\Repositories\AnswerOptionRepository;
use App\Main\Repositories\Base\DatabaseRepository;
use App\Main\Models\AnswerOption;

class DefaultAnswerOptionRepository extends DatabaseRepository implements AnswerOptionRepository
{
    protected $tableName = 'answer_options';

    protected $model = AnswerOption::class;

    public function findByQuestionId($questionId)
    {
        return $this->findBy('question_id', $questionId);
    }

    public function findByAnswerOption($option)
    {
        return $this->findBy('answer_option', $option);
    }

    public function findByQuestionIdAndAnswerOption($questionId, $option)
    {
        return $this->findByMultiple(['question_id' => $questionId,
            'answer_option' => $option]);
    }

    public function deleteByQuestionId($questionId)
    {
        return $this->deleteBy('question_id', $questionId);
    }
}
