<?php

namespace App\Main\Libraries;

use App\Main\Exceptions\DatabaseException;
use function App\Main\Helpers\normalize_path;
use PDO;

class Database
{
    private $dbConnection;

    /**
     * Database constructor.
     * @throws DatabaseException
     */
    public function __construct()
    {
        /** @noinspection SpellCheckingInspection */
        try {
            $dsn = 'mysql:dbname=' . DB_NAME . ';host=' . DB_HOST . ';port=' . DB_PORT;
            $this->dbConnection = new PDO($dsn, DB_USERNAME, DB_PASSWORD, DB_OPTIONS);
            $this->dbConnection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            $this->dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            Log::info("Successfully connected to database.");
        } catch (\Exception $exception) {
            throw new DatabaseException("Failed to connect to database. " . $exception->getMessage(), 500, $exception);
        }
    }

    /**
     * Returns an instance of a connection to the database.
     *
     * @return PDO
     */
    public function getConnection()
    {
        return $this->dbConnection;
    }

    /**
     * Initiates a transaction
     *
     * @return bool
     * @see PDO::beginTransaction()
     */
    public function beginTransaction()
    {
        return $this->dbConnection->beginTransaction();
    }

    /**
     * Commits a transaction.
     *
     * @return bool
     * @see PDO::commit()
     */
    public function commit()
    {
        return $this->dbConnection->commit();
    }

    /**
     *  Rolls back a transaction
     *
     * @return bool
     * @see PDO::rollBack()
     */
    public function rollback()
    {
        return $this->dbConnection->rollBack();
    }

    /**
     * Checks if a transaction is already running.
     *
     * @return bool
     * @see PDO::inTransaction()
     */
    public function inTransaction()
    {
        return $this->dbConnection->inTransaction();
    }

    public function exportDatabaseTables($targetFilePath, array $tables = null)
    {
        Log::info("Backing up database to " . $targetFilePath);
        $targetFilePath = normalize_path($targetFilePath);
        $dumpConfigFileName = $this->getDumpConfigFile();
        $db = DB_NAME;
        $tablesString = $tables != null ? join(' ', $tables) : '';
        $command = "mysqldump --defaults-file=$dumpConfigFileName $db $tablesString > $targetFilePath";
        Log::info("Command: " . $command);

        $output = exec($command) == 0;
        // unlink($dumpConfigFileName);
        return $output;
    }

    /**
     * @return string
     */
    private function getDumpConfigFile()
    {
        $user = DB_USERNAME;
        $password = DB_PASSWORD;
        $dumpConfigFileName = APP_ROOT . '/Main/Config/dump.my.cnf';
        $dumpConfigFileName = normalize_path($dumpConfigFileName);
        $dumpConfigFile = fopen($dumpConfigFileName, "w") or die("Unable to open file!");
        fwrite($dumpConfigFile, "[mysqldump]\nuser=$user\npassword=$password");
        fclose($dumpConfigFile);
        return $dumpConfigFileName;
    }
}
