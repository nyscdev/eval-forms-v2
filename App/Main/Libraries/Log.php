<?php

namespace App\Main\Libraries;

use Exception;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use stdClass;
use Throwable;

class Log
{
    public static function info($message)
    {
        $output = self::getLogMessageWithSeverity($message, 'INFO');

        if (MODE == 'PRODUCTION') {
            try {
                self::getLogger(Logger::INFO)->info(self::getFormattedMessage($message));
            } catch (Exception $exception) {
            }
        }

        self::writeMessageToConsole($output);
    }

    private static function getLogMessageWithSeverity($message, $severity)
    {
        return self::getFormattedDateTime() . " - " . $severity . ": " . self::getFormattedMessage($message);
    }

    /**
     * @return false|string
     */
    private static function getFormattedDateTime()
    {
        return date("d M Y h:m:s A");
    }

    /**
     * @return false|string
     */
    private static function getFormattedDate()
    {
        return date("d_M_Y");
    }

    /**
     * @param $message mixed The object to serialize
     * @return string
     */
    private static function getFormattedMessage($message)
    {
        try {
            if (is_array($message)) {
                $output = '[ ';
                $isFirst = true;
                foreach ($message as $item) {
                    $serialized = $item . '';
                    $serialized = $serialized != '' ? $serialized : serialize($item);
                    $output .= $isFirst ? $serialized : ', ' . $serialized;
                    $isFirst = false;
                }

                $output .= ' ]';
                return $output;
            } elseif (is_string($message)) {
                return $message;
            }

            try {
                return (string)$message;
            } catch (Exception $e) {
                return serialize($message);
            }
        } catch (Throwable $e) {
            return "";
        }
    }

    private static function getLogger($severity)
    {
        $logger = new Logger("");
        try {
            $file = LOGS_ROOT . Logger::getLevelName($severity) . '_' . self::getFormattedDate() . '.log';
            $logger->pushHandler(new StreamHandler($file, $severity));
        } catch (Exception $e) {
        }

        return $logger;
    }

    /**
     * @param $message
     * @param string $color Bash color
     */
    private static function writeMessageToConsole($message, $color = ""): void
    {
        if (MODE === 'DEBUG') {
            echo $color . $message;
            echo "\n";
        }
    }

    public static function warning($message)
    {
        $output = self::getLogMessageWithSeverity($message, 'WARNING');

        if (MODE == 'PRODUCTION') {
            try {
                self::getLogger(Logger::WARNING)->warning(self::getFormattedMessage($message));
            } catch (Exception $exception) {
            }
        }

        self::writeMessageToConsole($output);
    }

    public static function exception(Throwable $throwable)
    {
        $message = $throwable->getMessage();
        Log::error($message);
        $trace = explode('#', $throwable->getTraceAsString());
        foreach ($trace as $value) {
            Log::error($value);
        }
    }

    public static function error($message)
    {
        $output = self::getLogMessageWithSeverity($message, 'ERROR');

        if (MODE == 'PRODUCTION') {
            try {
                self::getLogger(Logger::ERROR)->error(self::getFormattedMessage($message));
            } catch (Exception $exception) {
            }
        }

        self::writeMessageToConsole($output);
    }
}
