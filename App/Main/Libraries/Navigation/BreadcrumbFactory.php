<?php

namespace App\Main\Libraries\Navigation;

use App\Main\Libraries\Log;
use App\Main\Libraries\Navigation\BreadcrumbProviders\BreadcrumbProvider;
use App\Main\Libraries\Navigation\BreadcrumbProviders\FormBreadcrumbProvider;
use App\Main\Exceptions\NotFoundException;
use ReflectionClass;
use ReflectionException;
use RuntimeException;

/**
 * This class is simply responsible for determining the BreadcrumbProvider to use
 * based on the input Breadcrumb type
 */
class BreadcrumbFactory
{
    /**
     * Stores a mapping of the different types of breadcrumbs to the providers responsible for building them.
     *
     * @var array
     */
    private static $breadcrumbProviders = [
        "FormBreadcrumb" => FormBreadcrumbProvider::class
    ];

    /**
     * @param string $type The type of breadcrumb
     * @param array $roles
     * @param null $root The first item in the breadcrumb
     * @return Breadcrumb
     * @throws NotFoundException
     */
    public static function makeBreadcrumbForType(string $type, array $roles, $root = null)
    {
        if (!\array_key_exists($type, self::$breadcrumbProviders)) {
            Log::info("Breadcrumb not found");
            throw new NotFoundException("Breadcrumb not found for type: " .$type);
        }

        try {
            $breadcrumbFactoryReflection = new ReflectionClass(self::$breadcrumbProviders[$type]);
            $breadcrumbProvider = $breadcrumbFactoryReflection->newInstanceArgs(array($root));
            assert($breadcrumbProvider instanceof BreadcrumbProvider);
            $childBreadcrumbs = $breadcrumbProvider->getBreadcrumbs();
            return new Breadcrumb($childBreadcrumbs, $roles);
        } catch (ReflectionException $e) {
            throw new RuntimeException("Unable to obtain Breadcrumb provider for type: " . $type, 500, $e);
        }
    }
}
