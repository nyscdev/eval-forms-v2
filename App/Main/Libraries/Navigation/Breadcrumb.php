<?php

namespace App\Main\Libraries\Navigation;

use App\Main\Exceptions\NavigationException;

/**
 * This class represents a breadcrumb for a specific page
 */
class Breadcrumb implements NavNode
{
    private $roles = [];

    private $childBreadcrumbs = [];

    private $isInCurrentPath = false;

    /**
     * @param array $childBreadcrumbs
     * @param array $roles
     */
    public function __construct($childBreadcrumbs = [], $roles = [])
    {
        $this->roles = $roles;
        $this->childBreadcrumbs = isset($childBreadcrumbs) ? $childBreadcrumbs : [];
    }

    public function hasUrl($url, $params = [])
    {
        //TODO: finish implementing this method
        return false;
    }

    public function hasRole(string $role)
    {
        return in_array($role, $this->roles);
    }

    public function getNavNodeForRole($role)
    {
        if ($this->hasRole($role) != true) {
            return null;
        }

        return $this;
    }

    public function hasChildren()
    {
        return !empty($this->childBreadcrumbs);
    }

    /**
     * @param string $url
     * @param array $params
     * @return array
     * @throws NavigationException
     */
    public function buildBreadcrumbsForUrl($url, $params = [])
    {
        if ($this->hasUrl($url, $params) != true) {
            throw new NavigationException();
        }

        $this->isInCurrentPath = true;

        if ($this->hasChildren() == false) {
            return null;
        }

        $breadcrumbs = null;
        foreach ($this->childBreadcrumbs as $childBreadcrumb) {
            assert($childBreadcrumb instanceof Breadcrumb);

            if ($childBreadcrumb->hasUrl($url, $params)) {
                $breadcrumbs = $childBreadcrumb->buildBreadcrumbsForUrl($url, $params);
                break;
            }
        }

        return $breadcrumbs;
    }
}
