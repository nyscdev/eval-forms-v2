<?php

namespace App\Main\Libraries\Navigation;

use App\Main\Exceptions\NavigationException;
use function App\Main\Helpers\removeGenericParameters;

class NavItemNode extends NavItem implements NavNode
{
    /**
     * @var array
     */
    public $childNodes = [];
    
    /**
     * @param NavItemNode|Breadcrumb $node
     */
    public function addChild($node)
    {
        \array_push($this->childNodes, $node);
    }

    public function getChildren()
    {
        return $this->childNodes;
    }

    public function addChildren($nodes)
    {
        $this->childNodes = \array_merge($this->childNodes, $nodes);
    }

    public function hasChildren()
    {
        return !empty($this->childNodes);
    }

    /**
     * Checks itself and its children to see if they correspond to a particular url
     *
     * @param string $url
     * @param array $params
     * @return bool
     */
    public function hasUrl($url, $params = [])
    {
        $params = removeGenericParameters($params);
        if ($this->getNavUrlWithoutParameters() == $url and $this->parameters == $params) {
            return true;
        }

        if (empty($this->childNodes)) {
            return false;
        }
        
        foreach ($this->childNodes as $child) {
            if ($child->hasUrl($url, $params)) {
                return true;
            }
        }

        return false;
    }

    public function getNavNodeForRole($role)
    {
        if ($this->hasRole($role) == false) {
            return null;
        }

        if ($this->hasChildren()) {
            $childNodesWithRole = [];
            foreach ($this->childNodes as $childNode) {
                $node = $childNode->getNavNodeForRole($role);
                if ($node != null) {
                    \array_push($childNodesWithRole, $node);
                }
            }
            $this->childNodes = $childNodesWithRole;
        }
        
        return $this;
    }

    /**
     * @param string $url
     * @param array $params
     * @return array|null
     * @throws NavigationException
     */
    public function buildBreadcrumbsForUrl($url, $params = [])
    {
        if ($this->hasUrl($url, $params) != true) {
            throw new NavigationException();
        }

        $this->isInCurrentPath = true;

        if ($this->hasChildren() == false) {
            return null;
        }

        $breadcrumbs = null;
        foreach ($this->childNodes as $childNode) {
            assert($childNode instanceof NavNode);

            if ($childNode->hasUrl($url, $params)) {
                $breadcrumbs = $childNode->buildBreadcrumbsForUrl($url, $params);
                break;
            }
        }

        return $breadcrumbs;
    }
}
