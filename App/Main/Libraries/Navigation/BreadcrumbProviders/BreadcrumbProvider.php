<?php

namespace App\Main\Libraries\Navigation\BreadcrumbProviders;

/**
 * All Breadcrumb providers should implement the method in this interface
 */
interface BreadcrumbProvider
{
    public function getBreadcrumbs();
}
