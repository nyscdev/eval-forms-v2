<?php

namespace App\Main\Libraries\Navigation;


/**
 * Represents the navigation for the site and for the page that's currently being loaded in response to a request
 */
class SiteNavigation
{
    /**
     * Contains all the NavNodes in the navigation
     *
     * @var array
     */
    private $navNodes = [];

    private $userRole;

    private $isCurrentPathActivated = false;

    private $currentPageHasBreadcrumbs = false;


    //=================================PUBLIC METHODS==================================//

    /**
     * SiteNavigation constructor.
     * @param $navNodes
     * @param $userRole
     */
    public function __construct($navNodes, $userRole = null)
    {
        if (!empty($navNodes)) {
            $this->navNodes = \array_merge($this->navNodes, $navNodes);
        }

        $this->userRole = $userRole;
    }
    
    public function getNavigationWithoutCurrentPath()
    {
        return $this->getUpdatedSiteNavigationForRole();
    }

    /**
     * @param $url
     * @param array $params
     * @return array
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function buildNavigationForUrl($url, $params = [])
    {
        $updatedSiteNavigation = $this->getUpdatedSiteNavigationForRole();
        $breadcrumbs = $updatedSiteNavigation->getBreadcrumbsFromUrl($url, $params);
        $siteNav = array(
            "navigation" => $updatedSiteNavigation->getNavNodes(),
            "breadcrumbs" => $breadcrumbs
        );

        $this->isCurrentPathActivated = true;
        return $siteNav;
    }

    public function addNavNode($navItemNode)
    {
        \array_push($this->navNodes, $navItemNode);
    }

    public function addNavNodes($navItemNodes)
    {
        $this->navNodes = \array_merge($this->navNodes, $navItemNodes);
    }

    public function getNavNodes()
    {
        return $this->navNodes;
    }

    //=========================================HELPERS===================================//

    private function getUpdatedSiteNavigationForRole()
    {
        $navNodesForRole = [];

        foreach ($this->navNodes as $navNode) {
            assert($navNode instanceof NavNode);
            if ($navNode->hasRole($this->userRole)) {
                \array_push($navNodesForRole, $navNode->getNavNodeForRole($this->userRole));
            }
        }

        $navForRole = new SiteNavigation($navNodesForRole, $this->userRole);
        return $navForRole;
    }

    /**
     * @param $url
     * @param array $params
     * @return array|null
     * @throws \App\Main\Exceptions\NavigationException
     */
    private function getBreadcrumbsFromUrl($url, $params = [])
    {
        $breadcrumbs = null;
        foreach ($this->navNodes as $navNode) {
            assert($navNode instanceof NavNode);

            if ($navNode->hasUrl($url, $params) == true) {
                $breadcrumbs = $navNode->buildBreadcrumbsForUrl($url, $params);
                break;
            }
        }

        if ($breadcrumbs != null) {
            $this->currentPageHasBreadcrumbs = true;
        }
        
        return $breadcrumbs;
    }
}
