<?php

namespace App\Main\Libraries\Navigation;

use App\Main\Http\Routing\Router;
use function App\Main\Helpers\get_array_key_last;

class NavItem
{
    /**
     * @var bool
     */
    public $isInCurrentPath = false;

    /**
     * @var bool
     */
    public $isActive;

    /**
     * The display text for this nav item.
     *
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $handler;

    /**
     * @var string
     */
    public $controller;

    /**
     * Stores all the roles this navItem should be visible for.
     *
     * @var array
     */
    public $roles = [];

    /**
     * Stores mappings for parameters to values that will be used in constructing the url for this navItem
     *
     * @var array
     */
    public $parameters = [];

    /**
     * The fully qualified URL of the navItem with parameters
     *
     * @var string
     */
    public $urlWithParams;

    //=======================================GETTERS AND SETTERS==================================//
    public function setToCurrentPath($isInCurrentPath)
    {
        $this->isInCurrentPath = $isInCurrentPath;
    }

    public function isInCurrentPath()
    {
        return $this->isInCurrentPath;
    }

    public function setActive($isActive)
    {
        $this->isActive = $isActive;
    }

    public function isActive()
    {
        return $this->isActive;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setHandler($handler)
    {
        $this->handler = $handler;
    }

    public function getHandler()
    {
        return $this->handler;
    }

    public function setController($controller)
    {
        $this->controller = $controller;
    }

    public function getController()
    {
        return $this->controller;
    }

    public function addRole(string $role)
    {
        $this->roles[] = $role;
    }

    public function addRoles($roles)
    {
        $this->roles = \array_merge($this->roles, $roles);
    }

    public function deleteRole(string $role)
    {
        $roleKey = \array_search($role, $this->roles);
        if ($roleKey != false) {
            unset($this->roles[$roleKey]);
            return true;
        } else {
            return false;
        }
    }

    public function hasRole($role)
    {
        return in_array($role, $this->roles);
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function addParameter($paramKey, $paramValue)
    {
        $this->parameters[$paramKey] = $paramValue;
    }

    public function deleteParameter($paramKey)
    {
        if (\array_key_exists($paramKey, $this->parameters)) {
            unset($this->parameters[$paramKey]);
        } else {
            return false;
        }
    }

    public function getParamValue($paramKey)
    {
        return $this->parameters[$paramKey];
    }

    public function addParameters(array $params)
    {
        $this->parameters = \array_merge($this->parameters, $params);
    }

    public function hasParameter($paramKey)
    {
        return \array_key_exists($paramKey, $this->parameters);
    }

    public function getParameters()
    {
        return $this->parameters;
    }
    
    public function getParametersAsString()
    {
        $paramString = "";

        if (!empty($this->parameters)) {
            $paramString .= "?";
        }

        foreach ($this->parameters as $paramKey => $paramValue) {
            $paramString .= $paramKey . "=" . $paramValue;

            if ($paramKey != get_array_key_last($this->parameters)) {
                $paramString .= "&";
            }
        }
        return $paramString;
    }

    public function setUrl()
    {
        $this->urlWithParams = $this->getNavUrlWithParameters();
    }

    protected function getNavUrlWithoutParameters()
    {
        return Router::getUrlForMethod($this->controller, $this->handler);
    }

    protected function getNavUrlWithParameters()
    {
        return $this->getNavUrlWithoutParameters() . $this->getParametersAsString();
    }
}
