<?php

namespace App\Main\Libraries\Navigation;

use App\Main\Exceptions\NavigationException;

/**
 * The methods in this interface must be implemented by all types of navigational items that could possibly
 * be children of a NavItemNode.
 *
 */
interface NavNode
{
    /**
     * Checks itself and its children to see if they correspond to a particular url
     *
     * @param string $url
     * @param array $params (optional)
     * @return bool
     */
    public function hasUrl($url, $params = []);

    /**
     * Checks itself to see if it's visible for a particular role
     *
     * @param string $role
     * @return bool
     */
    public function hasRole(string $role);

    /**
     * Checks itself and it's children for a particular role and returns only the child nodes that are visible
     * for the $role
     *
     * @param string $role
     * @return NavNode (breadcrumb or NavItemNode)
     */
    public function getNavNodeForRole($role);


    /**
     * Retrieves any breadcrumbs associated with this node for the specified URL, marking this node and it's children as part of the current path.
     *
     * @param string $url
     * @param array $params (optional)
     * @return array of breadcrumb items (NavItems) - if there are breadcrumbs in the last item of the path, else it returns null
     * @throws NavigationException If the NavNode doesn't have the provided Url
     */
    public function buildBreadcrumbsForUrl($url, $params = []);
}
