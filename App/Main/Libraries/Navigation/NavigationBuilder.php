<?php

namespace App\Main\Libraries\Navigation;

interface NavigationBuilder
{
    /**
     * @return SiteNavigation
     */
    public function getSiteNavigation();

    /**
     * @param SiteNavigation $nav
     * @return void
     */
    public function updateSiteNavigation(SiteNavigation $nav);
}
