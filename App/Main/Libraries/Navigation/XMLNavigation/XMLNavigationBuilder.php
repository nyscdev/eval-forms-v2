<?php

namespace App\Main\Libraries\Navigation\XMLNavigation;

use App\Main\Http\Session\SessionManager;
use App\Main\Libraries\Navigation\BreadcrumbFactory;
use App\Main\Libraries\Navigation\NavigationBuilder;
use App\Main\Libraries\Navigation\SiteNavigation;
use App\Main\Libraries\Navigation\NavItemNode;
use App\Main\Libraries\Navigation\Breadcrumb;
use SimpleXMLIterator;

class XMLNavigationBuilder implements NavigationBuilder
{
    /**
     * @var SessionManager
     */
    private $sessionManager;

    public function __construct(SessionManager $sessionManager)
    {
        $this->sessionManager = $sessionManager;
    }

    /**
     * @return SiteNavigation
     * @throws \App\Main\Exceptions\NotFoundException
     */
    public function getSiteNavigation()
    {
        return $this->buildSiteNavigationFromXML();
    }

    public function updateSiteNavigation(SiteNavigation $nav)
    {
//        $navNodes = $nav->getNavigation();
        // TODO: implement this method
        // $XMLNavigation = $this->buildXMLFromNavNode($navNodes);
    }

    //===============================================HELPERS======================================================//

    /**
     * @return SiteNavigation
     * @throws \App\Main\Exceptions\NotFoundException
     */
    private function buildSiteNavigationFromXML()
    {
        $navPath = \dirname(__FILE__);
	    $xml = new SimpleXMLIterator(join(DIRECTORY_SEPARATOR, [$navPath, 'siteNavigation.xml']), 0, true);
	    $nav = new SiteNavigation($this->buildNavNodesFromXMLObj($xml), $this->sessionManager->getCurrentUserAccessrole());
	    return $nav;
    }

    /**
     * This method builds and returns an array of NavNodes (objects that implement the NavNode interface)
     * from an XMLIteratorObject
     *
     * @param SimpleXMLIterator $xmlIteratorObj
     * @return array $nodes
     * @throws \App\Main\Exceptions\NotFoundException
     */
    private function buildNavNodesFromXMLObj($xmlIteratorObj)
    {
        $xmlIteratorObj->rewind();
        $nodes = [];
        
        while ($xmlIteratorObj->valid()) {
            $navNode = null;
            if ($xmlIteratorObj->key() == "navItem") {
                $navNode = $this->XMLObjToNavItemNode($xmlIteratorObj->current());
                if ($xmlIteratorObj->hasChildren()) {
                    $navNode->addChildren($this->buildNavNodesFromXMLObj($xmlIteratorObj->getChildren()));
                }
            } elseif ($xmlIteratorObj->key() == "breadcrumb") {
                $navNode = $this->XMLObjToBreadcrumbNode($xmlIteratorObj->current());
            }
            $nodes[] = $navNode;
            $xmlIteratorObj->next();
        }

        return $nodes;
    }

    private function XMLObjToNavItemNode(SimpleXMLIterator $xmlIteratorObj)
    {
        $objAttributes = $xmlIteratorObj->attributes();
        $navItemNode = new NavItemNode();
        $navItemNode->setTitle((string)$objAttributes["title"]);
        $navItemNode->setController((string)$objAttributes["controller"]);
        $navItemNode->setHandler((string)$objAttributes["handler"]);

        $isActive = false;
        if ((string)$objAttributes["isActive"] == "true") {
            $isActive = true;
        }
        $navItemNode->setActive($isActive);

        $roles = \explode(" ", (string)$objAttributes["roles"]);
        $navItemNode->addRoles($roles);

        $params = [];
        if (isset($objAttributes["params"])) {
            $paramString = $objAttributes["params"];
            \parse_str($paramString, $params);
        }
        $navItemNode->addParameters($params);
        $navItemNode->setUrl();
        return $navItemNode;
    }

    /**
     * @param $xmlIteratorObj
     * @return Breadcrumb
     * @throws \App\Main\Exceptions\NotFoundException
     */
    private function XMLObjToBreadcrumbNode(SimpleXMLIterator $xmlIteratorObj)
    {
        $objAttributes = $xmlIteratorObj->attributes();
        $roles = \explode(" ", (string)$objAttributes["roles"]);
        $type = (string)$objAttributes["type"];

        return BreadcrumbFactory::makeBreadcrumbForType($type, $roles);
    }
}
