<?php

namespace App\Main\Libraries;

class ViewModel
{
    public $nav;
    public $title;
    public $processingMessage = "";
    public $processingMessageType = "";
    /**
     * ViewModels constructor.
     * @param $title
     */
    public function __construct($title = "", $processingMessage = "", $processingMessageType = "success")
    {
        $this->title = $title;
        $this->processingMessage = $processingMessage;
        $this->processingMessageType = $processingMessageType;
    }

}
