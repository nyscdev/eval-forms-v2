<?php

namespace App\Main\Libraries\Rendering;

use App\Main\Exceptions\RenderException;

interface Renderer
{
    /**
     * Renders a UI file template with data.
     *
     * @param $template
     * @param $data
     * @throws RenderException
     * @return string A fully rendered html string.
     */
    public function render($template, $data);
}
