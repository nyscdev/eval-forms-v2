<?php

namespace App\Main\Libraries\Rendering\Twig;

use App\Main\Exceptions\RenderException;
use function App\Main\Helpers\ends_with;
use App\Main\Libraries\Log;
use App\Main\Libraries\Rendering\Renderer;
use Twig_Environment;
use Twig_Error_Loader;
use Twig_Error_Runtime;
use Twig_Error_Syntax;
use Twig_Loader_Filesystem;
use Twig_Extension_Debug;

class TwigRenderer implements Renderer
{
    /**
     * @var Twig_Environment
     */
    private $twigEnvironment;

    public function __construct(Twig_Environment $environment)
    {
        $this->twigEnvironment = $environment;
        $this->twigEnvironment->enableDebug();
        $this->twigEnvironment->addExtension(new CoreTwigExtension());
        $this->twigEnvironment->addExtension(new Twig_Extension_Debug());
        Log::info("Successfully initialized TwigRenderer");
    }

    public function render($template, $data)
    {
        if (!ends_with($template, '.twig') and $this->twigEnvironment->getLoader() instanceof Twig_Loader_Filesystem) {
            $template = $template . '.twig';
        }

        // Define config files
        $viewData = ["URL_ROOT" => URL_ROOT, "SITE_NAME" => SITE_NAME, "data" => $data];

        try {
            if (is_array($data)) {
                Log::info("Rendering " . $template . " with " . '[' . implode(', ', array_keys($data)) . ']');
            }
            $view = $this->twigEnvironment->render($template, $viewData);
            return $view;
        } catch (Twig_Error_Loader|Twig_Error_Runtime|Twig_Error_Syntax $e) {
            Log::exception($e);
            throw new RenderException("An error occurred while attempting to render " . $template . " with data.", 500, $e);
        }
    }
}
