<?php

namespace App\Main\Libraries\Rendering\Twig;

use App\Main\Http\Routing\Router;
use App\Main\Libraries\Log;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class CoreTwigExtension extends AbstractExtension
{
    public function getFunctions()
    {
        Log::info("Registering extension functions...");

        return [
            new TwigFunction('url_for', array(Router::class, 'getUrlForMethod')),
	        new TwigFunction("date_year", function(string $format){
	        	return date($format);
	        })
        ];
    }
}