<?php

namespace App\Main\Libraries;

use App\Main\Exceptions\CyclicDependencyException;
use App\Main\Exceptions\ServiceNotFoundException;
use ReflectionClass;
use ReflectionException;
use ReflectionParameter;
use RuntimeException;

class ServiceLocator
{
    /**
     * Stores all mappings of interface to implementation.
     *
     * @var array
     */
    private static $registry = [];

    /**
     * Stores all mappings of classes/interfaces to instances.
     *
     * @var array
     */
    private static $instances = [];

    /**
     * Used to store services currently being resolved
     * in order to prevent infinite recursion due to cyclic dependencies.
     *
     * @var array
     */
    private static $memo = [];

    /**
     * Used to retrieve an instance of any class or interface.
     *
     * @param $service mixed The class or interface to retrieve an instance for.
     * @return mixed An instance of the service.
     * @throws ServiceNotFoundException
     */
    public static function get($service)
    {
        if (isset(self::$memo[$service])) {
            self::$memo = [];
            //Fail fast.
            throw new CyclicDependencyException("Cyclic dependency found on " . $service);
        }

        self::$memo[$service] = true;

        if (isset(self::$instances[$service])) {
            unset(self::$memo[$service]);
            return self::$instances[$service];
        }

        try {
            $reflector = new ReflectionClass($service);
            $implementationInstance = self::getImplementationInstanceForService($reflector, $service);

            if (isset($implementationInstance)) {
                unset(self::$memo[$service]);
                return $implementationInstance;
            }

            $instance = self::createInstanceForService($reflector, $service);
            unset(self::$memo[$service]);
            return $instance;
        } catch (ReflectionException $e) {
            throw new ServiceNotFoundException("Unable to find instance of " . $service, $e->getCode(), $e);
        }
    }

    /**
     * @param ReflectionClass $reflector A reflector containing an Interface object
     * @param $service
     * @return mixed | null
     * @throws ServiceNotFoundException
     */
    private static function getImplementationInstanceForService(ReflectionClass $reflector, $service)
    {
        if (!$reflector->isInterface()) {
            return null;
        }

        if (isset(self::$registry[$service])) {
            return self::get(self::$registry[$service]);
        } else {
            throw new RuntimeException("Implementation of " . $service . " has not been provided.");
        }
    }

    /**
     * @param ReflectionClass $reflector
     * @param $service
     * @return object
     * @throws ServiceNotFoundException
     */
    private static function createInstanceForService(ReflectionClass $reflector, $service)
    {
        $constructor = $reflector->getConstructor();

        if (!isset($constructor)) {
            return self::instantiateNoArgConstructor($reflector, $service);
        }

        $parameters = $constructor->getParameters();

        return self::instantiateObjectWithParameters($reflector, $service, $parameters);
    }

    /**
     * @param ReflectionClass $reflector
     * @param $service
     * @return object
     */
    private static function instantiateNoArgConstructor(ReflectionClass $reflector, $service): object
    {
        $instance = $reflector->newInstance();
        //Cache the instance
        self::$instances[$service] = $instance;
        return $instance;
    }

    /**
     * @param ReflectionClass $reflector
     * @param $service
     * @param $parameters
     * @return object
     * @throws ServiceNotFoundException
     */
    private static function instantiateObjectWithParameters(ReflectionClass $reflector, $service, $parameters): object
    {
        $parameterInstances = self::resolveInstancesForConstructorArguments($parameters);
        $instance = $reflector->newInstanceArgs($parameterInstances);

        //Cache the instance
        self::$instances[$service] = $instance;
        return $instance;
    }

    /**
     * @param $parameters ReflectionParameter[]
     * @return array
     * @throws ServiceNotFoundException
     */
    private static function resolveInstancesForConstructorArguments($parameters): array
    {
        $parameterInstances = [];

        foreach ($parameters as $parameter) {
            if ($parameter->isOptional()) {
                continue;
            }

            $parameterInstances[] = self::get($parameter->getType()->getName());
        }
        return $parameterInstances;
    }

    /**
     * Used to provide interface => implementation class mapping to the ServiceLocator
     *
     * @param array $values A array mapping interfaces to class implementations.
     */
    public static function registerImplementations(array $values)
    {
        foreach (array_keys($values) as $key) {
            unset(self::$registry[$key]);
            self::$registry[$key] = $values[$key];
        }
    }

    /**
     * Used to optionally provide instance mappings to the ServiceLocator.
     *
     * @param array $values A dictionary mapping a key to an object instance.
     */
    public static function registerInstances(array $values)
    {
        foreach (array_keys($values) as $key) {
            if (!is_object($values[$key])) {
                throw new RuntimeException($values[$key] . " is not a valid object.");
            }

            unset(self::$instances[$key]);
            self::$instances[$key] = $values[$key];
        }
    }

    public static function clearAll()
    {
        self::$registry = [];
        self::$instances = [];
    }
}
