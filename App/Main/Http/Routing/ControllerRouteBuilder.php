<?php

namespace App\Main\Http\Routing;

use function App\Main\Helpers\sanitize_url;
use App\Main\Http\Enums\HTTPMethod;

class ControllerRouteBuilder
{
    /**
     * @var Route[]
     */
    private $routes = [];

    /**
     * @var
     */
    private $controller;
    /**
     * @var
     */
    private $baseUrl;

    private $addRouteCallBack;

    private $authorities = [];


    public function __construct($controllerName, $baseUrl, callable $addRouteCallBack)
    {
        $this->controller = $controllerName;
        $this->baseUrl = sanitize_url($baseUrl);
        $this->addRouteCallBack = $addRouteCallBack;
    }

    public function addMapping($url, $handler, $httpMethod = HTTPMethod::GET, $authorities = [])
    {
        $url = sanitize_url($url);
        $route = new Route($this->baseUrl . '/' . $url, $httpMethod);
        $route->setController($this->controller);
        $route->setHandler($handler);
        $route->addAuthorities(array_merge($this->authorities, $authorities));
        call_user_func_array($this->addRouteCallBack, [$route]);
        $this->routes[] = $route;
        return $this;
    }

    public function withAuthority($authority)
    {
        foreach ($this->routes as $route) {
            $route->addAuthority($authority);
        }

        $this->authorities[] = $authority;
        return $this;
    }

    public function withAuthorities(array $authorities)
    {
        foreach ($this->routes as $route) {
            $route->addAuthorities($authorities);
        }

        $this->authorities = array_merge($this->authorities, $authorities);
        return $this;
    }
}