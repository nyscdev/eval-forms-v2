<?php

namespace App\Main\Http\Routing;

use App\Main\Controllers\Base\Controller;
use App\Main\Exceptions\NotFoundException;
use App\Main\Exceptions\ServiceNotFoundException;
use App\Main\Exceptions\UnauthorizedAccessException;
use App\Main\Http\AuthManager;
use App\Main\Http\Enums\HTTPMethod;
use App\Main\Http\RequestManagement\Request;
use App\Main\Libraries\Log;
use App\Main\Libraries\ServiceLocator;
use ReflectionClass;
use ReflectionException;
use ReflectionParameter;
use TypeError;
use Exception;

use function App\Main\Helpers\get_short_class;

class Router
{
    /**
     * Stores all route information for the application.
     *
     * @var Route[]
     */
    private static $routeTable = [];

    /**
     * Creates a mapping of a URL and HTTP Method to a Route object.
     *
     * @param $url
     * @param string $httpMethod
     * @return RouteBuilder
     */
    public static function register($url, $httpMethod = HTTPMethod::GET)
    {
        $route = new Route($url, $httpMethod);
        Router::$routeTable[$route->getRouteKey()] = $route;
        return new RouteBuilder($route);
    }

    public static function registerForController($controller, $baseUrl)
    {
        $controllerRouteBuilder = new ControllerRouteBuilder($controller, $baseUrl, function (Route $addedRoute) {
            Router::$routeTable[$addedRoute->getRouteKey()] = $addedRoute;
        });

        $baseRoute = new Route($baseUrl);
        $baseRoute->setController($controller);
        Router::$routeTable[$baseRoute->getRouteKey()] = $baseRoute;

        return $controllerRouteBuilder;
    }

    /**
     * Returns the associated URL for a controller's method.
     *
     * @param $controller string The name of the controller
     * @param $method string. The method who's URL is being searched for.
     * @return null|string. The URL if found, null otherwise.
     */
    public static function getUrlForMethod($controller, $method)
    {
        foreach (self::$routeTable as $route) {
            $matchesController = $route->getController() === $controller ||
                get_short_class($route->getController()) === $controller;

            if ($matchesController and $route->getHandler() === $method) {
                return URL_ROOT . '/' . $route->getUrl();
            }
        }

        return null;
    }

    /**
     * Searches for and invokes a method using values defined in the request.
     * Verifies that the user is authorized to access the URL specified in the request.
     *
     * @param Request $request The request object.
     *
     * @return bool True if the routing completed successfully.
     * @throws ServiceNotFoundException
     * @throws NotFoundException
     * @throws UnauthorizedAccessException
     */
    public static function routeRequest(Request $request)
    {
        $route = self::getValidRouteForRequest($request);
        $controller = self::getControllerInstanceForRoute($route->getController());
        self::invokeAppropriateMethodForRequest($request, $controller, $route);
        return true;
    }


    //============================ Helpers =================================//


    /**
     * Looks up a registered route matching the request and validates it.
     *
     * @param Request $request
     * @return Route a validated route.
     *
     * @throws NotFoundException
     * @throws UnauthorizedAccessException
     */
    private static function getValidRouteForRequest(Request $request): Route
    {  
        
        if (!isset(Router::$routeTable[(string)$request])) { 
            throw new NotFoundException("The specified route could not be found. " . ((string)$request), 404);
        }
         
        $route = Router::$routeTable[(string)$request]; 

        try {
            self::validateRoute($route);
        } catch (TypeError $e) {
            throw new NotFoundException("The specified route could not be found.", 404, $e);
        }
        return $route;
    }

    /**
     * Checks that the route exists and that the user has the right permissions to access it.
     *
     * @param $route Route. The route to validate.
     *
     * @throws UnauthorizedAccessException
     */
    private static function validateRoute(Route $route): void
    {
        if (!AuthManager::hasAuthorities($route->getAuthorities())) {
            throw new UnauthorizedAccessException("You are not authorized to access this resource");
        }
    }

    /**
     * Creates an instance of the route's controller.
     *
     * @param $controllerName string. The name of the controller to instantiate.
     * @return Controller. An instance of the controller.
     *
     * @throws ServiceNotFoundException
     */
    private static function getControllerInstanceForRoute($controllerName): Controller
    {
        return ServiceLocator::get($controllerName);
    }

    /**
     * Checks the route handler info and invokes the method.
     * If the method is not found, the controllers's index will be invoked instead.
     *
     * @param Request $request
     * @param Controller $controller
     * @param Route $route
     *
     * @throws NotFoundException
     */
    private static function invokeAppropriateMethodForRequest(Request $request, Controller $controller, Route $route): void
    {
        if (!method_exists($controller, $route->getHandler())) {
            $response = self::invokeControllerIndex($controller);
        } else {
            $response = self::invokeRouteHandler($request, $controller, $route);
        }

        echo $response;
    }

    /**
     * Invokes the index method in the controller.
     *
     * @param $controller
     * @return mixed
     */
    private static function invokeControllerIndex(Controller $controller)
    {
        return $controller->index();
    }

    /**
     * Obtains all required parameters for the route handler and invokes it.
     *
     * @param Request $request
     * @param Controller $controller
     * @param Route $route
     *
     * @return mixed
     * @throws NotFoundException
     */
    private static function invokeRouteHandler(Request $request, Controller $controller, Route $route)
    {
        $parameters = self::getHandlerParametersFromRequest($controller, $route->getHandler(), $request->getParams());
        return call_user_func_array([$controller, $route->getHandler()], $parameters);
    }

    /**
     * Creates a parameter array that will be used to invoke the controller's method.
     *
     * @param Controller $controller
     * @param string $methodName
     * @param array $requestParams
     * @return array
     *
     * @throws NotFoundException
     */
    private static function getHandlerParametersFromRequest(Controller $controller, string $methodName, array $requestParams): array
    {
        $parametersInfo = self::getParameterInfoForMethod($controller, $methodName);
        $handlerParameters = [];

        foreach ($parametersInfo as $parameter) {
            $name = $parameter->getName();
            if ($parameter->hasType() && !$parameter->getType()->isBuiltin()) {
                $handlerParameters[$name] = self::createObjectParameterFromRequest($requestParams, $parameter->getType()->getName());
            } elseif ($name == "data") {
                $handlerParameters[$name] = $requestParams;
            } else {
                try {
                    $handlerParameters[$name] = $requestParams[$name];
                } catch (Exception $e) {
                    continue;
                }
            }
        }

        return $handlerParameters;
    }

    /**
     * Gets the names of the parameters required by the route method.
     *
     * @param $controller
     * @param string $methodName
     * @return ReflectionParameter[]
     *
     * @throws NotFoundException
     */
    private static function getParameterInfoForMethod($controller, string $methodName): array
    {
        try {
            $reflector = new ReflectionClass($controller);
            return $reflector->getMethod($methodName)->getParameters();
        } catch (ReflectionException $e) {
            throw new NotFoundException("404. Not found", 404, $e);
        }
    }

    /**
     * @param array $requestParams
     * @param $type
     * @return mixed
     * @throws NotFoundException
     */
    private static function createObjectParameterFromRequest(array $requestParams, $type)
    {
        try {
            $reflector = new ReflectionClass($type);
            $value = $reflector->newInstance();
            $properties = $reflector->getProperties();
        } catch (ReflectionException $e) {
            throw new NotFoundException("404. Not found", 404, $e);
        }

        foreach ($properties as $property) {
            if (!isset($requestParams[$property->getName()])) {
                continue;
            }

            //invoke setter.
            $setter = 'set' . ucfirst($property->getName());
            if (method_exists($value, $setter)) {
                $value->{$setter}($requestParams[$property->getName()]);
            } else {
                $value->{$property->getName()} = $requestParams[$property->getName()];
            }
        }
        return $value;
    }
}
