<?php

namespace App\Main\Http\Routing;

class RouteBuilder
{
    /**
     * @var Route
     */
    private $route;

    /**
     * RouteBuilder constructor.
     * @param Route $route
     */
    public function __construct(Route $route)
    {
        $this->route = $route;
    }

    public function withController(string $controller)
    {
        $this->route->setController($controller);
        return $this;
    }

    public function withHandler(string $handler)
    {
        $this->route->setHandler($handler);
        return $this;
    }

    public function withAuthorities(array $authorities)
    {
        $this->route->addAuthorities($authorities);
        return $this;
    }

    public function withAuthority(string $authority)
    {
        $this->route->addAuthority($authority);
        return $this;
    }
}
