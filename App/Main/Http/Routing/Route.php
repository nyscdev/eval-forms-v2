<?php

namespace App\Main\Http\Routing;

use App\Main\Controllers\HomeController;
use function App\Main\Helpers\sanitize_url;
use App\Main\Http\Enums\HTTPMethod;

class Route
{
    private $url;

    private $httpMethod = HTTPMethod::GET;

    private $controller = HomeController::class;

    private $handler = "index";

    private $authorities = [];

    /**
     * Route constructor.
     * @param $url
     * @param string $httpMethod
     */
    public function __construct($url, string $httpMethod = HTTPMethod::GET)
    {
        $this->url = sanitize_url($url);
        $this->httpMethod = $httpMethod;
    }


    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url): void
    {
        $this->url = sanitize_url($url);
    }

    /**
     * @return string
     */
    public function getHttpMethod()
    {
        return $this->httpMethod;
    }

    /**
     * @param string $httpMethod
     */
    public function setHttpMethod(string $httpMethod): void
    {
        $this->httpMethod = $httpMethod;
    }

    /**
     * @return string
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @param string $controller
     */
    public function setController(string $controller): void
    {
        $this->controller = $controller;
    }

    /**
     * @return string
     */
    public function getHandler()
    {
        return $this->handler;
    }

    /**
     * @param string $handler
     */
    public function setHandler(string $handler): void
    {
        $this->handler = $handler;
    }

    /**
     * @return array
     */
    public function getAuthorities(): array
    {
        return $this->authorities;
    }

    /**
     * @param array $authorities
     */
    public function setAuthorities(array $authorities): void
    {
        $this->authorities = $authorities;
    }

    public function addAuthorities(array $authorities)
    {
        foreach ($authorities as $authority) {
            $this->addAuthority($authority);
        }
    }

    public function addAuthority(string $authority)
    {
        $this->authorities[] = $authority;
    }

    public function getRouteKey()
    {
        return $this->httpMethod . '@' . $this->url;
    }
}
