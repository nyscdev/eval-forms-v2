<?php

namespace App\Main\Http\RequestManagement;

use App\Main\Exceptions\Handlers\ExceptionHandler;
use App\Main\Http\Enums\HTTPMethod;
use App\Main\Http\Routing\Router;
use RuntimeException;
use Throwable;

class RequestDispatcher
{
    private static $currentRequest = null;

    public static function dispatchRequest()
    {
        try {
            $request = RequestDispatcher::getRequest();
            self::$currentRequest = $request;
            Router::routeRequest($request);
        } catch (Throwable $e) {
            ExceptionHandler::handleException($e);
        }
    }

    /**
     * @return Request
     */
    public static function getCurrentRequest()
    {
        return self::$currentRequest;
    }

    /**
     * @return Request
     */
    private static function getRequest()
    {
        $requestUrl = $_SERVER['REQUEST_URI'];

        if (!empty($_POST)) {
            return self::makePostRequest();
        } elseif (isset($_GET['url'])) {
            return self::makeGetRequest();
        } elseif ($requestUrl == HOME_URL) {
            return self::makeHomeRequest();
        }

        throw new RuntimeException('Unknown Request.');

        return $requestUrl;
    }

    /**
     * @return Request
     */
    private static function makeGetRequest(): Request
    {
        $request = new Request($_GET['url']);
        unset($_GET['url']);
        $request->setParams($_GET);

        return $request;
    }

    /**
     * @return Request
     */
    private static function makePostRequest(): Request
    {
        $request = new Request($_GET['url'], HTTPMethod::POST);
        unset($_GET['url']);
        $request->setParams($_POST);
        if (!empty($_FILES)) {
            $request->setParams(array_merge($_FILES, $_POST));
        }

        return $request;
    }

    /**
     * @return Request
     */
    private static function makeHomeRequest(): Request
    {
        $request = new Request('');

        return $request;
    }
}
