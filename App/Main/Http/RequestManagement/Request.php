<?php

namespace App\Main\Http\RequestManagement;

//use function App\Main\Helpers\sanitize_url;
use App\Main\Http\Enums\HTTPMethod;

class Request
{
    /**
     * @var string|HTTPMethod
     */
    private $httpMethod = HTTPMethod::GET;

    /**
     * @var string
     */
    private $url = "";

    /**
     * @var array
     */
    private $params = [];

    public function __construct($url, $httpMethod = HTTPMethod::GET)
    {
        $this->httpMethod = $httpMethod;

        $this->setUrl($url);
    }

    /**
     * @return HTTPMethod|string
     */
    public function getHttpMethod()
    {
        return $this->httpMethod;
    }

    /**
     * @param HTTPMethod|string $httpMethod
     */
    public function setHttpMethod($httpMethod): void
    {
        $this->httpMethod = $httpMethod;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getAbsoluteUrl()
    {
        return URL_ROOT . '/' . $this->url;
    }
    
    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = \App\Main\Helpers\sanitize_url($url);
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * @param array $params
     */
    public function setParams(array $params): void
    {
        $this->params = $params;
    }

    public function __toString()
    {
        return $this->httpMethod . "@" . $this->url;
    }
}
