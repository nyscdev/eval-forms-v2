<?php

namespace App\Main\Http\Session;

use App\Main\Models\User;

class MockSessionManager implements SessionManager
{
    /**
     * @var User
     */
    private $user;
    private $formFilled;

    /**
     * MockSessionManager constructor.
     */
    public function __construct()
    {
        $this->user = new User();
        $this->user->setRoleId(29);
        $this->user->setState("Lagos");
        $this->user->setId(3);
    }

    public function getCurrentUserRoleId()
    {
        return $this->user->getRoleId();
    }

    public function getCurrentUserRoleName()
    {
        return "Corper";
    }

    public function getCurrentUserState()
    {
        return $this->user->getState();
    }

    public function getCurrentUserId()
    {
        return 2761;
    }

    public function getCurrentUserAccessrole()
    {
        return "Superadmin";
    }

    public function isUserLoggedIn()
    {
        return true;
    }

    public function setCurrentUser(User $user)
    {
        $this->user = $user;
    }

    public function revokeUserAccess()
    {
        unset($this->user);
    }

    public function storeCustomSessionData(string $key, $value)
    {
        $this->formFilled = $value;
    }

    public function getCustomSessionData(string $key)
    {
        return "Form 3B";
    }

    public function removeCustomSessionData(string $key)
    {
    }
}
