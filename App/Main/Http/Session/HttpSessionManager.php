<?php

namespace App\Main\Http\Session;

use App\Main\Models\Role;
use App\Main\Models\User;
use App\Main\Repositories\RoleRepository;
use App\Main\Repositories\UserRepository;
use InvalidArgumentException;

/**
 * This class handles all interactions with the SESSION array
 * It enforces best security practices to prevent session hijacking and fixation attacks
 */
class HttpSessionManager implements SessionManager
{
    private const ROLE_ID = "nysc.evaluations.users.roles.id";
    private const ROLE_NAME = "nysc.evaluations.users.roles.name";
    private const USER_ID = "nysc.evaluations.users.id";
    private const USER_STATE = "nysc.evaluation.users.state";
    private const LOGGED_IN = "nysc.evaluations.users.status.login";
    private const ACCESS_ROLE = "nysc.evaluations.users.access_role";
    private const CUSTOM_SESSION_KEYS = 'nysc.evaluations.session.custom.keys';

    private const KEYS = [
        self::ROLE_ID,
        self::ROLE_NAME,
        self::USER_ID,
        self::USER_STATE,
        self::LOGGED_IN,
        self::ACCESS_ROLE,
        self::CUSTOM_SESSION_KEYS
    ];

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * HttpSessionManager constructor.
     * @param UserRepository $userRepository
     * @param RoleRepository $roleRepository
     */
    public function __construct(UserRepository $userRepository, RoleRepository $roleRepository)
    {
        $this->userRepository = $userRepository;
        $this->roleRepository = $roleRepository;

        if (!isset($_SESSION[self::CUSTOM_SESSION_KEYS])) {
            $_SESSION[self::CUSTOM_SESSION_KEYS] = [];
        }
    }

    public function getCurrentUserRoleId()
    {
        return $_SESSION[self::ROLE_ID];
    }

    public function getCurrentUserRoleName()
    {
        return $_SESSION[self::ROLE_NAME];
    }

    public function getCurrentUserState()
    {
        return $_SESSION[self::USER_STATE];
    }

    public function getCurrentUserId()
    {
        return $_SESSION[self::USER_ID];
    }

    public function getCurrentUserAccessrole()
    {
        return $_SESSION[self::ACCESS_ROLE];
    }

    public function isUserLoggedIn()
    {
        return $_SESSION[self::LOGGED_IN];
    }

    /**
     * @param User $user
     */
    public function setCurrentUser(User $user)
    {
        $role = $this->roleRepository->findById($user->getRoleId());
        if ($role == null || $this->userRepository->findById($user->getId()) == null) {
            throw new InvalidArgumentException("User is not valid for this application.");
        }

        assert($role instanceof Role);
        $_SESSION[self::ROLE_ID] = $user->getRoleId();
        $_SESSION[self::ROLE_NAME] = $role->getRole();
        $_SESSION[self::USER_ID] = $user->getId();
        $_SESSION[self::USER_STATE] = $user->getState();
        $_SESSION[self::ACCESS_ROLE] = $user->getAccessRole();
        $_SESSION[self::LOGGED_IN] = true;
    }

    public function revokeUserAccess()
    {
        unset($_SESSION[self::ROLE_ID]);
        unset($_SESSION[self::ROLE_NAME]);
        unset($_SESSION[self::USER_ID]);
        unset($_SESSION[self::USER_STATE]);
        unset($_SESSION[self::LOGGED_IN]);
        unset($_SESSION[self::ACCESS_ROLE]);

        foreach ($_SESSION[self::CUSTOM_SESSION_KEYS] as $key) {
            unset($_SESSION[$key]);
        }

        $_SESSION[self::CUSTOM_SESSION_KEYS] = [];
    }

    public function storeCustomSessionData(string $key, $value)
    {
        if (in_array($key, self::KEYS)) {
            throw new InvalidArgumentException("Overriding Application Session Keys is not Allowed.", 500);
        }

        $_SESSION[self::CUSTOM_SESSION_KEYS][$key] = $key;
        $_SESSION[$key] = $value;
    }

    public function removeCustomSessionData(string $key)
    {
        unset($_SESSION[$key]);
        unset($_SESSION[self::CUSTOM_SESSION_KEYS][$key]);
    }

    public function getCustomSessionData(string $key)
    {
        return $_SESSION[$key];
    }
}
