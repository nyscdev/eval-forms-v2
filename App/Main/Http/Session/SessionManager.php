<?php

namespace App\Main\Http\Session;

use App\Main\Models\User;

interface SessionManager
{
    public function setCurrentUser(User $user);

    public function getCurrentUserRoleId();

    public function getCurrentUserRoleName();

    public function getCurrentUserState();

    public function getCurrentUserId();

    public function isUserLoggedIn();

    public function getCurrentUserAccessrole();

    public function revokeUserAccess();

    public function storeCustomSessionData(string $key, $value);

    public function removeCustomSessionData(string $key);

    public function getCustomSessionData(string $key);
}
