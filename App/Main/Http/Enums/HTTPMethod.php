<?php

namespace App\Main\Http\Enums;

abstract class HTTPMethod
{
    const GET = "GET";
    const POST = "POST";
}
