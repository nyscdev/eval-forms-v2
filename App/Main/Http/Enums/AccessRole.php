<?php

namespace App\Main\Http\Enums;

use ReflectionClass;

abstract class AccessRole
{
    const SUPERADMIN = "Superadmin";
    const ADMIN = "Admin";
    const ANALYST = "Analyst";
    const CORPER = "Corper";
    const STAFF = "Staff";

    const HOMEPAGES = [
        "Superadmin" => [
            "Controller" => "StaffController",
            "Handler" => "viewStaff"
        ],

        "Admin" => [
            "Controller" => "StaffController",
            "Handler" => "viewStaffForCurrentUserState"
        ],

        "Analyst" => [
            "Controller" => "AnalysisController",
            "Handler" => "getAnalyzeCorperDataView"
        ],

        "Corper" => [
            "Controller" => "FormResponseController",
            "Handler" => "getFormsViewForCurrentUser"
        ],

        "Staff" => [
            "Controller" => "FormResponseController",
            "Handler" => "getFormsViewForCurrentUser"
        ]
    ];

    /**
     * @return array
     * @throws \ReflectionException
     */
    public static function getConstants()
    {
        $r = new ReflectionClass(__CLASS__);
        return $r->getConstants();
    }
    
    /**
     * @return array
     * @throws \ReflectionException
     */
    public static function getValues()
    {
        $r = self::getConstants();
        $constantVals = [];

        foreach ($r as $key => $value) {
            if ($key == 'HOMEPAGES') {
                continue;
            }
            $constantVals[] = $value;
        }

        return $constantVals;
    }
}
