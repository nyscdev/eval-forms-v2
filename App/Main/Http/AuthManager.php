<?php

namespace App\Main\Http;

use App\Main\Http\Session\SessionManager;
use App\Main\Libraries\ServiceLocator;

class AuthManager
{
    private static $sessionManager;


    /**
     * Checks that user's role is one of the roles in the authorities for a particular request
     *
     * @param array $authorities
     * @return bool
     */
    public static function hasAuthorities(array $authorities)
    {
        self::$sessionManager =  ServiceLocator::get(SessionManager::class);

        if (empty($authorities)) {
            return true;
        }

        return \in_array(self::$sessionManager->getCurrentUserAccessrole(), $authorities);
    }
}
