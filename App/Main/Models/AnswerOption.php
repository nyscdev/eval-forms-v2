<?php

namespace App\Main\Models;

use App\Main\Libraries\Log;
use App\Main\Models\Base\Model;

class AnswerOption extends Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $question_id;

    /**
     * @var string
     */
    private $answer_option;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getQuestionId()
    {
        return $this->question_id;
    }

    /**
     * @param int $questionId
     */
    public function setQuestionId(int $questionId)
    {
        $this->question_id = $questionId;
    }

    /**
     * @return string
     */
    public function getAnswerOption()
    {
        return $this->answer_option;
    }

    /**
     * @param string $option
     */
    public function setAnswerOption(string $option): void
    {
        Log::info("Setting Option: ".$option);
        $this->answer_option = $option;
        Log::info("Done Setting Option: ".$this->answer_option);
    }
}
