<?php

namespace App\Main\Models\Enums;

use ReflectionClass;

abstract class AnswerFormat
{
    const RADIO_BOX = "RADIO_BOX";
    const EMPTY_TEXT_BOX = "EMPTY_TEXT_BOX";
    const DROP_DOWN = "DROP_DOWN";
    const SIGNATURE = "SIGNATURE";
    const EFFECTIVENESS = "EFFECTIVENESS";
    const SATISFACTION = "SATISFACTION";
    const STATE_DROP_DOWN = "STATE_DROP_DOWN";

    /**
     * @return array
     * @throws \ReflectionException
     */
    public static function getConstants()
    {
        $r = new ReflectionClass(__CLASS__);
        return $r->getConstants();
    }
}
