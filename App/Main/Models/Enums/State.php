<?php

namespace App\Main\Models\Enums;

use App\Main\Exceptions\NotFoundException;
use ReflectionClass;

abstract class State
{
    const AB = "Abia";
    const AD = "Adamawa";
    const AK = "Akwa Ibom";
    const AN = "Anambra";
    const BA = "Bauchi";
    const BY = "Bayelsa";
    const Be = "Benue";
    const BO = "Borno";
    const CR = "Cross River";
    const DE = "Delta";
    const EB = "Ebonyi";
    const ED = "Edo";
    const EK = "Ekiti";
    const EN = "Enugu";
    const FC = "Federal Capital Territory";
    const GO = "Gombe";
    const IM = "Imo";
    const JI = "Jigawa";
    const KD = "Kaduna";
    const KN = "Kano";
    const KT = "Katsina";
    const KE = "Kebbi";
    const KO = "Kogi";
    const KW = "Kwara";
    const LA = "Lagos";
    const NA = "Nasarawa";
    const NI = "Niger";
    const OG = "Ogun";
    const ON = "Ondo";
    const OS = "Osun";
    const OY = "Oyo";
    const PL = "Plateau";
    const RI = "Rivers";
    const SO = "Sokoto";
    const TA = "Taraba";
    const YO = "Yobe";
    const ZA = "Zamfara";

    /**
     * @return array
     * @throws \ReflectionException
     */
    public static function getConstants()
    {
        $r = new ReflectionClass(__CLASS__);
        return $r->getConstants();
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public static function getValues()
    {
        $r = self::getConstants();
        $constantVals = [];

        foreach ($r as $key => $value) {
            $constantVals[] = $value;
        }

        return $constantVals;
    }

    public static function getStateName($stateAbbreviation)
    {
        $stateName = constant("self::$stateAbbreviation");

        if($stateName == null){
            throw new NotFoundException("State code not found");
        }
        return $stateName;
    }
}
