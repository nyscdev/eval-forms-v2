<?php

namespace App\Main\Models;

use App\Main\Models\Base\Model;

class Section extends Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $form_id;

    /**
     * @var int
     */
    private $role_id;
    
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $description;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getFormId()
    {
        return $this->form_id;
    }

    /**
     * @param int $formId
     */
    public function setFormId(int $formId): void
    {
        $this->form_id = $formId;
    }

    /**
     * @return int
     */
    public function getRoleId()
    {
        return $this->role_id;
    }

    /**
     * @param int $roleId
     */
    public function setRoleId(int $roleId): void
    {
        $this->role_id = $roleId;
    }
    
    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }
}
