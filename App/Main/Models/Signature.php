<?php

namespace App\Main\Models;

use App\Main\Models\Base\Model;

class Signature extends Model
{
    /**
     * @var int
     */
    private $user_id;

    /**
     * @var string
     */
    private $link;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->user_id;
    }

    /**
     * @param int $id
     */
    public function setId($id): void
    {
        $this->user_id = $id;
    }

    public function getIdColumnName()
    {
        return 'user_id';
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->user_id = $userId;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink(string $link): void
    {
        $this->link = $link;
    }
}
