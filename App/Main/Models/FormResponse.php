<?php

namespace App\Main\Models;

use App\Main\Libraries\Log;
use App\Main\Models\Base\Model;

class FormResponse extends Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $user_id;

    /**
     * @var int
     */
    private $form_id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getFormId()
    {
        return $this->form_id;
    }

    /**
     * @param int $formId
     */
    public function setFormId(int $formId)
    {
        $this->form_id = $formId;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->user_id = $userId;
    }
}
