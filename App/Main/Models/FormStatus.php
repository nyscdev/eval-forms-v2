<?php

namespace App\Main\Models;

use App\Main\Models\Base\Model;

class FormStatus extends Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $form_id;

    /**
     * @var int
     */
    private $role_id;

    /**
     * @var bool
     */
    private $active;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }


    /**
     * @return int
     */
    public function getFormId()
    {
        return $this->form_id;
    }

    /**
     * @param int $formId
     */
    public function setFormId(int $formId): void
    {
        $this->form_id = $formId;
    }

    /**
     * @return int
     */
    public function getRoleId()
    {
        return $this->role_id;
    }

    /**
     * @param int $roleId
     */
    public function setRoleId(int $roleId): void
    {
        $this->role_id = $roleId;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }
}
