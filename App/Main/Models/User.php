<?php

namespace App\Main\Models;

use App\Main\Models\Base\Model;
use InvalidArgumentException;

class User extends Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var int
     */
    private $role_id;


    /**
     * @var string
     */
    private $state;

    /**
     * @var string
     */
    private $user_type;

    /**
     * @var string
     */
    private $access_role;

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id): void
    {
        if (!isset($id)) {
            throw new InvalidArgumentException("User Id may not be null");
        }

        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        if (!isset($username)) {
            throw new InvalidArgumentException("Username may not be null");
        }

        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        //TODO: Do password validity checks

        //This operation is commented out because it is too slow and I'm not sure we have data sensitive enough
        //to warrant its usage.
        //$this->password = password_hash($password, PASSWORD_DEFAULT, ["cost" => 8]);
        
        $this->password = $password;
    }

    /**
     * @param string $password
     * @return bool
     */
    public function passwordEquals(string $password): bool
    {
        //return \password_verify($password, $this->password);
        
        return $this->password ==  strtolower($password);
    }

    /**
     * @return int
     */
    public function getRoleId()
    {
        return $this->role_id;
    }

    /**
     * @param int $roleId
     */
    public function setRoleId(int $roleId): void
    {
        if (!isset($roleId)) {
            throw new InvalidArgumentException("Role Id may not be null");
        }

        $this->role_id = $roleId;
    }

    public function getAccessRole()
    {
        return $this->access_role;
    }

    public function setAccessRole(string $access_role)
    {
        $this->access_role = $access_role;
    }

    public function setState($state): void
    {
        $this->state = $state;
    }

    public function getState()
    {
        return $this->state;
    }

    /**
     * @return string
     */
    public function getUserType()
    {
        return $this->user_type;
    }

    /**
     * @param string $type
     */
    public function setUserType(string $type): void
    {
        $this->user_type = $type;
    }
}
