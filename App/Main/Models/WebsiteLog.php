<?php

namespace App\Main\Models;

use App\Main\Models\Base\Model;

class WebsiteLog extends Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $url;

    /**
     * @var int
     */
    private $time_stamp;

    /**
     * @var string
     */
    private $http_method;

    /**
     * @var int
     */
    private $http_response_code;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    /**
     * @return int
     */
    public function getTimeStamp()
    {
        return $this->time_stamp;
    }

    /**
     * @param int $timeStamp
     */
    public function setTimeStamp(int $timeStamp): void
    {
        $this->time_stamp = $timeStamp;
    }

    /**
     * @return string
     */
    public function getHttpMethod()
    {
        return $this->http_method;
    }

    /**
     * @param string $httpMethod
     */
    public function setHttpMethod(string $httpMethod): void
    {
        $this->http_method = $httpMethod;
    }

    /**
     * @return int
     */
    public function getHttpResponseCode()
    {
        return $this->http_response_code;
    }

    /**
     * @param int $httpResponseCode
     */
    public function setHttpResponseCode(int $httpResponseCode): void
    {
        $this->http_response_code = $httpResponseCode;
    }
}
