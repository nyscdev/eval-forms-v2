<?php

namespace App\Main\Models;

use App\Main\Models\Base\Model;

class Staff extends Model
{
    /**
     * @var int
     */
    private $user_id;

    /**
     * @var string
     */
    private $first_name;

    /**
     * @var string
     */
    private $last_name;
    
    /**
     * @var string
     */
    private $phone_number;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->user_id;
    }

    /**
     * @param int $id
     */
    public function setId($id): void
    {
        $this->user_id = $id;
    }

    public function getIdColumnName()
    {
        return 'user_id';
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->first_name = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->last_name = $lastName;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phone_number;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber(string $phoneNumber): void
    {
        $this->phone_number = $phoneNumber;
    }
}
