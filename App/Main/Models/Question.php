<?php

namespace App\Main\Models;

use App\Main\Models\Base\Model;
use App\Main\Models\Enums\AnswerFormat;

class Question extends Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var int
     */
    private $section_id;

    /**
     * @var string
     */
    private $question;

    /**
     * @var string | AnswerFormat
     */
    private $answer_format = AnswerFormat::EMPTY_TEXT_BOX;

    /**
     * @var int | null
     */
    private $parent_question_id = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getSectionId()
    {
        return $this->section_id;
    }

    /**
     * @param int $sectionId
     */
    public function setSectionId(int $sectionId): void
    {
        $this->section_id = $sectionId;
    }

    /**
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param string $question
     */
    public function setQuestion(string $question): void
    {
        $this->question = $question;
    }

    /**
     * @return AnswerFormat|string
     */
    public function getAnswerFormat()
    {
        return $this->answer_format;
    }

    /**
     * @param AnswerFormat|string $answerFormat
     */
    public function setAnswerFormat($answerFormat): void
    {
        $this->answer_format = $answerFormat;
    }

    public function setParentQuestionId($parentId)
    {
        $this->parent_question_id = $parentId;
    }

    public function getParentQuestionId()
    {
        return $this->parent_question_id;
    }
}
