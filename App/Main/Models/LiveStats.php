<?php

namespace App\Main\Models;

use App\Main\Models\Base\Model;

class LiveStats extends Model
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $state;

    /**
     * @var int
     */
    private $form_id;
    
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    
    public function getState()
    {
        return $this->state;
    }

    public function setState($state)
    {
        $this->state = $state;
    }

    public function getFormId()
    {
        return $this->form_id;
    }

    public function setFormId(int $form_id)
    {
        $this->form_id = $form_id;
    }
}
