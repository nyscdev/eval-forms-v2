<?php

namespace App\Main\Models;

use App\Main\Models\Base\Model;

class Corper extends Model
{
    /**
     * @var int
     */
    private $user_id;

    /**
     * @var string
     */
    private $call_up_number;

    /**
     * @var string
     */
    private $date_of_birth;

    /**
     * @var string
     */
    private $state_code;

    /**
     * @var string
     */
    private $institution_name;

    /**
     * @var string
     */
    private $qualification;

    /**
     * @var string
     */
    private $first_name;

    /**
     * @var string
     */
    private $last_name;

    /**
     * @var string
     */
    private $middle_name;
	
    /**
     * @var int
     */
    private $platoon_id;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->user_id;
    }

    /**
     * @param int $id
     */
    public function setId($id): void
    {
        $this->user_id = $id;
    }

    public function getIdColumnName()
    {
        return 'user_id';
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId): void
    {
        $this->user_id = $userId;
    }

    /**
     * @return string
     */
    public function getCallUpNumber()
    {
        return $this->call_up_number;
    }

    /**
     * @param string $callUpNumber
     */
    public function setCallUpNumber(string $callUpNumber): void
    {
        $this->call_up_number = $callUpNumber;
    }

    /**
     * @return int
     */
    public function getDateOfBirth()
    {
        return $this->date_of_birth;
    }

    /**
     * @param int $dateOfBirth
     */
    public function setDateOfBirth(string $dateOfBirth): void
    {
        $this->date_of_birth = $dateOfBirth;
    }

    /**
     * @return string
     */
    public function getStateCode()
    {
        return $this->state_code;
    }

    /**
     * @param string $stateCode
     */
    public function setStateCode(string $stateCode): void
    {
        $this->state_code = $stateCode;
    }

    /**
     * @return string
     */
    public function getInstitutionName()
    {
        return $this->institution_name;
    }

    /**
     * @param string $institutionName
     */
    public function setInstitutionName(string $institutionName): void
    {
        $this->institution_name = $institutionName;
    }

    /**
     * @return string
     */
    public function getQualification()
    {
        return $this->qualification;
    }

    /**
     * @param string $qualification
     */
    public function setQualification(string $qualification): void
    {
        $this->qualification = $qualification;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->first_name = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->last_name = $lastName;
    }

    /**
     * @return string
     */
    public function getMiddleName()
    {
        return $this->middle_name;
    }

    /**
     * @param string $middleName
     */
    public function setMiddleName(string $middleName): void
    {
        $this->middle_name = $middleName;
    }
	
	/**
     * @return int
     */
    public function getPlatoonId()
    {
        return $this->platoon_id;
    }

    /**
     * @param int $platoon_id
     */
    public function setPlatoonId(int $platoon_id): void
    {
        $this->platoon_id = $platoon_id;
    }

}
