<?php

namespace App\Main\Models\Base;

use App\Main\Libraries\Log;
use ReflectionClass;
use ReflectionException;
use ReflectionProperty;
use Throwable;

abstract class Model
{

    /**
     * Gets the id of this model. All models should have an id column.
     *
     * @return mixed The id of the model
     */
    abstract public function getId();

    /**
     * Sets the id of this model.
     *
     * @param $id
     * @return mixed
     */
    abstract public function setId($id);

    /**
     * Returns the name of the primary key/ unique id column for the model.
     * Subclasses with a different identifier column should override this.
     *
     * @return string
     */
    public function getIdColumnName()
    {
        return 'id';
    }

    /**
     * Checks that all properties have valid data.
     *
     * @return bool true if all fields have valid data, false otherwise.
     */
    public function isValid()
    {
        return false;
    }

    /**
     * Returns a query syntax that can be called alongside 'INSERT INTO TABLE'
     *
     * @param array $exclude Properties here will be excluded
     * @return string
     */
    public function getInsertSyntax($exclude = [])
    {
        $values = $this->getPropertiesMap(true, $exclude);
        return "(" . implode(", ", array_keys($values)) . ") VALUES (" . implode(', ', array_values($values)) . ")";
    }

    public function getInsertKeySyntax()
    {
        $values = $this->getPropertiesMap(false);
        
        if ($this->getIdColumnName() == "id") {
            unset($values["id"]);
        }

        return "(" . implode(", ", array_keys($values)) . ")";
    }

    public function getInsertValues()
    {
        $values = [];

        foreach ($this->getPropertiesMap(false) as $key => $value) {
            $values[$key] = (isset($value) ? is_string($value) ? "'" . $value . "'" : $value : 'null');
        }

        return $values;
    }

    public function getInsertSyntaxForValues($values = [])
    {
        if ($values == []) {
            $values = $this->getInsertValues();
        }
        
        return "(" . implode(', ', array_values($values)) . ")";
    }

    public function getGenericUpdateSyntax($skipNulls = false, $exclude = [])
    {
        $values = [];

        foreach ($this->getPropertiesMap($skipNulls, $exclude) as $propertyName => $value) {
            $values[] = $propertyName . '=VALUES(' . $propertyName . ')';
        }

        return implode(", ", $values);
    }

    /**
    * @param bool $skipNulls if true, null properties will be omitted
    * @return array An array mapping a property name as a named conditional to the property value
    */
    public function getPDOReadyPropertiesMap($skipNulls = true)
    {
        $values = [];
        try {
            $properties = $this->getProperties();
        } catch (ReflectionException $e) {
            Log::warning($e->getMessage());
            Log::exception($e);
            return $values;
        }

        foreach ($properties as $property) {
            $propertyValue = $this->getValueForProperty($property);
            if (!isset($propertyValue) and $skipNulls) {
                continue;
            }
            $values[':' . $property->getName()] = $propertyValue;
        }

        return $values;
    }

    /**
     * @param bool $skipNulls If true, null properties will be omitted.
     * @param array $exclude Properties specified here will be excluded.
     * @return array An array mapping a propertyName to it's value.
     */
    public function getPropertiesMap($skipNulls = true, $exclude = [])
    {
        $values = [];
        try {
            $properties = $this->getProperties();
        } catch (ReflectionException $e) {
            Log::warning($e->getMessage());
            Log::exception($e);
            return $values;
        }

        foreach ($properties as $property) {
            if (in_array($property->getName(), $exclude)) {
                continue;
            }

            $propertyValue = $this->getValueForProperty($property);
            if (!isset($propertyValue) and $skipNulls) {
                continue;
            }
            $values[$property->getName()] = $propertyValue;
        }

        return $values;
    }

    /**
     * @return array An array containing all property names.
     */
    private function getPropertiesDefinition()
    {
        $values = [];
        try {
            $properties = $this->getProperties();
        } catch (ReflectionException $e) {
            Log::warning($e->getMessage());
            Log::exception($e);
            return $values;
        }

        foreach ($properties as $property) {
            $values[] = $property->getName();
        }

        return $values;
    }

    /**
     * This method will be used to prepare queries for use with PHP's PDOStatement object
     * By using named conditionals
     * @param bool $skipNulls if true, null properties will be omitted.
     * @return array An array mapping a property name to a colon plus the property name (named conditional in a PDO statement query)
     */
    private function getPropertiesMapForBinding($skipNulls = true)
    {
        $values = [];
        try {
            $properties = $this->getProperties();
        } catch (ReflectionException $e) {
            Log::warning($e->getMessage());
            Log::exception($e);
            return $values;
        }

        foreach ($properties as $property) {
            $propertyValue = $this->getValueForProperty($property);
            if (!isset($propertyValue) and $skipNulls) {
                continue;
            }
            $values[$property->getName()] = ':' . $property->getName();
        }

        return $values;
    }

    /**
     * @return ReflectionProperty[]
     * @throws ReflectionException
     */
    private function getProperties()
    {
        $className = get_class($this);
        $reflector = new ReflectionClass($className);
        $properties = $reflector->getProperties();
        return $properties;
    }

    /**
     * @param $property ReflectionProperty
     * @return string | null
     */
    private function getValueForProperty(ReflectionProperty $property)
    {
        try {
            $getter = ucfirst($this->getPropertyNameFromColumnName($property->getName()));
            if (method_exists($this, "get" . $getter)) {
                $propertyValue = $this->{"get" . $getter}();
            } elseif (method_exists($this, "is" . ucfirst($property->getName()))) {
                $propertyValue = $this->{"is" . ucfirst($property->getName())}();
            } else {
                return null;
            }
        } catch (Throwable $e) {
            Log::warning($e->getMessage());
            return null;
        }

        return $propertyValue;
    }

    private function getPropertyNameFromColumnName($columnName)
    {
        $propertyName = str_replace('_', '', ucwords($columnName, '_'));
        $propertyName = lcfirst($propertyName);
        return $propertyName;
    }

    public function __toString()
    {
        $values = $this->getUpdateSyntax(false);
        return get_class($this) . "{" . $values . "}";
    }

    /**
     * Returns a query syntax that can be called alongside 'UPDATE TABLE SET'
     *
     * @param bool $skipNulls If true, null properties will be omitted.
     * @param $exclude array Properties here will be excluded from result
     * @return string
     */
    public function getUpdateSyntax($skipNulls = true, $exclude = [])
    {
        $values = [];

        foreach ($this->getPropertiesMap($skipNulls, $exclude) as $key => $value) {
            $values[] = $key . "=" . (isset($value) ? $value : 'null');
        }

        return implode(", ", $values);
    }

    public function getPDOReadyUpdateSyntax($skipNulls = true)
    {
        $values = [];

        foreach ($this->getPropertiesMapForBinding($skipNulls) as $key => $value) {
            $values[] = $key . "=" . (isset($value) ? $value : 'null');
        }

        return implode(", ", $values);
    }

    public function getPDOReadyInsertSyntax()
    {
        $values = $this->getPropertiesMapForBinding(true);
        return "(" . implode(", ", array_keys($values)) . ") VALUES (" . implode(', ', array_values($values)) . ")";
    }

    //============================================DATA VALIDATION============================//
}
