<?php

namespace App\Main\Controllers\Base;

use App\Main\Exceptions\RenderException;
use App\Main\Exceptions\ServiceNotFoundException;
use function App\Main\Helpers\redirect;
use App\Main\Http\Routing\Router;
use App\Main\Http\Session\SessionManager;
use App\Main\Libraries\Rendering\Renderer;
use App\Main\Libraries\ServiceLocator;
use App\Main\Http\RequestManagement\RequestDispatcher;
use App\Main\Libraries\Navigation\NavigationBuilder;
use App\Main\Libraries\ViewModel;
use RuntimeException;

/**
 * Class Controller
 *
 * A base class for handling all incoming HTTP requests.
 *
 * All public methods in this class or any of it's subclasses
 * must return their responses.
 */
abstract class Controller
{
    /**
     * @var Renderer
     */
    protected $renderer;

    /**
     * @var SessionManager
     */
    protected $sessionManager;

    /**
     * @var NavigationBuilder
     */
    private $navigationBuilder;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        try {
            $this->renderer = ServiceLocator::get(Renderer::class);
            $this->navigationBuilder = ServiceLocator::get(NavigationBuilder::class);
            $this->sessionManager = ServiceLocator::get(SessionManager::class);
        } catch (ServiceNotFoundException $e) {
            throw new RuntimeException("Renderer implementation has not been provided.", 500, $e);
        }
    }

    /**
     * @return mixed
     */
    public function index()
    {
        //TODO:// Redirect somewhere
        return 'This is from the Base Controller';
    }

    //Load view

    /**
     * @param $view
     * @param array|ViewModel $data
     * @return string The rendered html string.
     * @throws RenderException
     * @throws \App\Main\Exceptions\NavigationException
     * @see Renderer::render()
     */
    protected function render($view, $data)
    {
        $siteNav = $this->navigationBuilder->getSiteNavigation();
        $currentRequest = RequestDispatcher::getCurrentRequest();
        $nav = $siteNav->buildNavigationForUrl($currentRequest->getAbsoluteUrl(), $currentRequest->getParams());

        if (!isset($data)) {
            $data = [];
        }

        if ($data instanceof ViewModel) {
            $data->nav = $nav;
        } else {
            $data ["nav"] = $nav;
        }

        $view = $this->renderer->render($view, $data);
        return $view;
    }

    /**
     * Redirects the application to the specified handler.
     *
     * @param $controller string The owning controller to be redirected to
     * @param $handler string The name of the method to be redirected to.
     * @param array $params Optional params to pass to the new URL
     */
    protected function redirectTo($controller, $handler, $params = [])
    {
        $redirect_url = Router::getUrlForMethod($controller, $handler);

        if (!empty($params)) {
            $redirect_url .= '?';

            foreach ($params as $name => $value) {
                $redirect_url .= $name . '=' . $value . '&';
            }

            $redirect_url = substr($redirect_url, 0, -1);
        }


        redirect($redirect_url);
    }

    /**
     * Redirects the application to the specified handler within the same controller.
     *
     * @param $handler string The name of the method to be redirected to.
     * @param array $params Optional params to pass to the new URL
     */
    protected function redirectToSelf($handler, $params = [])
    {
        $this->redirectTo(get_class($this), $handler, $params);
    }
}
