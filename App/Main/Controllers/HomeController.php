<?php

namespace App\Main\Controllers;

use App\Main\Controllers\Base\Controller;
use App\Main\Exceptions\RenderException;
use function App\Main\Helpers\redirect;
use App\Main\Http\Enums\AccessRole;
use App\Main\Features\DashboardManagement\DashboardService;

class HomeController extends Controller
{
    /**
     * @var DashboardService
     */
    private $dashboardService;

    /**
     * HomeController constructor.
     * @param DashboardService $dashboardService
     */
    public function __construct(DashboardService $dashboardService)
    {
        parent::__construct();
        $this->dashboardService = $dashboardService;
    }


    /**
     * @return mixed|string
     */
    public function index()
    {
        if ($this->sessionManager->isUserLoggedIn() == true) {
            $homeController = AccessRole::HOMEPAGES[$this->sessionManager->getCurrentUserAccessrole()]["Controller"];
            $homeHandler = AccessRole::HOMEPAGES[$this->sessionManager->getCurrentUserAccessrole()]["Handler"];
            $this->redirectTo($homeController, $homeHandler);
        } else {
            $this->redirectTo("AuthController", "getLoginView");
        }
    }

    /**
     * @return string
     * @throws RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function testing()
    {
        $data = [
            "title" => "Testing"
        ];

        return $this->render('home/hello', $data);
    }
}
