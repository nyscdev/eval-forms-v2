<?php

namespace App\Main\Controllers;

use App\Main\Controllers\Base\Controller;
use App\Main\Exceptions\RenderException;

class ErrorPagesController extends Controller
{
    /**
     * @param string $message
     * @return mixed|string
     * @throws RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function displayErrorPage($message = "")
    {
        $data["processingMessage"] = $message;
        $data["title"] = "Error Page";
        return $this->render('error/oops', $data);
    }
}
