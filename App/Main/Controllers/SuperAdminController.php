<?php

namespace App\Main\Controllers;

use App\Main\Controllers\Base\Controller;

/**
 * Class SuperAdminController
 * @package App\Main\Controllers
 * @deprecated This controller will be deleted eventually
 */
class SuperAdminController extends Controller
{

    /**
     * @return string
     * @throws \App\Main\Exceptions\NavigationException
     * @throws \App\Main\Exceptions\RenderException
     */
    public function superAdminOverview()
    {
        $data = array(
            "Word" => "Hello",
            "title" => "Staff List"
        );
        return $this->render("superadmin/index", $data);
    }

    /* WEBSITE LIVE REPORTS
     * TODO: add any more methods that should be in this section
     */

    public function viewWebsiteLiveReports()
    {
    }

    /* UTILITIES
     * TODO: add more methods here for backing up data to Dropbox and refreshing the database after backup
     * in preparation for a new batch of corpers
     */
}
