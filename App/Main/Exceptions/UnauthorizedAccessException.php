<?php

namespace App\Main\Exceptions;

use Exception;
use Throwable;

class UnauthorizedAccessException extends Exception
{
    public function __construct(
        string $message = "You are not authorized to access this resource.",
        int $code = 401,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
