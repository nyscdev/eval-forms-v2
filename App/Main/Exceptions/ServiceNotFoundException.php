<?php

namespace App\Main\Exceptions;

use Exception;
use Throwable;

class ServiceNotFoundException extends Exception
{
    public function __construct(
        string $message = "Requested service was not found.",
        int $code = 500,
        Throwable $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
