<?php

namespace App\Main\Exceptions;

use RuntimeException;
use Throwable;

class CyclicDependencyException extends RuntimeException
{
    public function __construct(string $message = "Internal Server Error", int $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
