<?php

namespace App\Main\Exceptions;

use Exception;
use Throwable;

class NavigationException extends Exception
{
    public function __construct(string $message = "There is a problem with the navigation for this page", int $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
