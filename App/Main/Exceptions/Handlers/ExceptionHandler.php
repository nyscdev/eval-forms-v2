<?php

namespace App\Main\Exceptions\Handlers;

use App\Main\Features\Authentication\AuthController;
use App\Main\exceptions\DatabaseException;
use App\Main\Exceptions\NotFoundException;
use App\Main\Exceptions\UnauthorizedAccessException;
use App\Main\Libraries\Log;
use App\Main\Http\Routing\Router;
use function App\Main\Helpers\redirect;
use Exception;
use RuntimeException;
use function App\Main\Helpers\get_short_class;
use Throwable;

class ExceptionHandler
{
    public static function handleException(Throwable $throwable)
    {
        $exceptionType = get_short_class($throwable);
        Log::info("Handling " . $exceptionType);

        if ($exceptionType === Exception::class) {
            self::handleGenericException($throwable);
            return;
        }

        $handlerMethod = 'handle' . $exceptionType;
        if (method_exists(self::class, $handlerMethod)) {
            call_user_func_array([self::class, $handlerMethod], [$throwable]);
        } else {
            self::handleGenericException($throwable);
        }
    }

    protected static function handleGenericException(Throwable $throwable)
    {
        Log::exception($throwable);
        $errorUrl = Router::getUrlForMethod("ErrorPagesController", "displayErrorPage");
        redirect($errorUrl);
    }

    protected static function handleNotFoundException(NotFoundException $exception)
    {
        Log::info("Handling Not Found.");
        Log::info($exception->getMessage());
        Log::exception($exception);
        $message = "404 : The resource you are trying to access does not exist. " . $exception->getMessage();
        $errorUrl = Router::getUrlForMethod("ErrorPagesController", "displayErrorPage") . "?message=" . $message;
        redirect($errorUrl);
    }

    protected static function handleUnauthorizedAccessException(UnauthorizedAccessException $exception)
    {
        Log::exception($exception);
        $url = Router::getUrlForMethod(AuthController::class, 'getLoginView') . "?message=" . $exception->getMessage();
        redirect($url);
    }

    protected static function handleRuntimeException(RuntimeException $exception)
    {
        $message = $exception->getMessage();
        Log::exception($exception);
        $errorUrl = Router::getUrlForMethod("ErrorPagesController", "displayErrorPage") . "?message=" . $message;
        redirect($errorUrl);
    }

    protected static function handleDatabaseException(DatabaseException $exception)
    {
        $message = $exception->getMessage();
        Log::exception($exception);
        $errorUrl = Router::getUrlForMethod("ErrorPagesController", "displayErrorPage") . "?message=" . $message;
        redirect($errorUrl);
    }
}
