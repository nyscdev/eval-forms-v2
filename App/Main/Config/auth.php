<?php
/**
 * IMPORTANT:
 * WE CURRENTLY DON'T MAKE USE OF THIS CLASS, NOT SURE IF ANYTHING WILL BREAK IF WE DELETE IT SO IT'S STILL HERE FOR NOW
 * BUT AS FAR AS I CAN TELL, IT'S NOT BEING USED IN ANY PART OF THE APPLICATION
 */

namespace App\Main\Config;

//Session Key
define('AUTH', 'nysc.evaluations.authorities');

//======================== AUTHORITIES ==========================//

define('AUTH_CAN_VIEW_CORPER_DASHBOARD', 0);
define('AUTH_CAN_VIEW_STAFF_DASHBOARD', 1);
define('AUTH_CAN_CREATE_FORM', 2);
define('AUTH_CAN_VIEW_FORM', 3);
define('AUTH_CAN_ANSWER_QUESTION', 4);

//TODO://Add or remove relevant authorities.


//=== ROLES (A role in this context is a group of authorities) ===//

define('AUTH_ROLE_CORPER', [
    AUTH_CAN_ANSWER_QUESTION,
    AUTH_CAN_VIEW_CORPER_DASHBOARD,
    AUTH_CAN_VIEW_FORM,
]);

define('AUTH_ROLE_STAFF', [
    AUTH_CAN_VIEW_STAFF_DASHBOARD,
    AUTH_CAN_VIEW_FORM,
]);

define('AUTH_ROLE_ADMIN', [
    AUTH_CAN_CREATE_FORM,
    AUTH_CAN_VIEW_FORM,
]);

//TODO://Add or remove relevant roles with relevant authorities.
