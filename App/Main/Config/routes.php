<?php

namespace App\Main\Config;

use App\Main\Features\Authentication\AuthController;
use App\Main\Controllers\SuperAdminController;
use App\Main\Controllers\HomeController;
use App\Main\Controllers\ErrorPagesController;
use App\Main\Features\AnswerManagement\AnswerController;
use App\Main\Features\DataManagement\CloudServiceProviders\DropBox\DropBoxApiController;
use App\Main\Features\FormManagement\FormController;
use App\Main\Features\FormSectionManagement\FormSectionController;
use App\Main\Features\CorperManagement\CorperController;
use App\Main\Features\QuestionManagement\QuestionController;
use App\Main\Features\RoleManagement\RoleController;
use App\Main\Features\StaffManagement\StaffController;
use App\Main\Features\FormResponseManagement\FormResponseController;
use App\Main\Features\FormStatusManagement\FormStatusController;
use App\Main\Http\Enums\HTTPMethod;
use App\Main\Features\DataAnalysis\AnalysisController;
use App\Main\Features\DataAnalysis\LiveStatsController;
use App\Main\Http\Routing\Router;
use App\Main\Features\DataManagement\DataManagementController; 

//======================================ROUTES FOR THE APPLICATION============================//
//Router::register('/superadmin/staff/')->withController(SuperAdminController::class)->withHandler('viewStaffList');

Router::register('/home')->withController(HomeController::class)->withHandler('testing');
Router::register('/home/index')->withController(HomeController::class);
Router::register('/')->withController(HomeController::class)->withHandler('index');

Router::registerForController(StaffController::class, '/staff/')
    ->addMapping('/all/', 'viewStaff')
    ->addMapping('/by-user-state/', 'viewStaffForCurrentUserState')
    ->addMapping('/create/', 'getStaffCreationView')
    ->addMapping('/create/', 'createStaff', HTTPMethod::POST)
    ->addMapping('/edit/', 'getEditStaffView')
    ->addMapping('/edit/', 'editStaff', HTTPMethod::POST)
    ->addMapping('/delete/', 'deleteStaff')
    ->addMapping('/reset-password/', 'getResetPasswordView')
    ->addMapping('/reset-password/', 'resetPassword', HTTPMethod::POST);

Router::registerForController(AuthController::class, '/auth/')
    ->addMapping('/login/', 'getLoginView')
    ->addMapping('/login-post/', 'login', HTTPMethod::POST)
    ->addMapping('/logout-user/', 'logout');

Router::registerForController(CorperController::class, '/corpers/')
    ->addMapping('/upload-spreadsheet/', 'getUploadSpreadsheetView')
    ->addMapping('/upload-spreadsheet/', 'uploadCorperData', HTTPMethod::POST);

Router::registerForController(FormResponseController::class, '/respond/')
    ->addMapping('/', 'getFormViewForUserRole')
    ->addMapping('/submit/', 'submitFormResponse', HTTPMethod::POST)
    ->addMapping('/my-forms/', 'getFormsViewForCurrentUser');

Router::registerForController(RoleController::class, 'roles')
    ->addMapping('/all/', 'viewRoles')
    ->addMapping('/create/', 'getCreateRoleView')
    ->addMapping('/create/', 'createRole', HTTPMethod::POST)
    ->addMapping('/edit/', 'getEditRoleView')
    ->addMapping('/edit/', 'editRole', HTTPMethod::POST)
    ->addMapping('/delete/', 'deleteRole');

Router::registerForController(FormController::class, 'forms')
    ->addMapping('/all/', 'viewForms')
    ->addMapping('/view/', 'viewForm')
    ->addMapping('/create/', 'getCreateFormView')
    ->addMapping('/create/', 'createForm', HTTPMethod::POST)
    ->addMapping('/edit/', 'getEditFormView')
    ->addMapping('/edit/', 'editForm', HTTPMethod::POST)
    ->addMapping('/delete/', 'deleteForm');

Router::registerForController(FormSectionController::class, 'sections')
    ->addMapping('/for-form/', 'viewSectionsForForm')
    ->addMapping('/create/', 'getCreateSectionView')
    ->addMapping('/create/', 'createSection', HTTPMethod::POST)
    ->addMapping('/edit/', 'getEditSectionView')
    ->addMapping('/edit/', 'editSection', HTTPMethod::POST)
    ->addMapping('/delete/', 'deleteSection');

Router::registerForController(QuestionController::class, 'questions')
    ->addMapping('/for-section/', 'viewQuestionsForSection')
    ->addMapping('/create/', 'getCreateQuestionViewForSection')
    ->addMapping('/create/', 'createQuestion', HTTPMethod::POST)
    ->addMapping('/edit/', 'getEditQuestionView')
    ->addMapping('/edit/', 'editQuestion', HTTPMethod::POST)
    ->addMapping('/delete/', 'deleteQuestion');

Router::registerForController(AnswerController::class, 'answer-options')
    ->addMapping('/for-question/', 'viewAnswerOptionsForQuestion')
    ->addMapping('/create/', 'getCreateAnswerOptionView')
    ->addMapping('/create/', 'createAnswerOption', HTTPMethod::POST)
    ->addMapping('/edit/', 'getEditAnswerOptionView')
    ->addMapping('/edit/', 'editAnswerOption', HTTPMethod::POST)
    ->addMapping('/delete/', 'deleteAnswerOption');

Router::registerForController(ErrorPagesController::class, '/error/')
    ->addMapping('/oops/', 'displayErrorPage');

Router::registerForController(FormStatusController::class, '/form-status/')
    ->addMapping('/all/', 'getFormStatusListView')
    ->addMapping('/activate/', 'activateFormForRole')
    ->addMapping('/deactivate/', 'deactivateFormForRole');

Router::registerForController(AnalysisController::class, 'analysis')
    ->addMapping('/form-data/', 'getAnalyzeCorperDataView')
    ->addMapping('/form-questions/', 'getQuestionsForForm')
    ->addMapping('/sections/', 'getCorperSectionsForForm')
    ->addMapping('/questions/', 'getQuestionsForSection')
    ->addMapping('/response/', 'getResponseAnalysisForQuestion')
    ->addMapping('/form-data-analyst/', 'getAnalyzeCorperDataViewForAnalysts');

Router::registerForController(LiveStatsController::class, 'stats')
    ->addMapping('/livestats-view/', 'getLiveStatsView')
    ->addMapping('/livestats', 'getLiveStats')
    ->addMapping('/livestats-analyst', 'getLiveStatsForAnalystView')
    ->addMapping('/livestats-stateAdmin', 'getLiveStatsForStateAdminView')
    ->addMapping('/timestamp/', 'createTimestampForCurrentUser', HTTPMethod::POST)
    ->addMapping('/timestamp-data/', 'getTimestampForCurrentUser');
    
Router::registerForController(DataManagementController::class, 'data')
    ->addMapping('/manage/', 'getDataManagementView')
    ->addMapping('/backup/', 'backUpToDropbox')
    ->addMapping('/clear-livestats/', 'clearLivestats')
    ->addMapping('/clear-responses/', 'clearResponses')
    ->addMapping('/clear-corpers/', 'clearCorpers');  

//================== DROPBOX ====================//
Router::registerForController(DropBoxApiController::class, '/api/dropbox/auth/')
    ->addMapping('callback', 'dropBoxApiCallback');
