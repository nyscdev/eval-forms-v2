<?php
namespace App\Main\Config;

use PDO;

//error_reporting(E_ALL & ~E_NOTICE);
error_reporting(E_STRICT);
define("APP_ROOT", dirname(__FILE__, 3));
define("SITE_ROOT", dirname(__FILE__, 4));
// Url root
define("URL_ROOT", (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST']  . '/eval-forms-v2');
define("HOME_URL", "/eval-forms-v2/");
// Define Site name
define("SITE_NAME", "NYSC Evaluation");
define('VIEW_ROOT', APP_ROOT . '/views');


// Define database parameters
define("LOGS_ROOT", "logs/nysc_evaluation.log");
define("DB_CONNECTION", "mysql");
define("DB_USERNAME", "root");
define("DB_PASSWORD", "");
define("DB_NAME", "nysc_evaluation");
define("DB_HOST", "127.0.0.1");
define("DB_PORT", "3306");
define("DB_OPTIONS", [
    PDO::ATTR_PERSISTENT => true,
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
]);

date_default_timezone_set('UTC');

//Dropbox Params
define('DROP_BOX_APP_KEY', '6jb99jfkyv1n4ku'); /*TODO: Replace with correct App Key From [https://www.dropbox.com/developers/apps]*/;
define('DROP_BOX_APP_SECRET', 'z0q8wtcjkl4mwfo'); /*TODO: Replace with correct App Secret From [https://www.dropbox.com/developers/apps]*/;
define('DROP_BOX_TOKEN_STORE', 'nysc.evaluations.cloudservice.dropbox.access_token');

define('MODE', 'PRODUCTION');
