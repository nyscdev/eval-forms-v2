<?php

namespace App\Main\Features\QuestionManagement\ViewModels;


use App\Main\Libraries\ViewModel;
use App\Main\Models\Enums\AnswerFormat;
use App\Main\Models\Form;
use App\Main\Models\Question;
use App\Main\Models\Section;

class QuestionDetailsViewModel extends ViewModel
{

    /**
     * @var Form
     */
    public $form;

    /**
     * @var Section
     */
    public $section;

    /**
     * @var Question
     */
    public $question;

    /**
     * @var array An array containing all possible answer formats
     *
     * Defaults to all answer formats available.
     */
    public $answerFormats;

    /**
     * QuestionDetailsViewModel constructor.
     * @param $title
     * @param array $answerFormats
     */
    public function __construct($title, $answerFormats = [])
    {
        parent::__construct($title);

        if (empty($answerFormats)) {
            $constants = AnswerFormat::getConstants();
            $this->answerFormats = [];

            foreach ($constants as $key => $value) {
                $this->answerFormats[] = array(
                    "formatName" => $key,
                    "formatValue" => $value
                );
            }
        }
    }
}