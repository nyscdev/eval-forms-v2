<?php

namespace App\Main\Features\QuestionManagement\ViewModels;


use App\Main\Libraries\ViewModel;
use App\Main\Models\Form;
use App\Main\Models\Section;

class QuestionsListViewModel extends ViewModel
{
    /**
     * @var Form
     */
    public $form;

    /**
     * @var Section
     */
    public $section;

    /**
     * @var array
     */
    public $questions;
}