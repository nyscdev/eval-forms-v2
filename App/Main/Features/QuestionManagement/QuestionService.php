<?php

namespace App\Main\Features\QuestionManagement;


use App\Main\Models\Enums\AnswerFormat;
use App\Main\Models\Question;
use App\Main\Repositories\AnswerOptionRepository;
use App\Main\Repositories\QuestionRepository;

class QuestionService
{
    private $questionRepository;
    private $answerOptionsRepository;

    /**
     * QuestionService constructor.
     * @param $questionRepository QuestionRepository
     * @param AnswerOptionRepository $answerOptionRepository
     */
    public function __construct(QuestionRepository $questionRepository, AnswerOptionRepository $answerOptionRepository)
    {
        $this->questionRepository = $questionRepository;
        $this->answerOptionsRepository = $answerOptionRepository;
    }

    public function getQuestionsForSection($sectionId)
    {
        return $this->questionRepository->findBySectionId($sectionId);
    }

    /**
     * @param $questionId
     * @return Question
     */
    public function getQuestion($questionId)
    {
        $question = $this->questionRepository->findById($questionId);
        assert($question instanceof Question);
        return $question;
    }

    /**
     * @param $parentId
     * @return array
     */
    public function getQuestionsWithParentId($parentId)
    {
        return $this->questionRepository->findByParentQuestionId($parentId);
    }

    public function createQuestion($sectionId, $questionText, $answerFormat)
    {
        $question = new Question();
        $question->setSectionId($sectionId);
        $question->setQuestion($questionText);
        $question->setAnswerFormat($answerFormat);
        $this->questionRepository->save($question);

        return $question;
    }

    public function editQuestion($questionId, $questionText, $answerFormat)
    {
        $question = $this->getQuestion($questionId);
        $this->updateAnswerFormatForQuestion($question, $answerFormat);
        $question->setQuestion($questionText);
        $this->questionRepository->save($question);

        return $question;
    }

    private function updateAnswerFormatForQuestion(Question $question, $answerFormat)
    {
        if ($question->getAnswerFormat() == $answerFormat) {
            return;
        }

        if ($answerFormat == AnswerFormat::RADIO_BOX || $answerFormat == AnswerFormat::DROP_DOWN) {
            $this->answerOptionsRepository->deleteByQuestionId($question->getId());
        }

        $question->setAnswerFormat($answerFormat);
    }

    public function deleteQuestion($questionId)
    {
        $this->questionRepository->deleteById($questionId);
    }
}