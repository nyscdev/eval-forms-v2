<?php

namespace App\Main\Features\QuestionManagement;

use App\Main\Controllers\Base\Controller;
use App\Main\Features\FormManagement\FormService;
use App\Main\Features\FormSectionManagement\FormSectionService;
use App\Main\Features\QuestionManagement\ViewModels\QuestionDetailsViewModel;
use App\Main\Features\QuestionManagement\ViewModels\QuestionsListViewModel;

class QuestionController extends Controller
{
    private $questionService;
    private $formService;
    private $sectionService;

    /**
     * QuestionController constructor.
     * @param QuestionService $questionService
     * @param FormService $formService
     * @param FormSectionService $sectionService
     */
    public function __construct(QuestionService $questionService, FormService $formService, FormSectionService $sectionService)
    {
        parent::__construct();
        $this->questionService = $questionService;
        $this->formService = $formService;
        $this->sectionService = $sectionService;
    }

    /**
     * @param $sectionId
     * @return string
     * @throws \App\Main\Exceptions\RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function viewQuestionsForSection($sectionId)
    {
        $section = $this->sectionService->getSection($sectionId);
        $form = $this->formService->getForm($section->getFormId());
        $questions = $this->questionService->getQuestionsForSection($sectionId);

        $questionsViewModel = new QuestionsListViewModel("List of Questions");
        $questionsViewModel->form = $form;
        $questionsViewModel->section = $section;
        $questionsViewModel->questions = $questions;

        //TODO: Replace view path
        return $this->render('/superadmin/form/question/list-questions', $questionsViewModel);
    }

    /**
     * @param $sectionId
     * @return string
     * @throws \App\Main\Exceptions\RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function getCreateQuestionViewForSection($sectionId)
    {
        $section = $this->sectionService->getSection($sectionId);
        $form = $this->formService->getForm($section->getFormId());

        $questionViewModel = new QuestionDetailsViewModel("Create Question");
        $questionViewModel->section = $section;
        $questionViewModel->form = $form;

        //TODO: Replace view path
        return $this->render('/superadmin/form/question/create-question', $questionViewModel);
    }

    public function createQuestion($sectionId, $questionText, $answerFormat)
    {
        $this->questionService->createQuestion($sectionId, $questionText, $answerFormat);
        $this->redirectToSelf('viewQuestionsForSection', ["sectionId" => $sectionId]);
    }

    /**
     * @param $questionId
     * @return string
     * @throws \App\Main\Exceptions\RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function getEditQuestionView($questionId)
    {
        $question = $this->questionService->getQuestion($questionId);
        $section = $this->sectionService->getSection($question->getSectionId());
        $form = $this->formService->getForm($section->getFormId());

        $questionViewModel = new QuestionDetailsViewModel("Edit Question");
        $questionViewModel->question = $question;
        $questionViewModel->form = $form;
        $questionViewModel->section = $section;

        //TODO: Replace view path
        return $this->render('/superadmin/form/question/edit-question', $questionViewModel);
    }

    public function editQuestion($questionId, $questionText, $answerFormat)
    {
        $question = $this->questionService->editQuestion($questionId, $questionText, $answerFormat);
        $this->redirectToSelf('viewQuestionsForSection', ["sectionId" => $question->getSectionId()]);
    }

    public function deleteQuestion($questionId)
    {
        $question = $this->questionService->getQuestion($questionId);
        $this->questionService->deleteQuestion($questionId);
        $this->redirectToSelf('viewQuestionsForSection', ["sectionId" => $question->getSectionId()]);
    }
}
