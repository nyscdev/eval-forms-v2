<?php

namespace App\Main\Features\FormSectionManagement\ViewModels;


use App\Main\Libraries\ViewModel;
use App\Main\Models\Form;

class SectionsListViewModel extends ViewModel
{
    /**
     * @var array
     */
    public $sections;

    /**
     * @var Form
     */
    public $form;

    /**
     * @var array
     */
    public $roles;
}