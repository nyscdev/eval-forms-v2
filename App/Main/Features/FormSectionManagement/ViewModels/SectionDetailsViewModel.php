<?php

namespace App\Main\Features\FormSectionManagement\ViewModels;


use App\Main\Libraries\ViewModel;
use App\Main\Models\Form;
use App\Main\Models\Section;

class SectionDetailsViewModel extends ViewModel
{
    /**
     * @var Section
     */
    public $section;

    /**
     * @var Form
     */
    public $form;

    /**
     * @var array
     */
    public $roles;
}