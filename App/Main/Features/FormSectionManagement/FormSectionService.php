<?php

namespace App\Main\Features\FormSectionManagement;

use App\Main\Models\Section;
use App\Main\Repositories\SectionRepository;

class FormSectionService
{
    private $sectionRepository;

    /**
     * FormSectionService constructor.
     * @param $sectionRepository SectionRepository
     */
    public function __construct(SectionRepository $sectionRepository)
    {
        $this->sectionRepository = $sectionRepository;
    }

    public function getSectionsForForm($formId)
    {
        return $this->sectionRepository->findByFormId($formId);
    }

    public function getCorperSectionsForForm($formId)
    {
        return $this->sectionRepository->findByFormIdForCorper($formId);
    }

    /**
     * @param $sectionId
     * @return Section
     */
    public function getSection($sectionId)
    {
        $section = $this->sectionRepository->findById($sectionId);
        assert($section instanceof Section);
        return $section;
    }

    public function createSection($formId, $roleId, $title, $description)
    {
        $section = new Section();
        $section->setFormId($formId);
        $section->setRoleId($roleId);
        $section->setTitle($title);
        $section->setDescription($description);

        $this->sectionRepository->save($section);
        return $section;
    }

    /**
     * @param $sectionId
     * @param $roleId
     * @param $title
     * @param $description
     * @return Section;
     */
    public function editSection($sectionId, $roleId, $title, $description)
    {
        $section = $this->sectionRepository->findById($sectionId);
        assert($section instanceof Section);
        $section->setRoleId($roleId);
        $section->setTitle($title);
        $section->setDescription($description);

        $this->sectionRepository->save($section);
        return $section;
    }

    public function delete($sectionId)
    {
        $this->sectionRepository->deleteById($sectionId);
    }
}
