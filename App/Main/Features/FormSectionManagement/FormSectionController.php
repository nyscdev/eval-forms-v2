<?php

namespace App\Main\Features\FormSectionManagement;


use App\Main\Controllers\Base\Controller;
use App\Main\Features\FormManagement\FormService;
use App\Main\Features\FormSectionManagement\ViewModels\SectionDetailsViewModel;
use App\Main\Features\FormSectionManagement\ViewModels\SectionsListViewModel;
use App\Main\Features\RoleManagement\RoleService;
use App\Main\Models\Section;

class FormSectionController extends Controller
{
    private $formSectionService;
    private $formService;
    private $roleService;

    /**
     * FormSectionController constructor.
     * @param FormSectionService $formSectionService
     * @param FormService $formService
     * @param RoleService $roleService
     */
    public function __construct(FormSectionService $formSectionService, FormService $formService, RoleService $roleService)
    {
        parent::__construct();
        $this->formSectionService = $formSectionService;
        $this->formService = $formService;
        $this->roleService = $roleService;
    }

    /**
     * @param $formId
     * @return string
     * @throws \App\Main\Exceptions\RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function viewSectionsForForm($formId)
    {
        $sections = $this->formSectionService->getSectionsForForm($formId);
        $roles = $this->roleService->getRoles();
        $form = $this->formService->getForm($formId);

        $sectionsViewModel = new SectionsListViewModel("List of Sections");
        $sectionsViewModel->sections = $sections;
        $sectionsViewModel->roles = $roles;
        $sectionsViewModel->form = $form;

        return $this->render('superadmin/form/section/list-sections', $sectionsViewModel);
    }

    /**
     * @param $formId
     * @return string
     * @throws \App\Main\Exceptions\RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function getCreateSectionView($formId)
    {
        $roles = $this->roleService->getRoles();
        $form = $this->formService->getForm($formId);

        $sectionViewModel = new SectionDetailsViewModel("Create Section");
        $sectionViewModel->roles = $roles;
        $sectionViewModel->form = $form;

        return $this->render('superadmin/form/section/create-section', $sectionViewModel);
    }

    /**
     * @param $formId
     * @param $roleId
     * @param $title
     * @param $description
     */
    public function createSection($formId, $roleId, $title, $description)
    {
        $this->formSectionService->createSection($formId, $roleId, $title, $description);
        $this->redirectToSelf('viewSectionsForForm', ["formId" => $formId]);
    }

    /**
     * @param $sectionId
     * @return string
     * @throws \App\Main\Exceptions\RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function getEditSectionView($sectionId)
    {
        $section = $this->formSectionService->getSection($sectionId);
        assert($section instanceof Section);
        $form = $this->formService->getForm($section->getFormId());
        $roles = $this->roleService->getRoles();

        $sectionViewModel = new SectionDetailsViewModel("Edit Section");
        $sectionViewModel->form = $form;
        $sectionViewModel->roles = $roles;
        $sectionViewModel->section = $section;

        return $this->render("superadmin/form/section/edit-section", $sectionViewModel);
    }

    public function editSection($sectionId, $roleId, $title, $description)
    {
        $section = $this->formSectionService->editSection($sectionId, $roleId, $title, $description);
        $this->redirectToSelf('viewSectionsForForm', ["formId" => $section->getFormId()]);
    }

    public function deleteSection($sectionId)
    {
        $section = $this->formSectionService->getSection($sectionId);
        $this->formSectionService->delete($sectionId);
        $this->redirectToSelf('viewSectionsForForm', ["formId" => $section->getFormId()]);
    }
}