<?php
/**
 * Created by PhpStorm.
 * User: aueloka
 * Date: 2019-01-24
 * Time: 11:59
 */

namespace App\Main\Features\RoleManagement\ViewModels;


use App\Main\Libraries\ViewModel;

class RolesListViewModel extends ViewModel
{
    /**
     * @var array A list of roles
     */
    public $roles;
}