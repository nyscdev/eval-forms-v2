<?php

namespace App\Main\Features\RoleManagement\ViewModels;


use App\Main\Libraries\ViewModel;

class RoleDetailViewModel extends ViewModel
{
    /**
     * @var string The name of the role
     */
    public $role;
}