<?php
/**
 * Created by PhpStorm.
 * User: aueloka
 * Date: 1/23/2019
 * Time: 2:57 PM
 */

namespace App\Main\Features\RoleManagement;

use App\Main\Models\Role;
use App\Main\Repositories\RoleRepository;

class RoleService
{
    private $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function getRoles()
    {
        return $this->roleRepository->findAll();
    }

    /**
     * @param $roleId
     * @return Role
     */
    public function getRole($roleId)
    {
        $role = $this->roleRepository->findById($roleId);
        assert($role instanceof Role);
        return $role;
    }

    /**
     * @param $roleName
     * @return Role
     */
    public function getRoleWithName($roleName)
    {
        $role = $this->roleRepository->findByRole($roleName);
        assert($role instanceof Role);
        return $role;
    }
    
    public function createRole($roleName, $description)
    {
        $role = new Role();
        $role->setRole($roleName);
        $role->setDescription($description);
        $this->roleRepository->save($role);

        return $role;
    }

    public function editRole($roleName, $description, $roleId)
    {
        $role = $this->roleRepository->findById($roleId);
        assert($role instanceof Role);
        $role->setRole($roleName);
        $role->setDescription($description);
        $this->roleRepository->save($role);

        return $role;
    }

    public function deleteRole($roleId)
    {
        $this->roleRepository->deleteById($roleId);
    }
}
