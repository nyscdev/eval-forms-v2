<?php

namespace App\Main\Features\RoleManagement;

use App\Main\Controllers\Base\Controller;
use App\Main\Features\RoleManagement\ViewModels\RoleDetailViewModel;
use App\Main\Features\RoleManagement\ViewModels\RolesListViewModel;

class RoleController extends Controller
{
    private $roleService;

    public function __construct(RoleService $roleService)
    {
        parent::__construct();
        $this->roleService = $roleService;
    }

    /**
     * @return string
     * @throws \App\Main\Exceptions\RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function viewRoles()
    {
        $roles = $this->roleService->getRoles();
        $rolesView = new RolesListViewModel("List of Roles");
        $rolesView->roles = $roles;

        //TODO: Change view directory
        return $this->render('superadmin/role/list-roles', $rolesView);
    }

    /**
     * @return string
     * @throws \App\Main\Exceptions\RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function getCreateRoleView()
    {
        $data = array(
            "Word" => "Hello",
            "title" => "Staff List"
        );

        return $this->render("superadmin/role/create-role", $data);
    }

    public function createRole($role_name, $description)
    {
        $this->roleService->createRole($role_name, $description);
        $this->redirectToSelf('viewRoles');
    }

    /**
     * @param $roleId
     * @return string
     * @throws \App\Main\Exceptions\RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function getEditRoleView($roleId)
    {
        $role = $this->roleService->getRole($roleId);

        $roleViewModel = new RoleDetailViewModel();
        $roleViewModel->role = $role;
        $roleViewModel->title = "Edit Role";

        //TODO: Change view directory
        return $this->render("superadmin/role/edit-role", $roleViewModel);
    }

    public function editRole($role_name, $description, $roleId)
    {
        $this->roleService->editRole($role_name, $description, $roleId);
        $this->redirectToSelf('viewRoles');
    }

    public function deleteRole($roleId)
    {
        $this->roleService->deleteRole($roleId);
        $this->redirectToSelf('viewRoles');
    }
}
