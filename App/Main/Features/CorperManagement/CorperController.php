<?php

namespace App\Main\Features\CorperManagement;

use App\Main\Controllers\Base\Controller;
use App\Main\Features\StaffManagement\StaffController;
use App\Main\Libraries\ViewModel;


class CorperController extends Controller
{
    /**
     * @var CorperService
     */
    private $corperService;

    /**
     * CorperController constructor.
     * @param CorperService $corperService
     */
    public function __construct(CorperService $corperService)
    {
        parent::__construct();
        $this->corperService = $corperService;
    }


    /**
     * @return string
     * @throws \App\Main\Exceptions\RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function getUploadSpreadsheetView($message = "", $messageType = "success")
    {
        $viewModel = new ViewModel("Upload Corper Spreadsheet", $message, $messageType);
        return $this->render('corpers-data/upload-spreadsheet', $viewModel);
    }

    /**
     * @param $corperSpreadsheet
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function uploadCorperData($corperSpreadsheet)
    {
        $uploadedFileDirectory = $this->getUploadedFileDirectory($corperSpreadsheet);
        $message = $this->corperService->processCorperSpreadSheet($uploadedFileDirectory);
        $messageType = "success";

        if ($message != null || $message != "") {
            $messageType = "error";
        } else {
            $message = "Successfully Uploaded Corpers to the database!";
        }

        $this->redirectToSelf('getUploadSpreadsheetView', [
            "message" => $message,
            "messageType" => $messageType
        ]);
    }

    private function getUploadedFileDirectory($sheetInfo): string
    {
        $fileName = \filter_var($sheetInfo['name'], FILTER_SANITIZE_STRING);
        $currentDirectory = \dirname(__FILE__);
        $spreadsheetDir = join(DIRECTORY_SEPARATOR, [$currentDirectory, $fileName]);
        move_uploaded_file($sheetInfo['tmp_name'], $spreadsheetDir);

        return $spreadsheetDir;
    }
}
