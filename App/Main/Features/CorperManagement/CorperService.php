<?php

namespace App\Main\Features\CorperManagement;

use App\Main\Exceptions\NotFoundException;
use PhpOffice\PhpSpreadsheet\IOFactory;
use App\Main\Models\User;
use App\Main\Models\Corper;
use App\Main\Models\Enums\State;
use App\Main\Repositories\UserRepository;
use App\Main\Repositories\CorperRepository;
use App\Main\Features\RoleManagement\RoleService;

class CorperService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var CorperRepository
     */
    private $corperRepository;

    /**
     * @var RoleService
     */
    private $roleService;

    /**
     * CorperService constructor.
     *
     * @param UserRepository $userRepository
     * @param CorperRepository $corperRepository
     */
    public function __construct(
        UserRepository $userRepository,
        CorperRepository $corperRepository,
        RoleService $roleService
    ) {
        $this->userRepository = $userRepository;
        $this->corperRepository = $corperRepository;
        $this->roleService = $roleService;
    }

    /**
     * @param $corperSpreadsheet
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function processCorperSpreadSheet($uploadedFileDirectory)
    {
        list($spreadsheetDir, $sheetData) = $this->getSpreadsheetData($uploadedFileDirectory);
        $errorMessage = $this->validateSpreadsheetFormat($sheetData[1]);

        if ($errorMessage != null) {
            \unlink($spreadsheetDir);
            return $errorMessage;
        }

        $corpers = [];

        // extract and save data for each corper
        foreach ($sheetData as $corperData) {
            list($user, $corper) = $this->extractDataFromRow($corperData);

            if($this->userRepository->findByUsername($user->getUsername()) != null){
                continue;
            }

            $user = $this->userRepository->save($user);
            assert($user instanceof User);

            $corper->setId($user->getId());
            $corpers[] = $corper;
        }

        if(!empty($corpers)){
            $this->corperRepository->saveMultiple($corpers);
        }


        // remove the file from the directory to free up space when the processing is done
        \unlink($spreadsheetDir);
    }

    /**
     * @param $sheetInfo
     *
     * @return array
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    private function getSpreadsheetData($spreadsheetDir): array
    {
        $spreadsheet = IOFactory::load($spreadsheetDir);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        return array($spreadsheetDir, $sheetData);
    }


    /**
     * @param $rowData
     *
     * @return array
     */
    private function extractDataFromRow($rowData): array
    {
        // Get user ID from last 4 digits of state code and stae abbreviation
        $stateCode = $rowData['A'];
        $stateCodeArray = explode('/', $stateCode);
        $stateAbbreviation = $stateCodeArray[0];
        $state = State::getStateName($stateAbbreviation);

        // save user details
        $user = new User();
        $user->setUsername($stateCode);
        $user->setPassword($rowData['B']);
        $user->setRoleId($this->roleService->getRoleWithName('Corper')->getId());
        $user->setAccessRole('Corper');
        $user->setState($state);

        // Save Corper details
        $corperArray = $this->extractCorperDetails($rowData);
        $corper = $this->createCorper($corperArray);
        $user->setPassword(strtolower($corperArray['last_name']));

        return array($user, $corper);
    }

    private function extractCorperDetails($corperData)
    {
        $fullName = $corperData['C'];
        $fullNameArray = explode(' ', $fullName);
        $firstName = '';
        $middleName = '';
        $lastName = '';
        if (count($fullNameArray) == 2) {
            $lastName = $fullNameArray[0];
            $firstName = $fullNameArray[1];
        } elseif (count($fullNameArray) > 2) {
            $lastName = $fullNameArray[0];
            $middleName = $fullNameArray[1];
            $firstName = $fullNameArray[2];
        }

        //Convert date from dd/mm/yyyy format to yyyy-mm-dd format which is the accepted MYSQL date format
        $dateOfBirth = $corperData['H'];
        $dateArray = explode('/', $dateOfBirth);
        $dateArray = array_reverse($dateArray);
        $newDateOfBirth = implode('-', $dateArray);

        return array(
            'state_code' => $corperData['A'],
            'call_up_number' => $corperData['B'],
            'first_name' => $firstName,
            'middle_name' => $middleName,
            'last_name' => $lastName,
            'gender' => $corperData['D'],
            'institution_name' => $corperData['E'],
            'course_of_study' => $corperData['F'],
            'qualification' => $corperData['G'],
            'date_of_birth' => $newDateOfBirth,
            'platoon_id' => (int)$corperData['I'],
        );
    }

    private function createCorper($corperArray)
    {
        $corper = new Corper();
        $corper->setStateCode($corperArray['state_code']);
        $corper->setCallUpNumber($corperArray['call_up_number']);
        $corper->setDateOfBirth($corperArray['date_of_birth']);
        $corper->setInstitutionName($corperArray['institution_name']);
        $corper->setQualification($corperArray['qualification']);
        $corper->setFirstName($corperArray['first_name']);
        $corper->setMiddleName($corperArray['last_name']);
        $corper->setLastName($corperArray['middle_name']);
        $corper->setPlatoonId($corperArray['platoon_id']);

        return $corper;
    }

    /**
     * Validates that the format of data in the first row of the spreadsheet is the exact format required by the application.
     *
     * @param array
     *
     * @return mixed
     * @since 1.0.0
     *
     */
    private function validateSpreadsheetFormat($firstRowOfSpreadsheet)
    {
        $errorMessage = "1.) Error! Please ensure you remove headings before upload" .
            "<br/><br/>2.) Error! Check to confirm that your spreadsheet has ONLY these columns in the EXACT order they are listed: <br/>" .
            "State Code, CallUp Number, Full Name, Gender, Institution, Program of Study, " .
            "Qualification (e.g B.ENG), Date of Birth and Platoon. <br/><br/>See the images below for guidance";
        
        //Check that the state code is the first column on this spreadsheet
        if(!isset($firstRowOfSpreadsheet["A"]) || preg_match("/(^[A-Z]{2}\/[0-9]{2}[A-Z]{1}\/[0-9]{4})/i", $firstRowOfSpreadsheet["A"]) != 1){
            $errorMessage .= "<br/><br/>3.)The State Code should be on column A";
            return $errorMessage;
        }

        if(!isset($firstRowOfSpreadsheet["B"]) || preg_match("/(^[A-Z]{4}\/[A-Z]{3}\/[0-9]{4}\/[0-9]{6,10})/i", $firstRowOfSpreadsheet["B"]) != 1){
            $errorMessage .= "<br/><br/>3.)The Callup number should be on column B";
            return $errorMessage;
        }

        if(!isset($firstRowOfSpreadsheet["D"]) || preg_match("/(^[M,F]{1})/i", $firstRowOfSpreadsheet["D"]) != 1){
            $errorMessage .= "<br/><br/>3.)The Gender should be on column D";
            return $errorMessage;
        }

        if(!isset($firstRowOfSpreadsheet["H"]) || preg_match("/(^[0-9]{2}\/[0-9]{2}\/[0-9]{4})/i", $firstRowOfSpreadsheet["H"]) != 1){
            $errorMessage .= "<br/><br/>3.)The Date of Birth should be on column H with format dd/mm/yyyy or DD/MM/YYYY";
            return $errorMessage;
        }

        if(!isset($firstRowOfSpreadsheet["I"]) || preg_match("/(^[0-9]{1,2})$/i", $firstRowOfSpreadsheet["I"]) != 1){
            $errorMessage .= "<br/><br/>3.)The Platoon Number should be on column I (a number between 1 and 10)";
            return $errorMessage;
        }

        if(!isset($firstRowOfSpreadsheet["C"])){
            $errorMessage .= "<br/><br/>3.)The Full Name should be on column C";
            return $errorMessage;
        }

        return null;
    }
}
