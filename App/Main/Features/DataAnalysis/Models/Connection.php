<?php

namespace App\Main\Features\DataAnalysis\Models;


class Connection
{
    /**
     * @var string The name of the connection source (e.g questions)
     */
    private $source;

    /**
     * @var string The name of the connection destination (e.g sections)
     */
    private $destination;

    /**
     * @var string The source identifier that will be used to connect to the destination (e.g section_id)
     */
    private $sourceId;

    /**
     * @var string The identifier for the destination that the source will connect to (e.g id)
     */
    private $destinationId;

    /**
     * Connection constructor.
     * @param string $source
     * @param string $sourceId
     * @param string $destination
     * @param string $destinationId
     */
    public function __construct(string $source, string $sourceId, string $destination, string $destinationId)
    {
        $this->source = $source;
        $this->destination = $destination;
        $this->sourceId = $sourceId;
        $this->destinationId = $destinationId;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @return string
     */
    public function getDestination(): string
    {
        return $this->destination;
    }

    public function getQuery()
    {
        $query = "JOIN " . $this->destination . " ON " . $this->source . "." . $this->sourceId . "=" . $this->destination . "." . $this->destinationId;
        return $query;
    }

    public function __toString()
    {
        return $this->getQuery();
    }
}