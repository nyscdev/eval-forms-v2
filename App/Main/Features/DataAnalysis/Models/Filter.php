<?php

namespace App\Main\features\DataAnalysis\Models;


class Filter implements Aggregatable
{
    /**
     * @var string The source of the dimension (e.g questions)
     */
    private $source;

    /**
     * @var string The name of the dimension to filter by (e.g id)
     */
    private $dimension;

    /**
     * @var mixed the value of the filter (e.g 5, 'Lagos')
     */
    private $value;

    /**
     * @var array A list of connections needed to get to the source
     */
    private $connections;

    /**
     * @var bool
     */
    private $includeInSelection;

    /**
     * @var string Optional. If the filter is selected as part of the columns, this name will be used
     */
    private $displayName;

    /**
     * @var string The operator to use (=, <, >, !=), defaults to '='
     */
    private $operator = "=";

    /**
     * Filter constructor.
     * @param string $source
     * @param string $dimension
     * @param mixed $value
     * @param array $connections
     * @param bool $includeInSelection
     * @param string $displayName
     * @param string $operator
     */
    public function __construct(string $source, string $dimension, $value, array $connections = [], $includeInSelection = false, $displayName = '', string $operator = '=')
    {
        $this->dimension = $dimension;
        $this->source = $source;
        $this->connections = $connections;
        $this->operator = $operator;
        $this->value = $value;
        $this->includeInSelection = $includeInSelection;
        $this->displayName = $displayName == '' ? $this->getDimension() : $displayName;
    }

    public function getName()
    {
        return $this->source . '.' . $this->dimension;
    }

    public function __toString()
    {
        return $this->getName() .' '. $this->operator .' '. $this->value;
    }


    /**
     * @return string
     */
    public function getDimension(): string
    {
        return $this->dimension;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @return array
     */
    public function getConnections(): array
    {
        return $this->connections;
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->__toString();
    }

    /**
     * @return bool
     */
    public function shouldBeIncludedInSelection(): bool
    {
        return $this->includeInSelection;
    }

    /**
     * @return string
     */
    public function getDisplayName(): string
    {
        return $this->displayName;
    }


    /**
     * @return string
     */
    public function getOperator(): string
    {
        return $this->operator;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }


}