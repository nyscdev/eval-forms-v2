<?php

namespace App\Main\Features\DataAnalysis\Models;


interface Aggregatable
{
    /**
     * @return string The source for this aggregatable item
     */
    public function getSource();

    /**
     * @return string The dimension for the item
     */
    public function getDimension();

    /**
     * @return array The connections needed to get to the source
     */
    public function getConnections();
}