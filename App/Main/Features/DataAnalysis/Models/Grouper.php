<?php

namespace App\Main\Features\DataAnalysis\Models;


class Grouper implements Aggregatable
{
    /**
     * @var string The source of the dimension (e.g questions)
     */
    private $source;

    /**
     * @var string The name of the dimension to group by (e.g id)
     */
    private $dimension;

    /**
     * @var array A list of connections needed to get to the source
     */
    private $connections;

    /**
     * Grouper constructor.
     * @param string $source
     * @param string $dimension
     * @param array $connections
     */
    public function __construct(string $source, string $dimension, $connections = [])
    {
        $this->dimension = $dimension;
        $this->source = $source;
        $this->connections = $connections;
    }

    /**
     * @return string
     */
    public function getDimension(): string
    {
        return $this->dimension;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @return array
     */
    public function getConnections(): array
    {
        return $this->connections;
    }
}