<?php

namespace App\Main\Features\DataAnalysis;

use App\Main\Http\Session\SessionManager;
use App\Main\Repositories\LiveStatsRepository;
use App\Main\Models\LiveStats;
use App\Main\Models\Timestamp;
use App\Main\Repositories\TimestampRepository; 

class LiveStatsService
{
    /**
     * @var SessionManager
     */
    private $sessionManager;

    /**
     * @var LiveStatsRepository
     */
    private $liveStatsRepository;

    private $timestampRepository; 
    
    public function __construct(
        SessionManager $sessionManager, 
        LiveStatsRepository $liveStatsRepository,
        TimestampRepository $timestampRepository
    )
    {
        $this->sessionManager = $sessionManager;
        $this->liveStatsRepository = $liveStatsRepository;
        $this->timestampRepository = $timestampRepository; 
    }

    /**
     * Increment the number of responses for a particular form. This method should be called anytime a user submits a form
     *
     * @param $formId
     * @param bool $userCompletedForm (did the user complete the form?)
     */
    public function logStats($formId, $userCompletedForm)
    {
        $state = $this->sessionManager->getCurrentUserState();
        $liveStats = new LiveStats();
        $liveStats->setFormId($formId);
        $liveStats->setState($state);
        
        $this->liveStatsRepository->save($liveStats);
    }

    public function getLiveStats($formId, $state)
    {
        $liveStats = $this->liveStatsRepository->findByStateAndFormId($state, $formId);
        return $liveStats;
    }

    /**
     * Creates a new timestamp for a particular form in the current user's state.
     * Ideally, only the admin of a state should be able to have access to this functionality.
     * The timestamp will make it easier for analysts to filter responses from corpers by choosing
     * to show only responses that were submitted before the time indicated by the timestamp
     */
    public function createTimestampForForm($formId, $datetime)
    {
        $state = $this->sessionManager->getCurrentUserState();
        $timestamp = $this->timestampRepository->findByStateAndFormId($state, $formId);
        
        if(empty($timestamp))
        {
            $timestamp = new Timestamp(); 
            $timestamp->setFormId($formId);
            $timestamp->setState($state); 
        } else {
            $timestamp = $timestamp[0]; 
        }

        $timestamp->setTimestamp($datetime); 

        $this->timestampRepository->save($timestamp); 
    }

    public function getTimestamp($formId, $state)
    {
        $timestamp = $this->timestampRepository->findByStateAndFormId($state, $formId); 
        return $timestamp; 
    }
}
