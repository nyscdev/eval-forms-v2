<?php

namespace App\Main\Features\DataAnalysis;

use App\Main\Features\DataAnalysis\Aggregators\CountAggregator;
use App\Main\Features\DataAnalysis\Aggregators\GlobalAggregator;
use App\Main\Features\DataAnalysis\Constants\CommonConnections;
use App\Main\Features\DataAnalysis\Constants\CommonDistinctAggregators;
use App\Main\Features\DataAnalysis\Models\Connection;
use App\Main\Features\DataAnalysis\Models\Filter;
use App\Main\Features\DataAnalysis\RequestBodies\FiltersRequestBody;
use App\Main\Repositories\AnalysisRepository;
use App\Main\Repositories\QuestionRepository;
use App\Main\Models\Enums\AnswerFormat;
use TextAnalysis\Tokenizers\GeneralTokenizer;
use TextAnalysis\Analysis\FreqDist; 

class AnalysisService
{
    /**
     * @var AnalysisRepository
     */
    private $analysisRepository;
    private $formSectionService;
    private $questionService;
    private $questionRepository; 

    /**
     * AnalysisService constructor.
     * @param AnalysisRepository $analysisRepository
     */
    public function __construct(
        AnalysisRepository $analysisRepository,
        QuestionRepository $questionRepository
    ) {
        $this->analysisRepository = $analysisRepository;
        $this->questionRepository = $questionRepository; 
    }

    public function getAllCorperQualifications()
    {
        return $this->analysisRepository->getAggregatedData(CommonDistinctAggregators::allQualifications());
    }

    public function getAllCorperInstitutions()
    {
        return $this->analysisRepository->getAggregatedData(CommonDistinctAggregators::allInstitutions());
    }

    public function getQuestionsForForm($formId)
    {
        $connections = [
            new Connection('questions', 'section_id', 'sections', 'id')
        ];

        $filters = [
            new Filter('sections', 'form_id', $formId, $connections)
        ];

        $aggregator = new GlobalAggregator('questions', $filters);
        return $this->analysisRepository->getAggregatedData($aggregator);
    }

    public function getResponseAnalysisForQuestions(FiltersRequestBody $filtersRequestBody, $questionId)
    {
        $query = "SELECT question_responses.response as response, COUNT(*) as responseCount 
                  FROM `question_responses` 
                  Join corpers ON question_responses.user_id = corpers.user_id 
                  Join users ON question_responses.user_id = users.id " .
                  $this->getUserCompletedFormFilterQuery($filtersRequestBody->isCompleted()) .
                  "WHERE question_id = $questionId " .
                  $this->getCorperInstitutionFilterQuery($filtersRequestBody->getInstitution()) .
                  $this->getCorperQualificationFilterQuery($filtersRequestBody->getQualification()) .
                  $this->getCorperStateFilterQuery($filtersRequestBody->getState()) .
                  $this->getTimestampFilterQuery($filtersRequestBody->getTimestamp()) .
                  "GROUP BY question_responses.response";

        $question = $this->questionRepository->findById($questionId);
        $responses = $this->analysisRepository->getSqlAggregatedData($query);

        if ($question->getAnswerFormat() == AnswerFormat::EMPTY_TEXT_BOX) {
            $responses = $this->getAnalysisForOpenEndedQuestions($questionId); 
        }
        return $responses; 
    }

    public function getAnalysisForOpenEndedQuestions($questionId){
        $openEndedQuestionQuery = "SELECT question_responses.response as response
                                   FROM `question_responses` 
                                   WHERE question_id = $questionId"; 

        $openEndedQuestionResponses = $this->analysisRepository->getSqlAggregatedData($openEndedQuestionQuery); 
        $responses = $openEndedQuestionResponses; 

        $openEndedResponseText = ""; 
        foreach($responses as $response){
            $response["response"] = trim($response["response"]); 
            $response["response"] = strtolower($response["response"]); 
            $responseText = str_replace(' ', 'spaceShouldBeHere', $response["response"]);
            $openEndedResponseText = $openEndedResponseText . $responseText . " "; 
        }

        $responses = []; 
          
        $tokenizer = new GeneralTokenizer(); 
        $tokens = $tokenizer->tokenize($openEndedResponseText);
        if(!empty($tokens)){
            $freqDistributor = new FreqDist($tokens); 
            $top10 = array_splice($freqDistributor->getKeyValuesByFrequency(), 0, 10);
            $responses = []; 

            foreach($top10 as $key => $value){
                $keyText = str_replace('spaceShouldBeHere', ' ', $key);
                $keyText = ucwords($keyText); 
                $responses[] = ["response" => $keyText, "responseCount" => $value]; 
            }
        }
        return $responses; 
    }

    public function getUserCompletedFormFilterQuery($isCompleted)
    {
        if ($isCompleted == "true") {
            return "join form_response on question_responses.user_id = form_response.user_id ";
        }
        
        return null;
    }

    public function getCorperStateFilterQuery($state)
    {
        if ($state != null and !empty($state)) {
            return "and users.state = '$state' ";
        }
        return null;
    }

    public function getCorperInstitutionFilterQuery($corperInstitution)
    {
        if ($corperInstitution != null and !empty($corperInstitution)) {
            return "and corpers.institution_name = '$corperInstitution' ";
        }

        return null;
    }

    public function getCorperQualificationFilterQuery($corperQualification)
    {
        if ($corperQualification != null and !empty($corperQualification)) {
            return "and corpers.qualification = '$corperQualification' ";
        }
        return null;
    }

    public function getTimestampFilterQuery($timestamp)
    {
        if ($timestamp != null and !empty($timestamp)) {
            return "and question_responses.timestamp < '$timestamp' ";
        }
        return null;
    }
}
