<?php

namespace App\Main\Features\DataAnalysis\Constants;


use App\Main\Features\DataAnalysis\Aggregators\DistinctAggregator;

class CommonDistinctAggregators
{
    public static function allInstitutions()
    {
        return new DistinctAggregator('corpers', 'institution_name');
    }

    public static function allQualifications()
    {
        return new DistinctAggregator('corpers', 'qualification');
    }
}
