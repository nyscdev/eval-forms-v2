<?php

namespace App\Main\Features\DataAnalysis\Constants;


use App\Main\Features\DataAnalysis\Models\Filter;

class CommonFilters
{
    public static function questionResponsesForQuestion($questionId, $includeInSelection = false, $displayName = '')
    {
        return [
            new Filter("question_responses", "question_id", $questionId, [], $includeInSelection, $displayName)
        ];
    }

    public static function questionResponsesForSection($sectionId, $includeInSelection = false, $displayName = '')
    {
        return [
            new Filter("questions", "section_id", $sectionId, CommonConnections::questionResponseToQuestion(), $includeInSelection, $displayName)
        ];
    }

    public static function questionResponsesForForm($formId, $includeInSelection = false, $displayName = '')
    {
        return [
            new Filter("sections", "form_id", $formId, CommonConnections::questionResponseToSection(), $includeInSelection, $displayName)
        ];
    }
}
