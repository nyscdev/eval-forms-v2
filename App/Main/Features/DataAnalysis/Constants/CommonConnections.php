<?php

namespace App\Main\Features\DataAnalysis\Constants;


use App\Main\Features\DataAnalysis\Models\Connection;

class CommonConnections
{
    static function questionResponseToCorper() {
        return [
            new Connection("question_responses", "user_id", "corpers", "user_id")
        ];
    }

    static function questionResponseToUser() {
        return [
            new Connection("question_responses", "user_id", "users", "id")
        ];
    }

    static function questionResponseToQuestion() {
        return [
            new Connection("question_responses", "question_id", "questions", "id")
        ];
    }

    static function questionResponseToSection() {
        return [
            new Connection("question_responses", "question_id", "questions", "id"),
            new Connection("questions", "section_id", "sections", "id")
        ];
    }

    static function questionResponseToForm() {
        return [
            new Connection("question_responses", "question_id", "questions", "id"),
            new Connection("questions", "section_id", "sections", "id"),
            new Connection("sections", "form_id", "forms", "id"),
        ];
    }
}