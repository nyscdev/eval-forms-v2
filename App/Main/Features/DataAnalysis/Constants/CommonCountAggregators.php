<?php

namespace App\Main\Features\DataAnalysis\Constants;


use App\Main\Features\DataAnalysis\Aggregators\CountAggregator;

class CommonCountAggregators
{
    public static function numberOfQuestionResponses()
    {
        return new CountAggregator("question_responses", null, [], "no_of_responses");
    }

    public static function numberOfQuestionResponsesForQuestion($questionId)
    {
        $filters = CommonFilters::questionResponsesForQuestion($questionId);
        return new CountAggregator("question_responses", null, $filters, "no_of_responses");
    }

    public static function responsesForFormByUserState($formId)
    {
        $grouper = CommonGroupers::questionResponseByUserState();
        $filters = CommonFilters::questionResponsesForForm($formId, false);
        return new CountAggregator("question_responses", $grouper, $filters, "number_of_responses");
    }
}