<?php

namespace App\Main\Features\DataAnalysis\Constants;


use App\Main\Features\DataAnalysis\Models\Grouper;

class CommonGroupers
{
    public static function questionResponseByQuestionId()
    {
        return new Grouper('questions', 'id', CommonConnections::questionResponseToQuestion());
    }

    public static function questionResponseByUserState()
    {
        return new Grouper('users', 'state', CommonConnections::questionResponseToUser());
    }

    public static function questionResponseBySectionId()
    {
        return new Grouper('sections', 'id', CommonConnections::questionResponseToSection());
    }

    public static function questionResponseByFormId()
    {
        return new Grouper('forms', 'id', CommonConnections::questionResponseToForm());
    }
}