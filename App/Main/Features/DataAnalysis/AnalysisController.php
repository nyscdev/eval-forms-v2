<?php

namespace App\Main\Features\DataAnalysis;

use App\Main\Controllers\Base\Controller;
use App\Main\Features\DataAnalysis\Constants\CommonDistinctAggregators;
use App\Main\Features\DataAnalysis\Constants\CommonFilters;
use App\Main\Features\DataAnalysis\Models\Filter;
use App\Main\Features\DataAnalysis\RequestBodies\FiltersRequestBody;
use App\Main\Features\DataAnalysis\ViewModels\FormAnalysisViewModel;
use App\Main\Features\DataAnalysis\ViewModels\ResponseAnalysisViewModel;
use App\Main\Features\FormSectionManagement\FormSectionService;
use App\Main\Features\FormManagement\FormService;
use App\Main\Features\QuestionManagement\QuestionService;
use App\Main\Libraries\ViewModel;
use App\Main\Models\Enums\State;
use App\Main\Repositories\AnalysisRepository;

class AnalysisController extends Controller
{
    /**
     * @var AnalysisService
     */
    private $analysisService;

    /**
     * @var FormService
     */
    private $formService;

    /**
     * @var FormSectionService
     */
    private $formSectionService;

    /**
     * @var QuestionService
     */
    private $questionService;

    /**
     * AnalysisController constructor.
     * @param AnalysisService $analysisService
     * @param FormService $formService
     * @param FormSectionService $formSectionService
     * @param QuestionService $questionService
     */
    public function __construct(
        AnalysisService $analysisService,
        FormService $formService,
        FormSectionService $formSectionService,
        QuestionService $questionService
    ) {
        parent::__construct();
        $this->analysisService = $analysisService;
        $this->formService = $formService;
        $this->formSectionService = $formSectionService;
        $this->questionService = $questionService;
    }

    /**
     * @return string
     * @throws \App\Main\Exceptions\NavigationException
     * @throws \App\Main\Exceptions\RenderException
     * @throws \ReflectionException
     */
    public function getAnalyzeCorperDataView()
    {
        $forms = $this->formService->getForms();
        $states = State::getValues();
        $qualifications = $this->analysisService->getAllCorperQualifications();
        $institutions = $this->analysisService->getAllCorperInstitutions();

        $viewModel = new FormAnalysisViewModel();
        $viewModel->formsList = $forms;
        $viewModel->statesList = $states;
        $viewModel->institutionsList = $institutions;
        $viewModel->qualificationsList = $qualifications;
        $viewModel->title = "Analyze Data";

        return $this->render("analysis/analyzedata", $viewModel);
    }

    public function getAnalyzeCorperDataViewForAnalysts()
    {
        $forms = $this->formService->getForms();
        $states = State::getValues();
        $qualifications = $this->analysisService->getAllCorperQualifications();
        $institutions = $this->analysisService->getAllCorperInstitutions();

        $viewModel = new FormAnalysisViewModel();
        $viewModel->formsList = $forms;
        $viewModel->state = $this->sessionManager->getCurrentUserState();
        $viewModel->institutionsList = $institutions;
        $viewModel->qualificationsList = $qualifications;
        $viewModel->title = "Analyze Data";

        return $this->render("analysis/analyzedata-analyst", $viewModel);
    }

    public function getQuestionsForForm($formId)
    {
        $questions = $this->analysisService->getQuestionsForForm($formId);
    }

    public function getCorperSectionsForForm($formId)
    {
        $sections = $this->formSectionService->getCorperSectionsForForm($formId);
        $viewModel = new FormAnalysisViewModel();
        $viewModel->sectionsList = $sections;
        return $this->render("analysis/form-sections", $viewModel);
    }

    public function getQuestionsForSection($sectionId)
    {
        $questions = $this->questionService->getQuestionsForSection($sectionId);
        $viewModel = new FormAnalysisViewModel();
        $viewModel->questionsList = $questions;
        return $this->render("analysis/form-questions", $viewModel);
    }

    /**
     * @param $filtersArray FiltersRequestBody will contain a list of filters that were selected
     * by the user like institution, is form completed - consistent data, state etc.
     * @param $questionId
     */
    public function getResponseAnalysisForQuestion(FiltersRequestBody $filtersArray, $questionId)
    {
        //WARNING: This request isn't grouped by anything, therefore the response is only going to contain 1 item
        //which would be the the total number of responses that match the filters

        $responses = $this->analysisService->getResponseAnalysisForQuestions($filtersArray, $questionId);

        $viewModel = new ResponseAnalysisViewModel();

        $sum = 0;
       
        foreach ($responses as $response) {
            $sum += $response["responseCount"];
        }

        foreach ($responses as $response) {
            $value = [
                "Response" => $response["response"],
                "Count" => $response['responseCount'],
                "Percentage" => ($response['responseCount'] * 100) / ($sum == 0 ? 1 : $sum)
            ];

            $viewModel->aggregatedResponses[] = $value;
        }

        $viewModel->totalResponseCount = $sum;
        return \json_encode($viewModel);
    }
}
