<?php

namespace App\Main\Features\DataAnalysis\ViewModels;

use App\Main\Libraries\ViewModel;

class FormAnalysisViewModel extends ViewModel
{
    /**
     * List of forms that corpers are meant to fill (form Title and Form Id)
     * @var array
     */
    public $formsList = [];

    /**
     * Aggregated list of corpers institutions from the corpers table
     */
    public $institutionsList = [];

    /**
     * List of states
     */
    public $statesList = [];

    /**
     * Aggregated List of qualifications
     */
    public $qualificationsList = [];

    /**
     * List of sections in this form, ideally should be sections that are assigned to the Corper role
     */
    public $sectionsList = [];

    /**
     * List of questions for a particular section
     */
    public $questionsList = [];

    /**
     * Current user's state (useful for the analysis views made for just analysts and state admins)
     */
    public $state = "";
}
