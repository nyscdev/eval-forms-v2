<?php

namespace App\Main\Features\DataAnalysis\ViewModels;

use App\Main\Libraries\ViewModel;
use App\Main\Models\Form;

class ResponseAnalysisViewModel extends ViewModel
{
   
    /**
     * Each entry in this array will have this format
     * {
     *     Response => xxxx
     *     Count => 0000
     *     Percentage => 1%
     * }
     */
    public $aggregatedResponses = [];

    /**
     * The total counts of each response in $aggregatedResponse.
     */
    public $totalResponseCount;
}
