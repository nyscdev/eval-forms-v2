<?php

namespace App\Main\Features\DataAnalysis\ViewModels;

use App\Main\Libraries\ViewModel;

class LiveStatsViewModel extends ViewModel
{
    /**
     * List of forms that corpers are meant to fill (form Title and Form Id)
     * @var array
     */
    public $formsList = [];

    /**
     * List of states
     */
    public $statesList = [];
}
