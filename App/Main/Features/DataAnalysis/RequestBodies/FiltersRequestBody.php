<?php

namespace App\Main\Features\DataAnalysis\RequestBodies;

class FiltersRequestBody
{
    /**
     * @var string
     */
    private $institution;

    /**
     * @var string
     */
    private $completed;

    /**
     * @var string
     */
    private $state;

    /**
     * @var string
     */
    private $qualification;

    /**
     * @var DateTime
     */
    private $timestamp;
    
    /**
     * @return string
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * @param string $institution
     */
    public function setInstitution(string $institution): void
    {
        $this->institution = $institution;
    }

    /**
     * @return bool
     */
    public function isCompleted()
    {
        return $this->completed;
    }

    /**
     * @param bool $completed
     */
    public function setCompleted(string $completed): void
    {
        $this->completed = $completed;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState(string $state): void
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getQualification()
    {
        return $this->qualification;
    }

    /**
     * @param string $qualification
     */
    public function setQualification(string $qualification): void
    {
        $this->qualification = $qualification;
    }

    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    public function getTimestamp()
    {
        return $this->timestamp;
    }
}
