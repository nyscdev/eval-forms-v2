<?php

namespace App\Main\Features\DataAnalysis\Aggregators;

use App\Main\Features\DataAnalysis\Models\Connection;
use App\Main\Features\DataAnalysis\Models\Filter;
use App\Main\Features\DataAnalysis\Models\Grouper;
use App\Main\Libraries\Log;

abstract class Aggregator
{
    /**
     * @var string The source of the data being aggregated (e.g question_responses)
     */
    protected $source;

    /**
     * @var Grouper Specifies the dimension that the aggregator would group by
     */
    protected $grouper;

    /**
     * @var array A collection of filters for this aggregation
     */
    protected $filters = [];

    /**
     * @var array These are filled when qetQuery is run
     */
    private $queryParameters = [];

    /**
     * Aggregator constructor.
     * @param string $source
     * @param Grouper $grouper
     * @param array $filters
     */
    public function __construct(string $source, Grouper $grouper = null, array $filters = [])
    {
        $this->source = $source;
        $this->grouper = $grouper;
        $this->filters = $filters;
    }

    /**
     * @return array The names of the aggregated columns for this aggregator
     */
    protected abstract function getAggregatedColumns();

    /**
     * @return array
     */
    public function getQueryParameters(): array
    {
        return $this->queryParameters;
    }

    /**
     * @param bool $useParameters If true, placeholders will be used in place of filter values
     * in the query and getQueryParameters() will need to be called. Not needed if there are no filters
     * @return string The query represented by this aggregator
     */
    public function getQuery(bool $useParameters = false)
    {
        $this->queryParameters = [];
        $querySelection = 'SELECT';
        $query = ' FROM ' . $this->source;

        $queryFilters = !empty($this->filters) ? ' WHERE ' : '';
        $connectedSources = [];

        if ($this->grouper != null) {
            foreach ($this->grouper->getConnections() as $connection) {
                assert($connection instanceof Connection);
                if (isset($connectedSources[$connection->getDestination()])) {
                    //avoid adding same connection multiple times
                    Log::info("Skipping connection for " . $connection->getSource());
                    continue;
                }

                $query .= ' ' . $connection->getQuery();
                $connectedSources[$connection->getDestination()] = true;
            }
        }

        $isFirst = true;
        foreach ($this->filters as $filter) {
            assert($filter instanceof Filter);

            if ($useParameters) {
                $filterParameterName = $filter->getSource() . '_' . $filter->getDimension();
                $filterString = $filter->getName() . ' ' . $filter->getOperator() . ' :' . $filterParameterName;
                $queryFilters .= $isFirst ? $filterString : ' AND ' . $filterString;
                $this->queryParameters[':' . $filterParameterName] = $filter->getValue();
            } else {
                $queryFilters .= $isFirst ? $filter->getQuery() : ' AND ' . $filter->getQuery();
            }

            $querySelection .= $filter->shouldBeIncludedInSelection() ? ' ANY_VALUE(' . $filter->getName() . ') AS ' . $filter->getDisplayName() . ',' : '';

            $isFirst = false;
            foreach ($filter->getConnections() as $connection) {
                assert($connection instanceof Connection);
                if (isset($connectedSources[$connection->getDestination()])) {
                    //avoid adding same connection multiple times
                    Log::info("Skipping connection for " . $connection->getSource());
                    continue;
                }

                $query .= ' ' . $connection->getQuery();
                $connectedSources[$connection->getDestination()] = true;
            }
        }

        $query .= $queryFilters;

        if (!is_null($this->grouper)) {
            $query .= ' GROUP BY ' . $this->grouper->getSource() . '.' . $this->grouper->getDimension();
        }

        $querySelection .= ' ' . implode(', ', $this->getAggregatedColumns());
        $querySelection .= $query;
        return $querySelection;
    }
}
