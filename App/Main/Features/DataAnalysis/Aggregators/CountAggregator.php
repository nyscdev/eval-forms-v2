<?php

namespace App\Main\Features\DataAnalysis\Aggregators;

use App\Main\Features\DataAnalysis\Models\Grouper;

class CountAggregator extends Aggregator
{
    private $aggregatedName = "count";

    /**
     * CountAggregator constructor.
     * @param string $source
     * @param Grouper $grouper
     * @param string $aggregatedName
     * @param array $filters
     */
    public function __construct(string $source, Grouper $grouper = null, array $filters = [], string $aggregatedName = 'count')
    {
        parent::__construct($source, $grouper, $filters);
        $this->aggregatedName = $aggregatedName;
    }

    /**
     * @return array The names of the aggregated columns for this aggregator
     */
    protected function getAggregatedColumns()
    {
        $countColumn = 'COUNT(*) AS ' . $this->aggregatedName;

        if ($this->grouper != null) {
            $grouperColumn = $this->grouper->getSource() . '.' . $this->grouper->getDimension();
            return [$grouperColumn, $countColumn];
        }

        return [$countColumn];
    }
}
