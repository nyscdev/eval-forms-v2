<?php

namespace App\Main\Features\DataAnalysis\Aggregators;


class GlobalAggregator extends Aggregator
{
    /**
     * GlobalAggregator constructor.
     * @param string $source
     * @param array $filters
     */
    public function __construct(string $source, array $filters = [])
    {
        parent::__construct($source, null, $filters);
    }

    /**
     * @return array The names of the aggregated columns for this aggregator
     */
    protected function getAggregatedColumns()
    {
        return ['*'];
    }
}