<?php

namespace App\Main\Features\DataAnalysis\Aggregators;


class DistinctAggregator extends Aggregator
{
    private $distinctDimension;

    /**
     * DistinctAggregator constructor.
     * @param string $source
     * @param $distinctDimension
     * @param array $filters
     */
    public function __construct(string $source, $distinctDimension, array $filters = [])
    {
        parent::__construct($source, null, $filters);
        $this->distinctDimension = $distinctDimension;
    }

    /**
     * @return array The names of the aggregated columns for this aggregator
     */
    protected function getAggregatedColumns()
    {
        return ['DISTINCT('.$this->distinctDimension.')'];
    }
}
