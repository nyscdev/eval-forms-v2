<?php

namespace App\Main\Features\DataAnalysis;

use App\Main\Features\DataAnalysis\ViewModels\LiveStatsViewModel;
use App\Main\Features\DataAnalysis\ViewModels\LiveStatsForUserStateViewModel;
use App\Main\Models\Enums\State;
use App\Main\Features\FormManagement\FormService;
use App\Main\Features\DataAnalysis\LiveStatsService;
use App\Main\Controllers\Base\Controller;
use App\Main\Libraries\ViewModel;

class LiveStatsController extends Controller
{
    /**
     * @var LiveStatsService
     */
    private $liveStatsService;

    /**
     * @var FormService
     */
    private $formService;

    /**
     * LiveStatsController constructor.
     * @param LiveStatsService $liveStatsService
     * @param FormService $formService
     */
    public function __construct(
        LiveStatsService $liveStatsService,
        FormService $formService
    ) {
        parent::__construct();
        $this->liveStatsService = $liveStatsService;
        $this->formService = $formService;
    }

    public function getLiveStatsView()
    {
        $viewModel = new LiveStatsViewModel("Live Stats");
        $viewModel->statesList = State::getValues();
        $viewModel->formsList = $this->formService->getFormsWithSectionsForCorpers();
        return $this->render("analysis/livestats", $viewModel);
    }

    public function getLiveStatsForAnalystView()
    {
        $viewModel = new LiveStatsForUserStateViewModel("Live Stats");
        $viewModel->state = $this->sessionManager->getCurrentUserState();
        $viewModel->formsList = $this->formService->getFormsWithSectionsForCorpers();
        return $this->render("analysis/livestats-analyst", $viewModel);
    }

    public function getLiveStatsForStateAdminView()
    {
        $viewModel = new LiveStatsForUserStateViewModel("Live Stats");
        $viewModel->state = $this->sessionManager->getCurrentUserState();
        $viewModel->formsList = $this->formService->getFormsWithSectionsForCorpers();
        return $this->render("analysis/livestats-admin", $viewModel);
    }
    
    public function getLiveStats($state, $formId)
    {
        $liveStatsArray = $this->liveStatsService->getLiveStats($formId, $state);
        $response = null;
        
        if (!empty($liveStatsArray)) {
            $response = [
                "totalResponses" => sizeof($liveStatsArray),
                "totalCompletedResponses" => sizeof($liveStatsArray)
            ];
        }
        return \json_encode($response);
    }

    /**
     * A problem that analysts usually have when analyzing the responses from the corpers is that
     * the responses continue to update while they are doing the analysis.
     * This means that if they start analyzing a form with data from 300 corpers,
     * about 30 minutes after they've started analysis
     * more corpers would have filled the form so the analysis dashboard will be showing responses from say 340 corpers,
     * instead of the 300 they started analyzing the data with.
     * The idea of the timestamp is so that the admin of a particular state can lock the responses for a particular form
     * to a given point in time
     * that way the analysts can choose to only work with responses from corpers that filled the form
     * before that particular time.
     * This way analysts won't be frustrated by starting work with 300 responses and then 30 minutes later,
     * they have 350 responses, they will be able to restrict the responses they want to work with to a particular time
     */
    public function createTimestampForCurrentUser($formId)
    {
        $timestamp = date('Y-m-d H:i:s');
        $this->liveStatsService->createTimestampForForm($formId, $timestamp);
        $response = [
            "timestamp" => $timestamp
        ];
        return \json_encode($response);
    }

    public function getTimestampForCurrentUser($formId)
    {
        $state = $this->sessionManager->getCurrentUserState();
        $timestampArray = $this->liveStatsService->getTimestamp($formId, $state); 

       
        
        if (!empty($timestampArray)) {
            $timestamp = $timestampArray[0];
            $response = [
                "timestamp" => $timestamp->getTimestamp()
            ];
        }
        return \json_encode($response);
    }
}
