<?php

namespace App\Main\Features\FormResponseManagement;

use App\Main\Controllers\Base\Controller;
use App\Main\Exceptions\RenderException;
use App\Main\Features\FormManagement\FormService;
use App\Main\Features\FormSectionManagement\FormSectionService;
use App\Main\Features\FormResponseManagement\ViewModels\FormViewModel;
use App\Main\Features\FormResponseManagement\ViewModels\UserFormsViewModel;

class FormResponseController extends Controller
{
    /**
     * @var FormService
     */
    private $formService;

    /**
     * @var FormSectionService
     */
    private $sectionService;

    /**
     * @var FormResponseService
     */
    private $formResponseService;

    /**
     * FormResponseController constructor.
     * @param FormService $formService
     * @param FormSectionService $sectionService
     * @param FormResponseService $formResponseService
     */
    public function __construct(FormService $formService, FormSectionService $sectionService, FormResponseService $formResponseService)
    {
        parent::__construct();
        $this->formService = $formService;
        $this->sectionService = $sectionService;
        $this->formResponseService = $formResponseService;
    }

    /**
     * Returns a list of forms that the current user is supposed to be able to fill (i.e forms with sections corresponding
     * to the current user's form role)
     */
    public function getFormsViewForCurrentUser($message = "")
    {
        $roleId = $this->sessionManager->getCurrentUserRoleId();
        $forms = $this->formService->getActiveFormsForUserRole($roleId);
        $userFormsViewModel = new UserFormsViewModel("Forms you can fill at this time");
        $userFormsViewModel->forms = $forms;
        $userFormsViewModel->processingMessage = $message;

        return $this->render('/form-response/forms', $userFormsViewModel);
    }
    
    /**
     * @param $formTitle
     * @return string
     * @throws RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function getFormViewForUserRole($formTitle, $message = "")
    {
        $form = $this->formService->getFormWithTitle($formTitle);
        $this->sessionManager->storeCustomSessionData("FormFilled", $formTitle);
        
        $sections = $this->sectionService->getSectionsForForm($form->getId());
        $sectionsViewModels = [];

        foreach ($sections as $section) {
            $sectionViewModel = $this->formResponseService->getViewModelForSection($section);
            $sectionsViewModels[] = $sectionViewModel;
        }

        $formViewModel = new FormViewModel($form->getTitle(), $sectionsViewModels, $this->sessionManager->getCurrentUserRoleName());
        $formViewModel->processingMessage = $message;
        return $this->render('form-response/display-corper-form', $formViewModel);
    }

    public function submitFormResponse($data)
    {
        $this->formResponseService->saveResponses($data, $this->sessionManager->getCurrentUserId());
        $formTitle = $this->sessionManager->getCustomSessionData("FormFilled");
        $processingMessage = "You have successfully filled " . $formTitle;
        $this->redirectToSelf('getFormsViewForCurrentUser', ["message" => $processingMessage]);
    }
}
