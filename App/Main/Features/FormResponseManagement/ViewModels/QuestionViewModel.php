<?php

namespace App\Main\Features\FormResponseManagement\ViewModels;

use App\Main\Libraries\ViewModel;

class QuestionViewModel extends ViewModel
{
    public $id;

    public $question;

    public $answer_format;

    public $child_questions = [];

    public $options = [];
}
