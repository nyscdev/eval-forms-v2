<?php

namespace App\Main\Features\FormResponseManagement\ViewModels;


use App\Main\Libraries\ViewModel;

class FormViewModel extends ViewModel
{
    public $id;

    public $description;

    public $sections = [];

    public function __construct($title, $sections, string $role = "")
    {
        parent::__construct($title);

        $this->sections = $sections;

        if ($role != "") {
            $this->filterFormByRole($role);
        }
    }

    public function filterFormByRole($role)
    {
        foreach ($this->sections as $section) {
            if ($section->role == $role) {
                continue;
            }

            $key = array_search($section, $this->sections);
            unset($this->sections[$key]);
        }
    }
}