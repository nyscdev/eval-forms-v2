<?php

namespace App\Main\Features\FormResponseManagement\ViewModels;

use App\Main\Libraries\ViewModel;

class UserFormsViewModel extends ViewModel
{
    /**
     * Forms that are active for the current user
     * @var array
     */
    public $forms;
}
