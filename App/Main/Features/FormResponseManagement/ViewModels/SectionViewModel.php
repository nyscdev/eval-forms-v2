<?php

namespace App\Main\Features\FormResponseManagement\ViewModels;

use App\Main\Libraries\ViewModel;

class SectionViewModel extends ViewModel
{
    public $id;

    public $title;

    public $description;

    public $role;

    public $roleId;

    public $questions = [];
}
