<?php

namespace App\Main\Features\FormResponseManagement;

use App\Main\Features\FormResponseManagement\ViewModels\QuestionViewModel;
use App\Main\Features\FormResponseManagement\ViewModels\SectionViewModel;
use App\Main\Models\AnswerOption;
use App\Main\Models\Question;
use App\Main\Models\Role;
use App\Main\Models\Section;
use App\Main\Models\FormResponse;
use App\Main\Repositories\AnswerOptionRepository;
use App\Main\Repositories\FormRepository;
use App\Main\Repositories\QuestionRepository;
use App\Main\Repositories\QuestionResponseRepository;
use App\Main\Repositories\SectionRepository;
use App\Main\Models\QuestionResponse;
use App\Main\Repositories\RoleRepository;
use App\Main\Repositories\FormResponseRepository;
use App\Main\Features\DataAnalysis\LiveStatsService;

class FormResponseService
{
    /**
     * @var QuestionResponseRepository
     */
    private $questionResponseRepository;

    /**
     * @var QuestionRepository
     */
    private $questionRepository;

    /**
     * @var RoleRepository
     */
    private $roleRepository;

    /**
     * @var AnswerOptionRepository
     */
    private $answerOptionRepository;

    /**
     * @var SectionRepository
     */
    private $sectionRepository;

    /**
     * @var FormResponseRepository
     */
    private $formResponseRepository;

    /**
     * @var FormRepository
     */
    private $formRepository;

    /**
     * @var LiveStatsService
     */
    private $liveStatsService;

    /**
     * FormResponseService constructor.
     * @param QuestionResponseRepository $questionResponseRepository
     * @param QuestionRepository $questionRepository
     * @param RoleRepository $roleRepository
     * @param AnswerOptionRepository $answerOptionRepository
     * @param SectionRepository $sectionRepository
     * @param FormRepository $formRepository
     * @param LiveStatsService $liveStatsService
     */
    public function __construct(
        QuestionResponseRepository $questionResponseRepository,
        QuestionRepository $questionRepository,
        RoleRepository $roleRepository,
        AnswerOptionRepository $answerOptionRepository,
        SectionRepository $sectionRepository,
        FormResponseRepository $formResponseRepository,
        FormRepository $formRepository,
        LiveStatsService $liveStatsService
    ) {
        $this->questionResponseRepository = $questionResponseRepository;
        $this->questionRepository = $questionRepository;
        $this->roleRepository = $roleRepository;
        $this->answerOptionRepository = $answerOptionRepository;
        $this->sectionRepository = $sectionRepository;
        $this->formResponseRepository = $formResponseRepository;
        $this->formRepository = $formRepository;
        $this->liveStatsService = $liveStatsService;
    }

    public function saveResponses($questionResponses, $userId)
    {
        $responses = [];
        $userCompletedForm = true;
        $someQuestionId = null;

        foreach ($questionResponses as $questionName => $questionResponse) {
            if ($questionResponse == null) {
                $userCompletedForm = false;
                continue;
            }
            
            //remove the question part of the string and leave the number
            $questionIdString = \substr($questionName, 8);
            $questionId = (int)$questionIdString;
            $someQuestionId = $questionId;
            
            $response = new QuestionResponse();
            $response->setQuestionId($questionId);
            $response->setResponse($questionResponse);
            $response->setUserId($userId);

            $responses[] = $response;
        }

        $this->updateFormResponseForUser($someQuestionId, $userId, $userCompletedForm);
        $this->questionResponseRepository->saveMultiple($responses);
    }

    public function updateFormResponseForUser($questionId, $userId, $isFormComplete)
    {
        $questionForm = $this->getFormForQuestion($questionId);
        $this->liveStatsService->logStats($questionForm->getId(), $isFormComplete);

        if ($isFormComplete == false) {
            return;
        }
        
        //Make a record in the FormResponse table if the user filled the form completely
        $formResponse = new FormResponse();
        $formResponse->setFormId($questionForm->getId());
        $formResponse->setUserId($userId);
        $this->formResponseRepository->save($formResponse);
    }

    public function getFormForQuestion($questionId)
    {
        $question = $this->questionRepository->findById($questionId);
        $section = $this->sectionRepository->findById($question->getSectionId());
        $form = $this->formRepository->findById($section->getFormId());
        return $form;
    }

    public function getViewModelForSection(Section $section)
    {
        if ($section == null) {
            return new SectionViewModel();
        }

        $sectionViewModel = new SectionViewModel($section->getTitle());
        $sectionViewModel->id = $section->getId();
        $sectionViewModel->description = $section->getDescription();

        $role = $this->roleRepository->findById($section->getRoleId());
        assert($role instanceof Role);
        $sectionViewModel->role = $role->getRole();
        $sectionViewModel->roleId = $role->getId();

        $questions = $this->questionRepository->findBySectionId($section->getId());

        foreach ($questions as $question) {
            $sectionViewModel->questions[] = $this->getViewModelForQuestion($question);
        }

        return $sectionViewModel;
    }

    /**
     * Returns a view model populated with details of the specified question
     *
     * @param Question $question
     * @return QuestionViewModel
     */
    public function getViewModelForQuestion(Question $question)
    {
        if ($question == null) {
            return new QuestionViewModel();
        }

        $childQuestions = $this->questionRepository->findByParentQuestionId($question->getId());
        $options = $this->answerOptionRepository->findByQuestionId($question->getId());

        $questionViewModel = new QuestionViewModel();
        $questionViewModel->id = $question->getId();
        $questionViewModel->question = $question->getQuestion();
        $questionViewModel->answer_format = $question->getAnswerFormat();
        $questionViewModel->child_questions = $childQuestions;

        foreach ($options as $option) {
            assert($option instanceof AnswerOption);
            $questionViewModel->options[] = $option->getAnswerOption();
        }
        
        return $questionViewModel;
    }
}
