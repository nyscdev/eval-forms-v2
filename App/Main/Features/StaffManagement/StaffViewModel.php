<?php

namespace App\Main\Features\StaffManagement;

class StaffViewModel
{
    /**
    * @var int
    */
    public $user_id;

    /**
     * @var string
     */
    public $first_name;

    /**
     * @var string
     */
    public $last_name;
    
    /**
     * @var string
     */
    public $phone_number;

    /**
     * @var string
     */
    public $username;

    /**
     * @var string
     */
    public $password;

    /**
     * @var int
     */
    public $role_id;

    /**
     * @var string
     */
    public $state;

    /**
     * @var string
     */
    public $role;

    /**
     * @var string
     */
    public $access_role;
}
