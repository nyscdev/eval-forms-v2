<?php
/**
 * Created by PhpStorm.
 * User: aueloka
 * Date: 1/23/2019
 * Time: 3:29 PM
 */

namespace App\Main\Features\StaffManagement\ViewModels;

use App\Main\Libraries\ViewModel;

class CreateStaffViewModel extends ViewModel
{
    public $roles;
    public $states;
    public $access_roles;
}
