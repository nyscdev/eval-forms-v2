<?php
namespace App\Main\Features\StaffManagement;

use App\Main\Controllers\Base\Controller;
use App\Main\Features\RoleManagement\RoleService;
use App\Main\Features\StaffManagement\ViewModels\CreateStaffViewModel;
use App\Main\Models\Enums\State;
use App\Main\Http\Enums\AccessRole;

class StaffController extends Controller
{
    private $staffService;
    private $roleService;

    public function __construct(StaffService $staffService, RoleService $roleService)
    {
        parent::__construct();
        $this->staffService = $staffService;
        $this->roleService = $roleService;
    }

    /**
     * @return string
     * @throws \App\Main\Exceptions\RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function getStaffCreationView()
    {
        if ($this->sessionManager->getCurrentUserAccessrole() == "Admin") {
            return $this->getStaffCreationViewForAdmin();
        }
        
        $roles = $this->roleService->getRoles();
        $states = State::getValues();


        $viewData = new CreateStaffViewModel();
        $viewData->roles = $roles;
        $viewData->states = $states;
        $viewData->access_roles = AccessRole::getValues();

        return $this->render('staff/create', $viewData);
    }

    public function getStaffCreationViewForAdmin()
    {
        $roles = $this->roleService->getRoles();
        $states = State::getValues();


        $viewData = new CreateStaffViewModel();
        $viewData->roles = $roles;
        $viewData->states = [$this->sessionManager->getCurrentUserState()];
        $accessRoles = AccessRole::getValues();
        
        //Remove the superadmin access_role from the list because admins shouldn't be able to create superadmins
        $key = \array_search("Superadmin", $accessRoles);
        unset($accessRoles[$key]);

        $viewData->access_roles = $accessRoles;

        return $this->render('staff/create', $viewData);
    }

    /**
     * @param $staffId
     * @return string
     * @throws \App\Main\Exceptions\RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function getResetPasswordView($staffId)
    {
        $viewData = [
            "staffId" => $staffId
        ];

        return $this->render("staff/reset-password", $viewData);
    }

    public function getEditStaffView($staffId)
    {
        // TODO: implement
    }

    /**
     * @param string $message Some message to show
     * @return mixed|string
     * @throws \App\Main\Exceptions\RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function viewStaff($message = "")
    {
        $staffList = $this->staffService->getAllStaff();

        $viewData = [
            "title" => "List of Staff",
            "staff" => $staffList,
            "processingMessage" => $message
        ];

        return $this->render('staff/list', $viewData);
    }

    /**
     * @return string
     * @throws \App\Main\Exceptions\RenderException
     */
    public function viewStaffForCurrentUserState()
    {
        $state = $this->sessionManager->getCurrentUserState();
        return $this->viewStaffForState($state);
    }

    /**
     * @param $state
     * @return string
     * @throws \App\Main\Exceptions\RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function viewStaffForState($state = "")
    {
        $staffList = $this->staffService->getAllStaffForState($state);

        $viewData = [
            "title" => "List of Staff for " . $state,     //what to do about titles - each page needs a title
            "staff" => $staffList
        ];

        return $this->render('staff/list', $viewData);
    }

    public function createStaff($data)
    {
        $this->staffService->createStaff($data);
        $this->redirectToSelf('viewStaff');
    }

    public function deleteStaff($staffId)
    {
        $this->staffService->deleteStaff($staffId);
        $this->redirectToSelf('viewStaff');
    }


    public function resetPassword($staffId, $password)
    {
        $this->staffService->resetPassword($staffId, $password);
        $this->redirectToSelf('viewStaff');

        /**
         * I have the mind to do something like this
         * $redirect_url .= $processingMessage
         *
         * before calling the redirect method
         */
    }

    public function editStaff()
    {
        // TODO: implement
    }
}
