<?php

namespace App\Main\Features\StaffManagement;

use App\Main\Repositories\UserRepository;
use App\Main\Repositories\StaffRepository;
use App\Main\Models\User;
use App\Main\Models\Staff;

class StaffService
{
    private $staffRepository;
    private $userRepository;

    public function __construct(StaffRepository $staffRepository, UserRepository $userRepository)
    {
        $this->staffRepository = $staffRepository;
        $this->userRepository = $userRepository;
    }

    // Methods for doing processing
    public function deleteStaff($staffId)
    {
        //delete both the staff and the corresponding user
        $this->staffRepository->deleteById($staffId);
        $this->userRepository->deleteById($staffId);
    }

    public function resetPassword($staffId, $password)
    {
        $user = $this->userRepository->findById($staffId);
        assert($user instanceof User);
        $user->setPassword($password);
        $this->userRepository->save($user);
    }

    public function createStaff($staffData)
    {
        $user = new User();
        $user->setUsername($staffData["username"]);
        $user->setPassword($staffData["password"]);
        $user->setRoleId($staffData["roleId"]);
        $user->setState($staffData["state"]);
        $user->setAccessRole($staffData["accessRole"]);
        $user->setUserType("Staff"); 
        
        $user = $this->userRepository->save($user);
        assert($user instanceof User);

        $staff = new Staff();
        $staff->setId($user->getId());
        $staff->setFirstName($staffData["firstname"]);
        $staff->setLastName($staffData["lastname"]);
        $staff->setPhoneNumber($staffData["phonenumber"]);

        $this->staffRepository->save($staff);
        return $staff;
    }

    public function getAllStaff()
    {
        return $this->staffRepository->findAllStaff();
    }

    public function getAllStaffForState($state)
    {
        return $this->staffRepository->findAllStaffForState($state);
    }
}
