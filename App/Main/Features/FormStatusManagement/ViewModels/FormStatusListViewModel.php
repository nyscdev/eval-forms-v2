<?php

namespace App\Main\Features\FormStatusManagement\ViewModels;

use App\Main\Libraries\ViewModel;

class FormStatusListViewModel extends ViewModel
{
    
    /**
     * @var array[FormRolesViewModel] array of forms and their corresponding roles
     */
    public $forms = [];
}
