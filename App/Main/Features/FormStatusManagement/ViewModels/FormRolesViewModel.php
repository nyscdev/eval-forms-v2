<?php

namespace App\Main\Features\FormStatusManagement\ViewModels;

use App\Main\Libraries\ViewModel;

class FormRolesViewModel extends ViewModel
{
    public $form;

    /**
     * @var array[Role] the roles that are able to fill this form
     */
    public $rolesForForm = [];
}
