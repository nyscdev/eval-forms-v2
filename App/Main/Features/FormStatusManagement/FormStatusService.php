<?php

namespace App\Main\Features\FormStatusManagement;

use App\Main\Models\FormStatus;
use App\Main\Repositories\FormStatusRepository;

class FormStatusService
{
    private $formStatusRepository;

    /**
     * FormStatusService constructor.
     * @param FormStatusRepository $formStatusRepository
     */
    public function __construct(FormStatusRepository $formStatusRepository)
    {
        $this->formStatusRepository = $formStatusRepository;
    }

    public function activateFormForRole($formId, $roleId)
    {
        $formStatus = $this->getFormStatus($formId, $roleId);
        $formStatus->setActive(1);
        $this->formStatusRepository->save($formStatus);
    }

    public function deactivateFormForRole($formId, $roleId)
    {
        $formStatus = $this->getFormStatus($formId, $roleId);

        //TODO: refactor these two lines later, very similar to the activateFormForRole method
        $formStatus->setActive(0);
        $this->formStatusRepository->save($formStatus);
    }

    public function getFormStatus($formId, $roleId)
    {
        $formStatus = array_values($this->formStatusRepository->findByFormIdAndRoleId($formId, $roleId))[0];
        if ($formStatus == null) {
            $formStatus = new FormStatus();
            $formStatus->setFormId($formId);
            $formStatus->setRoleId($roleId);
        }
        return $formStatus;
    }
}
