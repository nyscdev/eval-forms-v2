<?php

namespace App\Main\Features\FormStatusManagement;

use App\Main\Controllers\Base\Controller;
use App\Main\Exceptions\RenderException;
use App\Main\Features\FormManagement\FormService;
use App\Main\Features\FormStatusManagement\FormStatusService;
use App\Main\Features\FormStatusManagement\ViewModels\FormStatusListViewModel;
use App\Main\Features\FormStatusManagement\ViewModels\FormRolesViewModel;

class FormStatusController extends Controller
{
    /**
     * @var FormService
     */
    private $formService;

    /**
     * @var FormStatusService
     */
    private $formStatusService;

    /**
     * FormStatusController constructor.
     * @param FormService $formService
     */
    public function __construct(FormService $formService, FormStatusService $formStatusService)
    {
        parent::__construct();
        $this->formService = $formService;
        $this->formStatusService = $formStatusService;
    }

    /**
     * Returns a list of all available forms and the roles that are meant to fill those forms
     *
     * This list is essentially part of a view for activating and deactivating forms for some user roles
     * e.g Form4B is not active for Platoon Inspectors yet, but it is active for Corpers
     */
    public function getFormStatusListView($message = "")
    {
        $forms = $this->formService->getForms();
        $formStatusListViewModel = new FormStatusListViewModel("Manage Form Status For Roles");
        $formStatusListViewModel->processingMessage = $message;
        
        foreach ($forms as $form) {
            $formViewModel = new FormRolesViewModel();
            $formViewModel->form = $form;
            $formViewModel->rolesForForm = $this->formService->getRolesForForm($form->getId());

            $formStatusListViewModel->forms[] = $formViewModel;
        }

        return $this->render("/form-status/formStatus.twig", $formStatusListViewModel);
    }

    public function activateFormForRole($formId, $roleId)
    {
        $this->formStatusService->activateFormForRole($formId, $roleId);
        $message = "You have successfully activated this form";
        return $this->redirectToSelf("getFormStatusListView", ["message" => $message]);
    }

    public function deactivateFormForRole($formId, $roleId)
    {
        $this->formStatusService->deactivateFormForRole($formId, $roleId);
        $message = "You have successfully deactivated this form";
        return $this->redirectToSelf("getFormStatusListView", ["message" => $message]);
    }
}
