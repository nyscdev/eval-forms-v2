<?php

namespace App\Main\Features\FormManagement;

use App\Main\Controllers\Base\Controller;
use App\Main\Features\FormManagement\ViewModels\FormDetailsViewModel;
use App\Main\Features\FormManagement\ViewModels\FormsListViewModel;
use App\Main\Libraries\ViewModel;

class FormController extends Controller
{
    private $formService;

    public function __construct(FormService $formService)
    {
        parent::__construct();
        $this->formService = $formService;
    }

    /**
     * @return string
     * @throws \App\Main\Exceptions\RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function viewForms()
    {
        $forms = $this->formService->getForms();

        $formsViewModel = new FormsListViewModel("List of Forms");
        $formsViewModel->forms = $forms;

        //TODO: Replace path for view
        return $this->render("superadmin/form/list-forms", $formsViewModel);
    }

    public function viewForm($formId)
    {
        //TODO
        //This will allow the admin see the sections
        //and questions in the form. This way they can select a question or a section to edit
    }

    /**
     * Returns a PDF copy of the form
     *
     * @param $formId
     */
    public function printForm($formId)
    {
        $this->formService->printForm($formId);
        //TODO: Redirect
    }

    /**
     * @return string
     * @throws \App\Main\Exceptions\RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function getCreateFormView()
    {
        //TODO: Replace path for view
        return $this->render("superadmin/form/create-form", new ViewModel("Create Form"));
    }

    public function createForm($title, $description)
    {
        $this->formService->createForm($title, $description);
        $this->redirectToSelf('viewForms');
    }


    /**
     * @param $formId
     * @return string
     * @throws \App\Main\Exceptions\RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function getEditFormView($formId)
    {
        $form = $this->formService->getForm($formId);

        $formViewModel = new FormDetailsViewModel("Edit Form");
        $formViewModel->form = $form;

        //TODO: Replace path for view
        return $this->render("superadmin/form/edit-form", $formViewModel);
    }

    public function editForm($formId, $title, $description)
    {
        $this->formService->editForm($formId, $title, $description);
        $this->redirectToSelf('viewForms');
    }

    public function deleteForm($formId)
    {
        $this->formService->deleteForm($formId);
        $this->redirectToSelf('viewForms');
    }
}
