<?php

namespace App\Main\Features\FormManagement;

use App\Main\Models\Form;
use App\Main\Repositories\FormRepository;
use App\Main\Repositories\RoleRepository;
use App\Main\Features\FormSectionManagement\FormSectionService;

class FormService
{
    private $formRepository;
    private $roleRepository;
    private $formSectionService;

    /**
     * FormService constructor.
     * @param FormRepository $formRepository
     */
    public function __construct(
        FormRepository $formRepository,
        RoleRepository $roleRepository,
        FormSectionService $formSectionService
    ) {
        $this->formRepository = $formRepository;
        $this->roleRepository = $roleRepository;
        $this->formSectionService = $formSectionService;
    }

    public function getForms()
    {
        return $this->formRepository->findAll();
    }

    public function getForm($formId)
    {
        return $this->formRepository->findById($formId);
    }

    public function getFormsWithSectionsForCorpers()
    {
        $forms = $this->getForms();
        $formsForCorpers = [];

        foreach ($forms as $form) {
            $sectionsForCorpers = $this->formSectionService->getCorperSectionsForForm($form->getId());
            if ($sectionsForCorpers != null) {
                $formsForCorpers[] = $form;
            }
        }

        return $formsForCorpers;
    }

    public function getActiveFormsForUserRole($roleId)
    {
        return $this->formRepository->findActiveFormsForRole($roleId);
    }
    
    /**
     * @param $formTitle
     * @return Form
     */
    public function getFormWithTitle($formTitle)
    {
        return $this->formRepository->findByTitle($formTitle);
    }

    public function printForm($formId)
    {
        //TODO
    }

    public function createForm($title, $description)
    {
        $form = new Form();
        $form->setTitle($title);
        $form->setDescription($description);
        $this->formRepository->save($form);

        return $form;
    }

    public function editForm($formId, $title, $description)
    {
        $form = $this->formRepository->findById($formId);
        assert($form instanceof Form);
        $form->setTitle($title);
        $form->setDescription($description);
        $this->formRepository->save($form);

        return $form;
    }

    public function deleteForm($formId)
    {
        $this->formRepository->deleteById($formId);
    }

    /**
     * Some forms are filled by more than one role.
     * This method returns an array of all the roles responsible for filling a form
     */
    public function getRolesForForm($formId)
    {
        return $this->roleRepository->findRolesForForm($formId);
    }
}
