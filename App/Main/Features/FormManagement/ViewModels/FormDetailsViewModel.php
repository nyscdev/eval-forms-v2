<?php

namespace App\Main\Features\FormManagement\ViewModels;


use App\Main\Libraries\ViewModel;
use App\Main\Models\Form;

class FormDetailsViewModel extends ViewModel
{
    /**
     * @var Form A form
     */
    public $form;
}