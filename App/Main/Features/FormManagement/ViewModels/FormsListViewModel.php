<?php

namespace App\Main\Features\FormManagement\ViewModels;


use App\Main\Libraries\ViewModel;

class FormsListViewModel extends ViewModel
{
    /**
     * @var array A list of forms
     */
    public $forms;
}