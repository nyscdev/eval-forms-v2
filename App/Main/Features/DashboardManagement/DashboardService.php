<?php

namespace App\Main\Features\DashboardManagement;

use App\Main\Features\StaffManagement\StaffController;
use App\Main\Http\Routing\Router;

class DashboardService
{
    public function getHomeUrlForUserRole($roleId)
    {
        //TODO: IMPLEMENT THIS METHOD SO IT RETURNS THE HOME URL DEPENDING ON THE ROLE
        //OF THE CURRENTLY LOGGED IN USER
        return Router::getUrlForMethod(StaffController::class, "viewStaff");
    }
}
