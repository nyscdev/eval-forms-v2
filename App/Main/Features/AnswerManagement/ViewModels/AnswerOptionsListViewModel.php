<?php

namespace App\Main\Features\AnswerManagement\ViewModels;


use App\Main\Libraries\ViewModel;
use App\Main\Models\Form;
use App\Main\Models\Question;
use App\Main\Models\Section;

class AnswerOptionsListViewModel extends ViewModel
{
    /**
     * @var Form
     */
    public $form;

    /**
     * @var Section
     */
    public $section;

    /**
     * @var Question
     */
    public $question;

    /**
     * @var array
     */
    public $answerOptions;
}