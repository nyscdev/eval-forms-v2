<?php

namespace App\Main\Features\AnswerManagement;


use App\Main\Libraries\Log;
use App\Main\Models\AnswerOption;
use App\Main\Repositories\AnswerOptionRepository;

class AnswerService
{
    private $answerRepository;

    /**
     * AnswerService constructor.
     * @param $answerRepository AnswerOptionRepository
     */
    public function __construct(AnswerOptionRepository $answerRepository)
    {
        $this->answerRepository = $answerRepository;
    }

    public function getOptionsForQuestion($questionId)
    {
        return $this->answerRepository->findByQuestionId($questionId);
    }

    /**
     * @param $optionId
     * @return AnswerOption
     */
    public function getOption($optionId)
    {
        Log::info("Getting Option: " . $optionId);
        $option = $this->answerRepository->findById($optionId);
        Log::info("Obtained Option: ");
        Log::info($option);
        assert($option instanceof AnswerOption);
        return $option;
    }

    /**
     * Returns a list of options with the specified question id
     *
     * @param $questionId
     * @return array
     */
    public function getOptionsWithQuestionId($questionId)
    {
        return $this->answerRepository->findByQuestionId($questionId);
    }

    public function createOption($questionId, $optionText)
    {
        $answerOption = new AnswerOption();
        $answerOption->setAnswerOption($optionText);
        $answerOption->setQuestionId($questionId);
        $this->answerRepository->save($answerOption);

        return $answerOption;
    }

    public function editOption($optionId, $newText)
    {
        $answerOption = $this->getOption($optionId);
        $answerOption->setAnswerOption($newText);
        $this->answerRepository->save($answerOption);

        return $answerOption;
    }

    public function deleteOption($optionId)
    {
        return $this->answerRepository->deleteById($optionId);
    }
}