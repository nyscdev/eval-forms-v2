<?php

namespace App\Main\Features\AnswerManagement;


use App\Main\Controllers\Base\Controller;
use App\Main\Features\AnswerManagement\ViewModels\AnswerOptionDetailsViewModel;
use App\Main\Features\AnswerManagement\ViewModels\AnswerOptionsListViewModel;
use App\Main\Features\FormManagement\FormService;
use App\Main\Features\FormSectionManagement\FormSectionService;
use App\Main\Features\QuestionManagement\QuestionService;
use App\Main\Libraries\Log;

class AnswerController extends Controller
{
    private $answerService;
    private $questionService;
    private $sectionService;
    private $formService;

    /**
     * AnswerController constructor.
     * @param AnswerService $answerService
     * @param QuestionService $questionService
     * @param FormSectionService $formSectionService
     * @param FormService $formService
     */
    public function __construct(AnswerService $answerService, QuestionService $questionService, FormSectionService $formSectionService, FormService $formService)
    {
        parent::__construct();
        $this->answerService = $answerService;
        $this->questionService = $questionService;
        $this->sectionService = $formSectionService;
        $this->formService = $formService;
    }

    /**
     * @param $questionId
     * @return string
     * @throws \App\Main\Exceptions\RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function viewAnswerOptionsForQuestion($questionId)
    {
        $answerOptions = $this->answerService->getOptionsForQuestion($questionId);
        $question = $this->questionService->getQuestion($questionId);
        $section = $this->sectionService->getSection($question->getSectionId());
        $form = $this->formService->getForm($section->getFormId());

        $answersViewModel = new AnswerOptionsListViewModel("List of options for question");
        $answersViewModel->answerOptions = $answerOptions;
        $answersViewModel->question = $question;
        $answersViewModel->section = $section;
        $answersViewModel->form = $form;

        //TODO: Replace path view
        return $this->render('superadmin/form/answer-options/list-options', $answersViewModel);
    }

    /**
     * @param $questionId
     * @return string
     * @throws \App\Main\Exceptions\RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function getCreateAnswerOptionView($questionId)
    {
        $question = $this->questionService->getQuestion($questionId);
        $section = $this->sectionService->getSection($question->getSectionId());
        $form = $this->formService->getForm($section->getFormId());

        $answerViewModel = new AnswerOptionDetailsViewModel("Create New Answer Option");
        $answerViewModel->question = $question;
        $answerViewModel->section = $section;
        $answerViewModel->form = $form;

        //TODO: Replace path view
        return $this->render('superadmin/form/answer-options/create-option', $answerViewModel);
    }

    public function createAnswerOption($questionId, $optionText)
    {
        Log::info("Creating Option called: " . $optionText . " for question with ID: " . $questionId);
        $answerOption = $this->answerService->createOption($questionId, $optionText);
        $this->redirectToSelf('viewAnswerOptionsForQuestion', ["questionId" => $answerOption->getQuestionId()]);
    }

    /**
     * @param $optionId
     * @return string
     * @throws \App\Main\Exceptions\RenderException
     * @throws \App\Main\Exceptions\NavigationException
     */
    public function getEditAnswerOptionView($optionId)
    {
        $answerOption = $this->answerService->getOption($optionId);
        $question = $this->questionService->getQuestion($answerOption->getQuestionId());
        $section = $this->sectionService->getSection($question->getSectionId());
        $form = $this->formService->getForm($section->getFormId());

        $answerViewModel = new AnswerOptionDetailsViewModel("Edit Option");
        $answerViewModel->answerOption = $answerOption;
        $answerViewModel->question = $question;
        $answerViewModel->section = $section;
        $answerViewModel->form = $form;

        //TODO: Replace path view
        return $this->render('superadmin/form/answer-options/edit-option', $answerViewModel);
    }

    public function editAnswerOption($optionId, $optionText)
    {
        $answerOption = $this->answerService->editOption($optionId, $optionText);
        $this->redirectToSelf('viewAnswerOptionsForQuestion', ["questionId" => $answerOption->getQuestionId()]);
    }

    public function deleteAnswerOption($optionId)
    {
        $answerOption = $this->answerService->getOption($optionId);
        $this->answerService->deleteOption($optionId);
        $this->redirectToSelf('viewAnswerOptionsForQuestion', ["questionId" => $answerOption->getQuestionId()]);

    }

}