<?php

namespace App\Main\Features\DataManagement;


use App\Main\Controllers\Base\Controller;
use App\Main\Libraries\ViewModel;

class DataManagementController extends Controller
{
    /**
     * @var DataManagementService
     */
    private $dataManagementService;

    public function __construct(DataManagementService $dataManagementService)
    {
        parent::__construct();
        $this->dataManagementService = $dataManagementService; 
    }

    public function getDataManagementView($message = "")
    {
        $data = new ViewModel("Data Management View", $message);

        return $this->render('data-management/data-management', $data);
    }

    public function backUpToDropbox()
    {
        if($this->dataManagementService->backUpDatabase() == true)
        {
            $this->redirectToSelf("getDataManagementView", ["message" => "Backup Successful"]); 
        } else {
            $this->redirectToSelf("getDataManagementView", ["message" => "Backup Failed"]); 
        }
    }

    // These methods are unsave, no checks, validations, catching of exceptions, nothing, so improve them later.
    public function clearLivestats()
    {
        $this->dataManagementService->clearLivestats(); 
        $this->redirectToSelf("getDataManagementView", ["message" => "Live stats cleared"]); 
    }

    public function clearResponses()
    {
        $this->dataManagementService->clearResponses(); 
        $this->redirectToSelf("getDataManagementView", ["message" => "Question Responses Table cleared"]); 
    }

    public function clearCorpers()
    {
        $this->dataManagementService->clearCorpers(); 
        $this->redirectToSelf("getDataManagementView", ["message" => "Corpers cleared successfully"]); 
    }
}