<?php

namespace App\Main\Features\DataManagement;


use App\Main\Features\DataManagement\CloudServiceProviders\CloudServiceProvider;
use App\Main\Libraries\Database;
use App\Main\Libraries\ServiceLocator;
use App\Main\Repositories\LiveStatsRepository; 
use App\Main\Repositories\CorperRepository; 
use App\Main\Repositories\UserRepository; 
use App\Main\Repositories\QuestionResponseRepository; 

class DataManagementService
{
    /**
     * @var Database
     */
    private $database;

    /**
     * @var CloudServiceProvider
     */
    private $cloudServiceProvider;

    /**
     * @var LiveStatsRepository
     */
    private $liveStatsRepository; 

    /**
     * @var CorperRepository
     */
    private $corperRepository;

    /**
     * @var UserRepository
     */
    private $userRepository; 

    /**
     * @var QuestionResponseRepository
     */
    private $questionResponseRepository; 

    /**
     * DataManagementService constructor.
     * @param Database $database
     * @param CloudServiceProvider $cloudServiceProvider
     */
    public function __construct(
        Database $database, 
        CloudServiceProvider $cloudServiceProvider,
        LiveStatsRepository $liveStatsRepository,
        CorperRepository $corperRepository,
        UserRepository $userRepository,
        QuestionResponseRepository $questionResponseRepository
    ){
        $this->database = $database;
        $this->cloudServiceProvider = $cloudServiceProvider;
        $this->liveStatsRepository = $liveStatsRepository;
        $this->corperRepository = $corperRepository;
        $this->userRepository = $userRepository; 
        $this->questionResponseRepository = $questionResponseRepository; 
    }


    /**
     * @param array|null $tables Optional array of tables to back up, defaults to all tables
     * @param null $successUri
     * @return bool true if the backup was successful, false otherwise
     */
    public function backUpDatabase(array $tables = null, $successUri = null)
    {
        $filename = APP_ROOT . DIRECTORY_SEPARATOR . DB_NAME . '-backup-' . date('dMY_h:m:sA') . '.sql';
        $isBackedUp = $this->database->exportDatabaseTables($filename, $tables);
        $uploaded = $this->cloudServiceProvider->upload($filename, $successUri, true);
        return $isBackedUp && $uploaded;
    }

    /**
     * Should there be a need to switch to an alternative cloud service, an implementation of that may be passed in here.
     * Defaults to whatever is registered with @see ServiceLocator
     * @param CloudServiceProvider $cloudServiceProvider
     */
    public function setCloudServiceProvider(CloudServiceProvider $cloudServiceProvider)
    {
        $this->cloudServiceProvider = $cloudServiceProvider;
    }

    public function clearLivestats()
    {
        $this->liveStatsRepository->deleteAll(); 
    }

    public function clearResponses()
    {
        $this->questionResponseRepository->deleteAll(); 
    }

    public function clearCorpers(){
        //before attempting to delete corpers from the database, ensure there are no responses attached to the corpers
        //to avoid foreign key constraint validation errors
        $this->clearResponses(); 

        $corpers = $this->corperRepository->findAll(); 
        foreach($corpers as $corper)
        {
            // delete both the corper and the corresponding user in this order to avoid foreign key constraing failure
            // Recall that the corper table Id column is user_id

            $this->corperRepository->delete($corper); 
            $this->userRepository->deleteById($corper->getId()); 
        }
    }
}