<?php

namespace App\Main\Features\DataManagement\CloudServiceProviders;


interface CloudServiceProvider
{
    public function upload(string $filename, string $successUrl = null, bool $deleteFileOnCompletion = true);

    public function download(string $filename, string $downloadDestination);
}