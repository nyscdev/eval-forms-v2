<?php

namespace App\Main\Features\DataManagement\CloudServiceProviders;


use App\Main\Libraries\Log;

class FileCloudServiceProvider implements CloudServiceProvider
{

    /**
     * @var string
     */
    private $uploadPath;

    /**
     * FileCloudServiceProvider constructor.
     */
    public function __construct()
    {
        $this->uploadPath = SITE_ROOT . DIRECTORY_SEPARATOR . "cloud_uploads";
    }

    public function upload(string $filename, string $successUrl = null, bool $deleteFileOnCompletion = true)
    {
        if (!file_exists($this->uploadPath)) {
            mkdir($this->uploadPath, 0777, true);
        }

        Log::info("Uploading " . $filename);
        $file = explode(DIRECTORY_SEPARATOR, $filename);
        $file = $file[count($file) - 1];
        $contents = file_get_contents($filename);
        $uploadedFile = fopen($this->uploadPath . DIRECTORY_SEPARATOR . $file, "w") or die("Unable to open file!");
        fwrite($uploadedFile, $contents);
        fclose($uploadedFile);

        if ($deleteFileOnCompletion) {
            unlink($filename);
        }

        return true;
    }

    public function download(string $filename, string $downloadDestination)
    {
        // TODO: Implement download() method.
    }
}