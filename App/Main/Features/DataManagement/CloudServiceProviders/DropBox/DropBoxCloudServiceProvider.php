<?php

namespace App\Main\Features\DataManagement\CloudServiceProviders\DropBox;


use App\Main\Exceptions\CloudServiceException;
use App\Main\Features\DataManagement\CloudServiceProviders\CloudServiceProvider;
use App\Main\Features\DataManagement\CloudServiceProviders\DropBox\Clients\DropBoxClient;
use function App\Main\Helpers\redirect;
use App\Main\Http\Routing\Router;
use App\Main\Http\Session\SessionManager;
use App\Main\Libraries\Log;

class DropBoxCloudServiceProvider implements CloudServiceProvider
{
    /*
     * We have 2 options here:
     *
     * 1.   Get an Authentication Token for the account we want to use and store hard-code it here.
     *      The app will use this to connect to DropBox.
     *
     *      Con:
     *          This means that anyone that gets access to the code can
     *          mess with the DropBox account (Security risk)
     *      Pro:
     *          Connection happens seamlessly as well as upload and download without any external interaction
     *
     * 2.   Register the App as an Oauth client with DropBox.
     *      The administrator will receive a prompt whenever the app needs to connect and will enter the credentials.
     *      The app uses this to obtain a token from DropBox and proceed with it's process
     *
     *      Con:
     *          Not very convenient, there would be break of flow to enter DropBox credentials and other things that come with it.
     *      Pro:
     *          Much more secure
     * */

    /**
     * @var SessionManager
     */
    private $sessionManager;

    /**
     * @var DropBoxClient
     */
    private $dropBoxClient;

    /**
     * @var bool Set to true after authenticate is called.
     */
    private $authenticated = false;


    /**
     * DropBoxCloudServiceProvider constructor.
     * @param SessionManager $sessionManager
     * @param DropBoxClient $dropBoxClient
     */
    public function __construct(SessionManager $sessionManager, DropBoxClient $dropBoxClient)
    {
        $this->sessionManager = $sessionManager;
        $this->dropBoxClient = $dropBoxClient;
    }

    public function upload(string $filename, string $successUrl = null, bool $deleteFileOnCompletion = true)
    {
        $state = 'upload/' . $filename . ';' . $successUrl . ';' . $deleteFileOnCompletion;
        $this->validateAccess($state);

        Log::info("Uploading " . $filename);
        $file = explode(DIRECTORY_SEPARATOR, $filename);
        $file = '/' . $file[count($file) - 1];
        $metadata = $this->dropBoxClient->upload($filename, $file);

        if ($deleteFileOnCompletion) {
            Log::info('Deleting file');
            unlink($filename);
        }

        if (isset($successUrl)) {
            redirect($successUrl);
        } else {
            return $metadata != null;
        }
    }

    public function download(string $filename, string $downloadDestination)
    {
        // TODO: Implement download() method.
    }

    /**
     * @param $state
     */
    public function validateAccess($state)
    {
        if ($this->authenticated) {
            return;
        }

        if (is_null($this->dropBoxClient->getAccessToken())) {
            $callback = Router::getUrlForMethod(DropBoxApiController::class, 'dropBoxApiCallback');

            $authUrl = $this->dropBoxClient->getAuthUrl($callback, [], $state);
            Log::info('Redirecting To: ' . $authUrl);
            Log::info('Callback: ' . $callback);
            redirect($authUrl);
        }
    }

    /**
     * @param $code
     * @param $state
     * @param $redirectUri
     * @throws CloudServiceException
     */
    public function authenticate($code, $state, $redirectUri)
    {
        $accessToken = $this->dropBoxClient->getAccessToken($code, $state, $redirectUri);
        $this->sessionManager->storeCustomSessionData(DROP_BOX_TOKEN_STORE, $accessToken);

        if (!isset($state)) {
            return;
        }

        $stateData = explode('/', $state);

        if (count($stateData) != 2) {
            throw new CloudServiceException("Invalid format used for state. Should be {method/param1;param2;...;paramN}");
        }

        $methodName = $stateData[0];
        $params = explode(';', $stateData[1]);
        $this->authenticated = true;

        call_user_func_array([$this, $methodName], $params);
    }
}