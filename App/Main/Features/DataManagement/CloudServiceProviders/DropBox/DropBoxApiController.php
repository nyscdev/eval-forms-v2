<?php

namespace App\Main\Features\DataManagement\CloudServiceProviders\DropBox;

use App\Main\Controllers\Base\Controller;
use App\Main\Http\Routing\Router;

class DropBoxApiController extends Controller
{

    /**
     * @var DropBoxCloudServiceProvider
     */
    private $dropBoxCloudServiceProvider;

    /**
     * DropBoxAuthController constructor.
     * @param DropBoxCloudServiceProvider $dropBoxCloudServiceProvider
     */
    public function __construct(DropBoxCloudServiceProvider $dropBoxCloudServiceProvider)
    {
        parent::__construct();
        $this->dropBoxCloudServiceProvider = $dropBoxCloudServiceProvider;
    }

    /**
     * @param $code
     * @param null $state
     * @throws \App\Main\Exceptions\CloudServiceException
     */
    public function dropBoxApiCallback($code, $state = null)
    {
        $url = Router::getUrlForMethod(DropBoxApiController::class, 'dropBoxApiCallback');
        $this->dropBoxCloudServiceProvider->authenticate($code, $state, $url);
    }
}
