<?php

namespace App\Main\Features\DataManagement\CloudServiceProviders\DropBox\Clients;


use App\Main\Http\Session\SessionManager;
use Kunnu\Dropbox\Dropbox;
use Kunnu\Dropbox\DropboxApp;

/**
 * Provides a wrapper around the DropBox SDK components.
 *
 * Class DropBoxClientImpl
 * @package App\Main\Features\DataManagement\CloudServiceProviders\DropBox
 */
class KunnuSdkDropBoxClient implements DropBoxClient
{
    /**
     * If null, the user will be redirected to sign in to DropBox
     *
     * @var string|null The access token for the required DropBox account
     */
    private $accessToken = null;

    /**
     * @var Dropbox
     */
    private $dropBox;

    /**
     * @var SessionManager
     */
    private $sessionManager;


    /**
     * DropBoxWrapperImpl constructor.
     * @param SessionManager $sessionManager
     */
    public function __construct(SessionManager $sessionManager)
    {
        $this->sessionManager = $sessionManager;
        $this->accessToken = "noBqWNfrhOAAAAAAAAAADcIZzQM4CUbhsmh7FkrQTB84B_dciuCDVE3Vq5BBK_HW"; 
        $this->accessToken = is_null($this->accessToken) ? $sessionManager->getCustomSessionData(DROP_BOX_TOKEN_STORE) : $this->accessToken;
        $dropBoxApp = new DropboxApp(DROP_BOX_APP_KEY, DROP_BOX_APP_SECRET, $this->accessToken);
        $this->dropBox = new Dropbox($dropBoxApp);
    }

    public function upload($filename, $file)
    {
        return $this->dropBox->upload($filename, $file);
    }

    public function getAuthUrl($callback, $params = [], $state = null)
    {
        return $this->dropBox->getAuthHelper()->getAuthUrl($callback, $params, $state);
    }

    /**
     * @param $code
     * @param string|null $state
     * @param string|null $redirectUri
     * @return string
     */
    public function getAccessToken($code = null, $state = null, $redirectUri = null)
    {
        if (!isset($code)) {
            return $this->accessToken;
        }

        $accessToken = $this->dropBox->getAuthHelper()->getAccessToken($code, $state, $redirectUri);
        return $accessToken->getToken();
    }
}