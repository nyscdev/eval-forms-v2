<?php

namespace App\Main\Features\DataManagement\CloudServiceProviders\DropBox\Clients;

/**
 * Interface providing a wrapper around the used DropBox SDK
 *
 * Interface DropBoxClient
 * @package App\Main\Features\DataManagement\CloudServiceProviders\DropBox
 */
interface DropBoxClient
{
    public function upload($filename, $file);

    public function getAuthUrl($callback, $params = [], $state = null);

    /**
     * @param $code
     * @param string|null $state
     * @param string|null $redirectUri
     * @return string
     */
    public function getAccessToken($code = null, $state = null, $redirectUri = null);
}