<?php

namespace App\Main\Features\Authentication;

use App\Main\Exceptions\UnauthorizedAccessException;
use App\Main\Libraries\Log;
use App\Main\Repositories\UserRepository;

class AuthenticationService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * AuthenticationService constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param string $username
     * @param string $password
     * @return \App\Main\Models\User|null
     * @throws UnauthorizedAccessException
     */
    public function authenticateCredentials(string $username, string $password)
    {
        Log::info("Searching user");
        $user = $this->userRepository->findByUsername($username);
        Log::info("User: " . $user);

        if (is_null($user) || !$user->passwordEquals($password)) {
            Log::info("Password doesn't match or username incorrect.");
            throw new UnauthorizedAccessException("Username or password is incorrect.");
        }

        Log::info("Authenticated " . $username . " Good to go");
        return $user;
    }
}
