<?php

namespace App\Main\Features\Authentication;

use App\Main\Controllers\Base\Controller;
use App\Main\Controllers\HomeController;
use App\Main\Exceptions\RenderException;
use App\Main\Libraries\Log;
use App\Main\Libraries\ViewModel;

/**
 * Class AuthController
 *
 * Handles all authentication mechanisms.
 *
 * Both staff and corpers will have different login pages.
 * Corpers will see fields for state code and surname
 * staff / admins will see fields for username and password.
 * Behind the scenes however, it's the same authentication mechanism
 * because for corpers their state code is the username
 * and their password is the surname.
 */
class AuthController extends Controller
{
    /**
     * @var AuthenticationService
     */
    private $authenticationService;

    /**
     * AuthController constructor.
     * @param AuthenticationService $authenticationService
     */
    public function __construct(AuthenticationService $authenticationService)
    {
        parent::__construct();
        $this->authenticationService = $authenticationService;
    }

    /**
     * Returns a view for the login page.
     *
     * @return string
     * @throws RenderException
     */
    public function getLoginView($message = "")
    {
        return $this->render('auth/login', new ViewModel("Login", $message));
    }

    /**
     * Verifies the username and password and logs a user in.
     *
     * @param $username
     * @param $password
     * @throws \App\Main\Exceptions\UnauthorizedAccessException
     */
    public function login($username, $password)
    {
        $user = $this->authenticationService->authenticateCredentials($username, $password);
        $this->sessionManager->setCurrentUser($user);
        Log::info("User logged in.");
        $this->redirectTo(HomeController::class, 'index');
    }

    public function logout()
    {
        $this->sessionManager->revokeUserAccess();
        $this->redirectToSelf('getLoginView');
    }
}
