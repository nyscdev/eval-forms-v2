<?php

namespace App\Tests\Helpers;

use App\Main\Libraries\Log;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class OutputInterceptorTest extends TestCase
{
    public function testWillInterceptLog()
    {
        Log::info("Please intercept this.");
        Log::info("And this");

        $var = ob_get_contents();

        Assert::assertTrue(strpos($var, "Please intercept this.") !== false);
        Assert::assertTrue(strpos($var, "And this") !== false);
    }
}
