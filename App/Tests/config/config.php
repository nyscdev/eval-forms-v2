<?php
namespace App\Tests\Config;
use PDO;
// App root
define("APP_ROOT", dirname(__FILE__, 3));
define("SITE_ROOT", dirname(__FILE__, 4));

// Url root
define("URL_ROOT", "http://localhost/eval-forms-v2");
// Define Site name
define("SITE_NAME", "NYSC Evaluation");
define('VIEW_ROOT', APP_ROOT.'/views');

// Define database parameters
define("LOG_FILE", "logs/nysc_evaluation.log");
define("DB_CONNECTION", "mysql");
define("DB_USERNAME", "root");
define("DB_PASSWORD", "");
define("DB_NAME", "nysc_evaluation_test");
define("DB_HOST", "127.0.0.1");
define("DB_PORT", "3306");
define("DB_OPTIONS", [
  PDO::ATTR_PERSISTENT => true,
  PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
]);

//Dropbox Params
define('DROP_BOX_APP_KEY', 'yjv68l26gbc494v');
define('DROP_BOX_APP_SECRET', 'ynlj6x1dkjryu9h');
define('DROP_BOX_TOKEN_STORE', 'nysc.evaluations.cloudservice.dropbox.access_token');


define('MODE', 'DEBUG');
