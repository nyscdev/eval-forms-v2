<?php

namespace App\Tests\Http\Mocks;

use App\Main\Controllers\Base\Controller;
use App\Main\Libraries\Log;

class MockController extends Controller
{
    public function mockMethod($mockParam)
    {
        Log::info("Called mockMethod with param: ". $mockParam);
        return true;
    }

    public function mockPostMethod(MockRequestObject $requestObject)
    {
        Log::info($requestObject);
    }
}
