<?php

namespace App\Tests\Http\Mocks;

class MockRequestObject
{
    private $propA;

    private $propB;

    private $propC;

    /**
     * @return mixed
     */
    public function getPropA()
    {
        return $this->propA;
    }

    /**
     * @param mixed $propA
     */
    public function setPropA($propA): void
    {
        $this->propA = $propA;
    }

    /**
     * @return mixed
     */
    public function getPropB()
    {
        return $this->propB;
    }

    /**
     * @param mixed $propB
     */
    public function setPropB($propB): void
    {
        $this->propB = $propB;
    }

    /**
     * @return mixed
     */
    public function getPropC()
    {
        return $this->propC;
    }

    /**
     * @param mixed $propC
     */
    public function setPropC($propC): void
    {
        $this->propC = $propC;
    }
}
