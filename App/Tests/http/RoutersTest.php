<?php

namespace App\Tests\Http;

use App\Main\Http\Enums\HTTPMethod;
use App\Main\Http\RequestManagement\Request;
use App\Main\Http\Routing\Router;
use App\Main\Http\Session\MockSessionManager;
use App\Main\Http\Session\SessionManager;
use App\Main\Libraries\Navigation\NavigationBuilder;
use App\Main\Libraries\Navigation\XMLNavigation\XMLNavigationBuilder;
use App\Main\Libraries\Rendering\Renderer;
use App\Main\Libraries\Rendering\Twig\TwigRenderer;
use App\Main\Libraries\ServiceLocator;
use App\Tests\Http\Mocks\MockController;
use Exception;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;
use Twig_Loader_Filesystem;
use Twig_LoaderInterface;

class RoutersTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();

        ServiceLocator::registerImplementations([
            Renderer::class => TwigRenderer::class,
            NavigationBuilder::class => XMLNavigationBuilder::class,
            SessionManager::class => MockSessionManager::class
        ]);

        ServiceLocator::registerInstances([
            Twig_LoaderInterface::class => new Twig_Loader_Filesystem(VIEW_ROOT)
        ]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        ServiceLocator::clearAll();
    }

    public function testCanRegisterRouteSuccessfully()
    {
        Router::register('/super_mocker/hi/')->withController(MockController::class)->withHandler('mockMethod');
        Router::register('/super_mocker/hi/', HTTPMethod::POST)->
        withController(MockController::class)->withHandler('mockPostMethod');
        Assert::assertTrue(true);
    }


    /**
     * @depends testCanRegisterRouteSuccessfully
     */
    public function testGetURLForMethodWillReturnFullURL()
    {

        $url = Router::getUrlForMethod(MockController::class, 'mockMethod');
        Assert::assertEquals("http://localhost/eval-forms-v2/super_mocker/hi", $url);
    }

    /**
     * @depends testCanRegisterRouteSuccessfully
     * @throws Exception
     */
    public function testWillRouteRequestToAppropriateMethod()
    {
        $request = new Request('super_mocker/hi/');
        $request->setParams(['mockParam' => 234]);

        $routed = Router::routeRequest($request);
        $logs = ob_get_contents();

        Assert::assertTrue(strpos($logs, "Called mockMethod with param: 234") !== false);
        Assert::assertTrue($routed);
    }

    /**
     * @depends testCanRegisterRouteSuccessfully
     * @throws Exception
     */
    public function testWillRoutePostRequest()
    {
        $request = new Request('super_mocker/hi/', HTTPMethod::POST);
        $request->setParams(['propA' => 234, 'propC' => 'stringParam']);

        $routed = Router::routeRequest($request);
        Assert::assertTrue($routed);
    }
}
