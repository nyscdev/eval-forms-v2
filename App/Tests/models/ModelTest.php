<?php

namespace App\Tests\Models;

use App\Main\Libraries\Log;
use App\Main\Models\Base\Model;
use App\Main\Models\User;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Assert;

//require_once '../config/config.php';

class ModelTest extends TestCase
{
    /**
     * @var Model
     */
    private $userModel;

    protected function setUp(): void
    {
        parent::setUp();
        $this->userModel = new User();
    }

    /**
     * @test
     */
    public function testWillReturnInsertSyntaxForUserModel()
    {
        \assert($this->userModel instanceof User);
        $this->userModel->setId(2);
        $this->userModel->setUsername("mockUser");
        $this->userModel->setPassword("mockPassword");
        $this->userModel->setRoleId(3);

        $insertSyntax = $this->userModel->getInsertSyntax(['password']);
        Log::info($insertSyntax);

        Assert::assertEquals("(id, username, role_id) VALUES (2, mockUser, 3)", $insertSyntax);
    }

    /**
     * @test
     */
    public function testWillReturnUpdateSyntaxForUserModel()
    {
        \assert($this->userModel instanceof User);
        $this->userModel->setId(2);
        $this->userModel->setUsername("mockUser");
        $this->userModel->setPassword("mockPassword");
        $this->userModel->setRoleId(3);

        $updateSyntax = $this->userModel->getUpdateSyntax(true, ["password"]);
        Log::info($updateSyntax);

        Assert::assertEquals("id=2, username=mockUser, role_id=3", $updateSyntax);
    }

    public function testWillSkipNullPropertiesWhenGettingInsertSyntax()
    {
        \assert($this->userModel instanceof User);
        $this->userModel->setId(2);
        $this->userModel->setPassword("mockPassword");
        $this->userModel->setRoleId(3);

        $insertSyntax = $this->userModel->getInsertSyntax(["password"]);
        Log::info($insertSyntax);

        Assert::assertEquals("(id, role_id) VALUES (2, 3)", $insertSyntax);
    }

    /**
     * @test
     */
    public function testWillSkipNullPropertiesWhenGettingUpdateSyntax()
    {
        \assert($this->userModel instanceof User);
        $this->userModel->setId(2);
        $this->userModel->setPassword("mockPassword");
        $this->userModel->setRoleId(3);

        $updateSyntax = $this->userModel->getUpdateSyntax(true, ["password"]);
        Log::info($updateSyntax);

        Assert::assertEquals("id=2, role_id=3", $updateSyntax);
    }
}
