<?php

namespace App\Tests\Repositories\Base;

use App\Main\Exceptions\ServiceNotFoundException;
use App\Main\Libraries\Database;
use App\Main\Libraries\ServiceLocator;
//use PHPUnit\DbUnit\Database\Connection;
//use PHPUnit\DbUnit\TestCaseTrait;
use PHPUnit\Framework\TestCase;

abstract class RepositoryTestCase extends TestCase
{
//    use TestCaseTrait;

    /**
     * @var Database
     */
    protected $database;


    /**
     * DefaultUserRepositoryTest constructor.
     * @throws ServiceNotFoundException
     */
    public function __construct()
    {
        parent::__construct();
        $this->database = ServiceLocator::get(Database::class);
    }


//    /**
//     * Returns the test database connection.
//     *
//     * @return Connection
//     */
//    protected function getConnection()
//    {
//        return $this->createDefaultDBConnection($this->database->getConnection(), DB_NAME);
//    }
}
