<?php

namespace App\Tests\Repositories\Mocks;

use App\Main\Libraries\Log;
use App\Main\Models\Base\Model;
use App\Main\Models\User;
use App\Main\Repositories\UserRepository;
use InvalidArgumentException;

class InMemoryUserRepository implements UserRepository
{

    /**
     * @var User[]
     */
    private $users = [];
    private $lastId;

    public function save($object)
    {
        Log::info("Attempting to save user...");

        if (!($object instanceof User)) {
            Log::error("Object is not a user.");
            throw new InvalidArgumentException("Object is not of a valid type.");
        }

        $this->lastId += 1;
        $object->setId($this->lastId);
        $this->users[] = $object;
        Log::info("Saved.");
    }

    public function findById($id)
    {
        return array_filter($this->users, function (User $object) use ($id) {
            return $object->getId() == $id;
        });
    }

    public function findAll()
    {
        return $this->users;
    }

    public function findByUsername($username)
    {
        return array_filter($this->users, function (User $object) use ($username) {
            return $object->getUsername() == $username;
        });
    }

    public function findByUsernameAndPassword($username, $password)
    {
        return array_filter($this->users, function (User $object) use ($username, $password) {
            return $object->getUsername() == $username and $object->getPassword() == $password;
        });
    }

    public function findByRoleId($roleId)
    {
        return array_filter($this->users, function (User $object) use ($roleId) {
            return $object->getRoleId() == $roleId;
        });
    }

    /**
     * @param $object Model. The object to delete.
     * @return bool true if successful. false otherwise.
     */
    public function delete($object)
    {
        return true;
    }

    /**
     * Inserts or updates the database table with the objects specified.
     *
     * @param array $objects The Model objects to insert
     * @param bool $updateIdColumnOnDuplicate If true, the ID column will attempt to update. This may lead to constraint violations.
     * @return bool True if successful, false otherwise
     */
    public function saveMultiple(array $objects, $updateIdColumnOnDuplicate = false)
    {
        // TODO: Implement saveMultiple() method.
    }

    /**
     * Delete an object with the specified ID.
     *
     * @param $id mixed The id of the object to delete
     * @return bool true if successful. false otherwise.
     */
    public function deleteById($id)
    {
        // TODO: Implement deleteById() method.
    }

    public function findByState($state)
    {
        // TODO: Implement findByState() method.
    }
}
