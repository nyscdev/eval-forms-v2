<?php

namespace App\Tests\Repositories\Mocks;


class AnalysisRepositoryTestDataModel
{
    /**
     * @var int
     */
    private $form_id;

    /**
     * @var string
     */
    private $state;

    /**
     * @var int
     */
    private $number_of_responses;

    /**
     * @return int
     */
    public function getFormId(): int
    {
        return $this->form_id;
    }

    /**
     * @param int $form_id
     */
    public function setFormId(int $form_id): void
    {
        $this->form_id = $form_id;
    }


    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState(string $state): void
    {
        $this->state = $state;
    }

    /**
     * @return int
     */
    public function getNumberOfResponses(): int
    {
        return $this->number_of_responses;
    }

    /**
     * @param int $numberOfResponses
     */
    public function setNumberOfResponses(int $numberOfResponses): void
    {
        $this->number_of_responses = $numberOfResponses;
    }
}