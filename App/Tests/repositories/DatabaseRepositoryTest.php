<?php

namespace App\Tests\Repositories\Base;

use App\Main\Exceptions\ServiceNotFoundException;
use App\Main\Libraries\Log;
use App\Main\Libraries\ServiceLocator;
use App\Main\Models\Role;
use App\Main\Models\User;
use App\Main\Models\Corper;
use App\Main\Repositories\Base\DatabaseRepository;
use App\Main\Repositories\Defaults\DefaultFormRepository;
use App\Main\Repositories\Defaults\DefaultRoleRepository;
use App\Main\Repositories\Defaults\DefaultUserRepository;
use App\Main\Repositories\Defaults\DefaultCorperRepository;
use App\Main\Repositories\FormRepository;
use App\Main\Repositories\RoleRepository;
use App\Main\Repositories\UserRepository;
use App\Main\Repositories\CorperRepository;
use PHPUnit\DbUnit\DataSet\IDataSet;
use PHPUnit\Framework\Assert;

class DatabaseRepositoryTest extends RepositoryTestCase
{

    /**
     * @var DatabaseRepository
     */
    private $userRepository;

    /**
     * @var DatabaseRepository
     */
    private $roleRepository;

    private $corperRepository;

    /**
     * @throws ServiceNotFoundException
     */
    protected function setUp(): void
    {
        ServiceLocator::registerImplementations([
            UserRepository::class => DefaultUserRepository::class,
            FormRepository::class => DefaultFormRepository::class,
            RoleRepository::class => DefaultRoleRepository::class,
            CorperRepository::class => DefaultCorperRepository::class
        ]);

        $this->userRepository = ServiceLocator::get(UserRepository::class);
        $this->roleRepository = ServiceLocator::get(RoleRepository::class);
        $this->corperRepository = ServiceLocator::get(CorperRepository::class);
    }

    protected function tearDown(): void
    {
        ServiceLocator::clearAll();
    }
//
//    /**
//     * Returns the test dataset.
//     *
//     * @return IDataSet
//     */
//    protected function getDataSet()
//    {
//        return $this->createFlatXmlDataSet(dirname(__FILE__) . '/files/database-repository-test-dataset.xml');
//    }

    public function testDeleteWorks()
    {
        $users = $this->userRepository->findAll();
        foreach ($users as $user) {
            \assert($user instanceof User);
            $this->userRepository->delete($user);
        }

        $roles = $this->roleRepository->findAll();
        foreach ($roles as $role) {
            \assert($role instanceof Role);
            $this->roleRepository->delete($role);
        }

        Assert::assertEmpty($this->userRepository->findAll());
        Assert::assertEmpty($this->roleRepository->findAll());
    }

    /**
     * @depends testDeleteWorks
     * @return Role
     */
    public function testWillSaveNewRoleToDB()
    {
        $role = new Role();
        $role->setDescription("A mock role for testing.");
        $role->setRole("Mock Role");
        $this->roleRepository->save($role);

        Assert::assertNotNull($this->roleRepository->findById($role->getId()));
        return $role;
    }

    public function testWillSaveMultipleCorpers()
    {
        $corper1 = new Corper();
        $corper1->setCallUpNumber("NYSC/13");
        $corper1->setDateOfBirth(233);
        $corper1->setFirstName("sdafds");
        $corper1->setInstitutionName("sdfsdfh");
        $corper1->setLastName("sfohioh");
        $corper1->setMiddleName("shoihoh");
        $corper1->setPlatoonId(3);
        $corper1->setQualification("ohioho");
        $corper1->setStateCode("ohihi");
        $corper1->setId(23);
        $corpers = array($corper1);
        $this->corperRepository->saveMultiple($corpers);

        Assert::assertNotNull($this->corperRepository->findById($corper1->getId()));
        return $corpers;
    }

    /**
     * @depends testWillSaveNewRoleToDB
     * @param Role $role
     * @return User
     */
    public function testCanSaveObjectWithForeignKey(Role $role)
    {
        $user = new User();
        $user->setPassword("@12a2a2a");
        $user->setUsername("mockBoss");
        $user->setRoleId($role->getId());

        $this->userRepository->save($user);
        Assert::assertNotNull($this->userRepository->findById($user->getId()));

        return $user;
    }

    /**
     * @depends testCanSaveObjectWithForeignKey
     * @param User $user
     */
    public function testWillUpdateModelWithSameID(User $user)
    {
        Log::info("Old User: " . $user);
        $oldId = $user->getId();
        $oldPassword = $user->getPassword();

        $user->setPassword("ChangedPasswordSameUser");
        $this->userRepository->save($user);
        Log::info("User after Save: " . $user);

        $dbUser = $this->userRepository->findById($user->getId());
        Log::info("DB User: " . $user);

        \assert($dbUser instanceof User);
        Assert::assertTrue($dbUser->getId() == $oldId);
        Assert::assertFalse($dbUser->getPassword() == $oldPassword);
    }
}
