<?php

namespace App\Tests\Repositories;

use App\Main\Features\DataAnalysis\Aggregators\CountAggregator;
use App\Main\Features\DataAnalysis\Constants\CommonCountAggregators;
use App\Main\Features\DataAnalysis\Constants\CommonFilters;
use App\Main\Features\DataAnalysis\Constants\CommonGroupers;
use App\Main\Libraries\Log;
use App\Main\Libraries\ServiceLocator;
use App\Main\Repositories\AnalysisRepository;
use App\Main\Repositories\Defaults\DefaultAnalysisRepository;
use App\Tests\Repositories\Base\RepositoryTestCase;
use App\Tests\Repositories\Mocks\AnalysisRepositoryTestDataModel;
//use PHPUnit\DbUnit\DataSet\IDataSet;
use PHPUnit\Framework\Assert;

/**
 * PhpUnit DbUnit is Abandoned
 *
 * Class AnalysisRepositoryTest
 * @package App\Tests\Repositories
 */
class AnalysisRepositoryTest //extends RepositoryTestCase
{
    /**
     * @var AnalysisRepository
     */
    private $analysisRepository;

    /**
     * @throws \App\Main\Exceptions\ServiceNotFoundException
     */
    protected function setUp(): void
    {
        parent::setUp();
        ServiceLocator::registerImplementations([
            AnalysisRepository::class => DefaultAnalysisRepository::class
        ]);

        $this->analysisRepository = ServiceLocator::get(AnalysisRepository::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        ServiceLocator::clearAll();
    }

    /**
     * Returns the test dataset.
     *
     * @return IDataSet
     */
    protected function getDataSet()
    {
        return $this->createXmlDataSet(dirname(__FILE__) . '/files/analysis-repository-test-dataset.xml');
    }

    public function testGetAggregatedDataWillReturnCorrectData()
    {
        $aggregator = CommonCountAggregators::responsesForFormByUserState(2);
        $data = $this->analysisRepository->getAggregatedData($aggregator);

        Log::info("Returned ". count($data) . " items");
        Log::info(array_keys($data));
        Log::info(array_values($data));

        Assert::assertTrue(count($data) == 2);

        $data0 = $data[0];
        $data1 = $data[1];

        Assert::assertTrue($data0['state'] == 'Delta');
        Assert::assertTrue($data0['number_of_responses'] == 8);
        Assert::assertTrue($data1['state'] == 'Edo');
        Assert::assertTrue($data1['number_of_responses'] == 8);
    }

    public function testGetSqlAggregatedDataWillReturnCorrectData()
    {
        $sql = $this->getTestSqlString();
        $data = $this->analysisRepository->getSqlAggregatedData($sql);

        Log::info("Returned ". count($data) . " items");
        Log::info(array_keys($data));
        Log::info(array_values($data));

        Assert::assertTrue(count($data) == 2);

        $data0 = $data[0];
        $data1 = $data[1];

        Assert::assertTrue($data0['state'] == 'Delta');
        Assert::assertTrue($data0['number_of_responses'] == 8);
        Assert::assertTrue($data1['state'] == 'Edo');
        Assert::assertTrue($data1['number_of_responses'] == 8);
    }

    public function testGetAggregatedDataWillReturnCorrectDataForCustomModel()
    {
        $aggregator = CommonCountAggregators::responsesForFormByUserState(2);
        $data = $this->analysisRepository->getAggregatedData($aggregator, AnalysisRepositoryTestDataModel::class);

        Log::info("Returned ". count($data) . " items");
        Log::info(array_keys($data));
        Log::info(array_values($data));

        Assert::assertTrue(count($data) == 2);

        $data0 = $data[0];
        $data1 = $data[1];

        Assert::assertTrue($data0 instanceof AnalysisRepositoryTestDataModel);
        Assert::assertTrue($data1 instanceof AnalysisRepositoryTestDataModel);
        assert($data0 instanceof AnalysisRepositoryTestDataModel);
        assert($data1 instanceof AnalysisRepositoryTestDataModel);
        Assert::assertTrue($data0->getState() == 'Delta');
        Assert::assertTrue($data0->getNumberOfResponses() == 8);
        Assert::assertTrue($data1->getState() == 'Edo');
        Assert::assertTrue($data1->getNumberOfResponses() == 8);
    }

    public function testGetSqlAggregatedDataWillReturnCorrectDataForCustomModel()
    {
        $sql = $this->getTestSqlString();
        $data = $this->analysisRepository->getSqlAggregatedData($sql, AnalysisRepositoryTestDataModel::class);

        Log::info("Returned ". count($data) . " items");
        Log::info(array_keys($data));
        Log::info(array_values($data));

        Assert::assertTrue(count($data) == 2);

        $data0 = $data[0];
        $data1 = $data[1];

        Assert::assertTrue($data0 instanceof AnalysisRepositoryTestDataModel);
        Assert::assertTrue($data1 instanceof AnalysisRepositoryTestDataModel);
        assert($data0 instanceof AnalysisRepositoryTestDataModel);
        assert($data1 instanceof AnalysisRepositoryTestDataModel);
        Assert::assertTrue($data0->getState() == 'Delta');
        Assert::assertTrue($data0->getNumberOfResponses() == 8);
        Assert::assertTrue($data1->getState() == 'Edo');
        Assert::assertTrue($data1->getNumberOfResponses() == 8);
    }

    public function testGetAggregatedDataWillReturnCorrectDataWithIncludedFilterSelection()
    {
        $grouper = CommonGroupers::questionResponseByUserState();
        $filters = CommonFilters::questionResponsesForForm(2, true, 'form_id');
        $aggregator = new CountAggregator("question_responses", $grouper, $filters, "number_of_responses");

        $data = $this->analysisRepository->getAggregatedData($aggregator, AnalysisRepositoryTestDataModel::class);

        Log::info("Returned ". count($data) . " items");
        Log::info(array_keys($data));
        Log::info(array_values($data));

        Assert::assertTrue(count($data) == 2);

        $data0 = $data[0];
        $data1 = $data[1];

        Assert::assertTrue($data0 instanceof AnalysisRepositoryTestDataModel);
        Assert::assertTrue($data1 instanceof AnalysisRepositoryTestDataModel);
        assert($data0 instanceof AnalysisRepositoryTestDataModel);
        assert($data1 instanceof AnalysisRepositoryTestDataModel);
        Assert::assertTrue($data0->getFormId() == 2);
        Assert::assertTrue($data0->getState() == 'Delta');
        Assert::assertTrue($data0->getNumberOfResponses() == 8);
        Assert::assertTrue($data1->getFormId() == 2);
        Assert::assertTrue($data1->getState() == 'Edo');
        Assert::assertTrue($data1->getNumberOfResponses() == 8);
    }

    private function getTestSqlString()
    {
        return "
          SELECT 
            users.state, COUNT(*) AS number_of_responses 
          FROM 
            question_responses 
          JOIN 
            users ON question_responses.user_id=users.id 
          JOIN 
            questions ON question_responses.question_id=questions.id 
          JOIN 
            sections ON questions.section_id=sections.id 
          WHERE 
            sections.form_id=2 
          GROUP BY users.state";
    }
}
