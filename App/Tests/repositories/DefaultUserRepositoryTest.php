<?php

namespace App\Tests\Repositories\Base;

use App\Main\Exceptions\ServiceNotFoundException;
use App\Main\Libraries\Log;
use App\Main\Libraries\ServiceLocator;
use App\Main\Models\User;
use App\Main\Repositories\Defaults\DefaultRoleRepository;
use App\Main\Repositories\Defaults\DefaultUserRepository;
use App\Main\Repositories\RoleRepository;
use App\Main\Repositories\UserRepository;
//use PHPUnit\DbUnit\DataSet\IDataSet;
use PHPUnit\Framework\Assert;


/**
 * Class DefaultUserRepositoryTest
 * @covers DefaultUserRepository
 */
class DefaultUserRepositoryTest extends RepositoryTestCase
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @throws ServiceNotFoundException
     */
    protected function setUp(): void
    {
        ServiceLocator::registerImplementations([
            UserRepository::class => DefaultUserRepository::class,
            RoleRepository::class => DefaultRoleRepository::class
        ]);

        $this->userRepository = ServiceLocator::get(UserRepository::class);
    }

    protected function tearDown(): void
    {
        ServiceLocator::clearAll();
    }
//
//    /**
//     * Returns the test dataset.
//     *
//     * @return IDataSet
//     */
//    protected function getDataSet()
//    {
//        return $this->createFlatXmlDataSet(dirname(__FILE__) . '/files/user-repository-test-dataset.xml');
//    }

    /**
     * @throws ServiceNotFoundException
     */
    public function testCanSaveUser()
    {
        $this->deleteAll();

        $user = new User();
        $roleRepository = ServiceLocator::get(RoleRepository::class);
        assert($roleRepository instanceof RoleRepository);
        $role = $roleRepository->findAll()[0];

        $user->setUsername('mockUser');
        $user->setPassword('P@ssw0rd');
        $user->setRoleId($role->getId());
        //default
        $user->setState("Lagos");
        $this->userRepository->save($user);

        Assert::assertTrue(true);
        return $user;
    }

    /**
     * @param User $expectedUser
     * @depends testCanSaveUser
     */
    public function testFindByUserNameWillReturnCorrectUser(User $expectedUser)
    {
        $actualUser = $this->userRepository->findByUsername('mockUser');
        Log::info($expectedUser);
        Log::info($actualUser);
        Assert::assertNotNull($actualUser);
        Assert::assertTrue($expectedUser->getId() == $actualUser->getId());
    }

    /**
     * @param User $expectedUser
     * @depends testCanSaveUser
     */
    public function testFindByUsernameAndPasswordWillReturnCorrectUser(User $expectedUser)
    {
        $password = 'P@ssw0rd';
        Log::info($expectedUser);

        $actualUser = $this->userRepository->findByUsername('mockUser');
        Log::info($actualUser);

        Assert::assertNotNull($actualUser);
        Assert::assertTrue($expectedUser->getId() == $actualUser->getId());
        Assert::assertTrue(password_verify($password, $actualUser->getPassword()));
    }

    /**
     * @depends testCanSaveUser
     * @param User $user
     */
    public function testFindByRoleIdWillReturnCorrectUsers(User $user)
    {
        $users = $this->userRepository->findByRoleId($user->getRoleId());
        Log::info($users);
        Assert::assertTrue(count($users) == 1);
    }

    private function deleteAll()
    {
        foreach ($this->userRepository->findAll() as $user) {
            $this->userRepository->delete($user);
        }
    }
}
