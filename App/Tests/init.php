<?php /** @noinspection PhpIncludeInspection */

namespace App\Tests;

use App\Main\Libraries\Log;

require_once "config/config.php";
require 'vendor/autoload.php';


require_once APP_ROOT . '/main/helpers/utils.php';
require_once APP_ROOT . '/main/helpers/url_helper.php';


Log::info("Bootstrapped Tests.");

//Start buffering output.
ob_start();
