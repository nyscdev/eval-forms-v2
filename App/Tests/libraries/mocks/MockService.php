<?php

namespace App\Tests\Libraries\Mocks;

use App\Main\Libraries\Log;
use App\Main\Repositories\FormRepository;
use App\Main\Repositories\UserRepository;

class MockService
{
    private $userRepository;
    private $formRepository;

    public function __construct(UserRepository $userRepository, FormRepository $formRepository)
    {
        $this->userRepository = $userRepository;
        $this->formRepository = $formRepository;

        Log::info("Hi from MockService.");
    }
}
