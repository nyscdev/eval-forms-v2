<?php

namespace App\Tests\Libraries\Mocks;

use App\Main\Exceptions\ServiceNotFoundException;
use App\Main\Libraries\Log;
use App\Main\Libraries\ServiceLocator;
use App\Main\Repositories\Defaults\DefaultFormRepository;
use App\Main\Repositories\Defaults\DefaultRoleRepository;
use App\Main\Repositories\Defaults\DefaultUserRepository;
use App\Main\Repositories\FormRepository;
use App\Main\Repositories\RoleRepository;
use App\Main\Repositories\UserRepository;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class ServiceLocatorTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();
        ServiceLocator::registerImplementations([
            UserRepository::class => DefaultUserRepository::class,
            FormRepository::class => DefaultFormRepository::class,
            RoleRepository::class => DefaultRoleRepository::class,
        ]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        ServiceLocator::clearAll();
    }

    /**
     * @throws ServiceNotFoundException
     */
    public function testServiceLocatorReturnsInstanceOfClass()
    {
        $mockServiceInstance = ServiceLocator::get(MockService::class);
        Log::info($mockServiceInstance);
        Assert::assertTrue(isset($mockServiceInstance));
        Assert::assertTrue($mockServiceInstance instanceof MockService);
    }

    /**
     * @throws ServiceNotFoundException
     */
    public function testServiceLocatorReturnsSameServiceTwice()
    {
        $mockServiceInstance = ServiceLocator::get(MockService::class);
        Log::info("\n\n\n\n\nGetting instance second time...\n\n\n\n");

        $mockServiceInstance2 = ServiceLocator::get(MockService::class);
        Assert::assertTrue($mockServiceInstance === $mockServiceInstance2);
    }

    /**
     * @throws ServiceNotFoundException
     */
    public function testWillThrowExceptionWhenServiceDoesNotExist()
    {
        $this->expectException(ServiceNotFoundException::class);
        ServiceLocator::get("UndefinedService");
    }
}
