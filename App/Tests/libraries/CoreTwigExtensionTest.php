<?php

namespace App\Tests\Libraries;

use App\Main\Exceptions\RenderException;
use App\Main\Exceptions\ServiceNotFoundException;
use App\Main\Http\Routing\Router;
use App\Main\Libraries\Log;
use App\Main\Libraries\Rendering\Renderer;
use App\Main\Libraries\Rendering\Twig\TwigRenderer;
use App\Main\Libraries\ServiceLocator;
use App\Tests\Http\Mocks\MockController;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;
use Twig_Loader_Array;
use Twig_LoaderInterface;

/**
 * Created by Augustine Eloka.
 * Date: 25/09/2018
 * Time: 03:58 PM
 */
class CoreTwigExtensionTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        ServiceLocator::clearAll();
    }

    /**
     * @throws RenderException
     * @throws ServiceNotFoundException
     */
    function testGetURLForMethodExtensionFunctionWorks()
    {
        //given
        $data = [
            'controller' => 'MockController',
            'method' => 'mockMethod'
        ];

        $templates = [
            'index.html' => "{{ url_for(data.controller, data.method) }}",
        ];

        //when
        Router::register('/super_mocker/hi/')->withController(MockController::class)->withHandler('mockMethod');
        ServiceLocator::registerInstances([Twig_LoaderInterface::class => new Twig_Loader_Array($templates)]);
        $renderer = ServiceLocator::get(TwigRenderer::class);
        assert($renderer instanceof Renderer);

        $renderedTemplate = $renderer->render('index.html', $data);

        Log::info("Actual: " . $renderedTemplate);
        Log::info("Expected: " . URL_ROOT . '/super_mocker/hi');

        //then
        Assert::assertTrue($renderedTemplate === URL_ROOT . '/super_mocker/hi');
    }
}