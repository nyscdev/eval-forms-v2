<?php

namespace App\Tests\Libraries\Mocks;

use App\Main\Exceptions\Handlers\ExceptionHandler;
use App\Main\Exceptions\NotFoundException;
use Exception;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class ExceptionHandlerTest extends TestCase
{
    public function testWillResolveExceptionType()
    {
        try {
            ExceptionHandler::handleException(new NotFoundException());
        } catch (\RuntimeException $e) {
        }
        $logs = ob_get_contents();
        Assert::assertTrue(strpos($logs, "Handling NotFoundException") !== false);
    }

    public function testStackOverflowDoesNotOccurWhenExceptionPassedIsOfExceptionType()
    {
        try {
            ExceptionHandler::handleException(new Exception());
        } catch (\RuntimeException $e) {
        }
        Assert::assertTrue(true);
    }
}
