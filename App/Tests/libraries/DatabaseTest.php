<?php

namespace App\Tests\Libraries;


use App\Main\Libraries\Database;
use App\Main\Libraries\Log;
use App\Main\Libraries\ServiceLocator;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class DatabaseTest extends TestCase
{
    /**
     * @var Database
     */
    private $database;

    /**
     * @throws \App\Main\Exceptions\ServiceNotFoundException
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->database = ServiceLocator::get(Database::class);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        ServiceLocator::clearAll();
    }

    public function testCanExportDatabase()
    {
        $file = APP_ROOT . "db.sql";
        $exported = $this->database->exportDatabaseTables($file);
        unlink($file);
        Log::info($exported);
        Assert::assertTrue($exported);
    }
}