<?php
/**
 * Description
 *
 * @package App\Main\Features\CorperManagement
 * @since 1.0.0
 * @author Kofi Oghenerukevwe Henrietta
 * @link http://moreinformation.com
 * @license GPL-2.0+
 *
 */

namespace App\Tests\Main\Features\CorperManagement;

use App\Main\Libraries\ServiceLocator;
use App\Main\Repositories\CorperRepository;
use App\Main\Repositories\Defaults\DefaultCorperRepository;
use App\Main\Repositories\Defaults\DefaultRoleRepository;
use App\Main\Repositories\Defaults\DefaultUserRepository;
use App\Main\Repositories\RoleRepository;
use App\Main\Repositories\UserRepository;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;
use App\Main\Features\CorperManagement\CorperService;
use App\Main\Models;

class CorperServiceTest extends TestCase
{

    private $userRepositoryMock = null;

    private $corperRepositoryMock = null;

    private $roleServiceMock = null;

    private $sheetPaths = null;

    private $currentPath = null;

    public function setUp(): void
    {
        parent::setUp();
        $this->userRepositoryMock = \Mockery::mock('App\Main\Repositories\Defaults\DefaultUserRepository');
        $this->corperRepositoryMock = \Mockery::mock('App\Main\Repositories\Defaults\DefaultCorperRepository');
        $this->roleServiceMock = \Mockery::mock('\App\Main\Features\RoleManagement\RoleService');
        $this->currentPath = \dirname(__FILE__);
        $this->sheetPaths = join(DIRECTORY_SEPARATOR, [$this->currentPath, 'test_sheets']);

        $corperRole = new Models\Role();
        $corperRole->setId(1);
        $corperRole->setRole('Corper');
        $this->roleServiceMock->shouldReceive('getRoleWithName')->andReturn($corperRole);
        $this->userRepositoryMock->shouldReceive('save')->andReturn(new Models\User());
        $this->userRepositoryMock->shouldReceive('findByUsername')->andReturn(null);
        $this->corperRepositoryMock->shouldReceive('saveMultiple')->andReturn(true);

    }


    public function testProcessCorperSpreadSheet_Succeeds_With_ValidSpreadsheet()
    {

        $corperService = new CorperService($this->userRepositoryMock, $this->corperRepositoryMock,
            $this->roleServiceMock);

        $validCorperSpreadsheetPath = join(DIRECTORY_SEPARATOR, [$this->sheetPaths, 'valid_corper_spreadsheet.xlsx']);
        $newFilePathForTesting = join(DIRECTORY_SEPARATOR, [$this->currentPath, 'spreadsheet.xlsx']);
        copy($validCorperSpreadsheetPath, $newFilePathForTesting);
        $message = $corperService->processCorperSpreadSheet($newFilePathForTesting);

        Assert::assertNull($message);

    }

    public function testProcessCorperSpreadSheet_Succeeds_With_ValidSpreadsheetContainingExistingData(){

        ServiceLocator::registerImplementations([
            UserRepository::class => DefaultUserRepository::class,
            CorperRepository::class => DefaultCorperRepository::class,
            RoleRepository::class => DefaultRoleRepository::class
        ]);

        $corperService = ServiceLocator::get(CorperService::class);

        $validCorperSpreadsheetPath = join(DIRECTORY_SEPARATOR, [$this->sheetPaths, 'valid_corper_spreadsheet.xlsx']);
        $newFilePathForTesting = join(DIRECTORY_SEPARATOR, [$this->currentPath, 'spreadsheet.xlsx']);
        copy($validCorperSpreadsheetPath, $newFilePathForTesting);
        $message = $corperService->processCorperSpreadSheet($newFilePathForTesting);
        Assert::assertNull($message);

        //Attempt to upload the exact same spreadsheet again
        copy($validCorperSpreadsheetPath, $newFilePathForTesting);
        $message = $corperService->processCorperSpreadSheet($newFilePathForTesting);
        Assert::assertNull($message);

        //TODO: Remove recently added corpers from the test database
    }

    public function testProcessCorperSpreadSheet_Fails_With_InvalidSpreadSheetDataWithRequiredColumns()
    {
        $corperService = new CorperService($this->userRepositoryMock, $this->corperRepositoryMock,
            $this->roleServiceMock);

        $validCorperSpreadsheetPath = join(DIRECTORY_SEPARATOR,
            [$this->sheetPaths, 'invalid_corper_spreadsheet_with_all_required_columns.xlsx']);

        $newFilePathForTesting = join(DIRECTORY_SEPARATOR, [$this->currentPath, 'spreadsheet.xlsx']);
        copy($validCorperSpreadsheetPath, $newFilePathForTesting);
        $message = $corperService->processCorperSpreadSheet($newFilePathForTesting);


        Assert::assertNotNull($message);
        Assert::assertStringContainsString("1.) Error! Please ensure you remove headings before upload" .
            "<br/><br/>2.) Error! Check to confirm that your spreadsheet has ONLY these columns in the EXACT order they are listed: <br/>" .
            "State Code, CallUp Number, Full Name, Gender, Institution, Program of Study, " .
            "Qualification (e.g B.ENG), Date of Birth and Platoon. <br/><br/>See the images below for guidance", $message);
    }

    public function testProcessCorperSpreadSheet_Fails_With_InvalidSpreadSheetDataWithMissingColumns()
    {
        \PHPUnit\Framework\Error\Warning::$enabled = false;

        $corperService = new CorperService($this->userRepositoryMock, $this->corperRepositoryMock,
            $this->roleServiceMock);

        $validCorperSpreadsheetPath = join(DIRECTORY_SEPARATOR,
            [$this->sheetPaths, 'invalid_corper_spreadsheet_with_missing_required_columns.xlsx']);

        $newFilePathForTesting = join(DIRECTORY_SEPARATOR, [$this->currentPath, 'spreadsheet.xlsx']);
        copy($validCorperSpreadsheetPath, $newFilePathForTesting);

        $message = $corperService->processCorperSpreadSheet($newFilePathForTesting);


        Assert::assertNotNull($message);
        Assert::assertStringContainsString("1.) Error! Please ensure you remove headings before upload" .
            "<br/><br/>2.) Error! Check to confirm that your spreadsheet has ONLY these columns in the EXACT order they are listed: <br/>" .
            "State Code, CallUp Number, Full Name, Gender, Institution, Program of Study, " .
            "Qualification (e.g B.ENG), Date of Birth and Platoon. <br/><br/>See the images below for guidance", $message);

    }

    public function testProcessCorperSpreadSheet_Fails_With_InvalidSpreadSheetDataWithHeadings()
    {
        $corperService = new CorperService($this->userRepositoryMock, $this->corperRepositoryMock,
            $this->roleServiceMock);

        $validCorperSpreadsheetPath = join(DIRECTORY_SEPARATOR,
            [$this->sheetPaths, 'invalid_corper_spreadsheet_with_headings_and_required_columns.xlsx']);

        $newFilePathForTesting = join(DIRECTORY_SEPARATOR, [$this->currentPath, 'spreadsheet.xlsx']);
        copy($validCorperSpreadsheetPath, $newFilePathForTesting);
        $message = $corperService->processCorperSpreadSheet($newFilePathForTesting);


        Assert::assertNotNull($message);
        Assert::assertStringContainsString("1.) Error! Please ensure you remove headings before upload" .
            "<br/><br/>2.) Error! Check to confirm that your spreadsheet has ONLY these columns in the EXACT order they are listed: <br/>" .
            "State Code, CallUp Number, Full Name, Gender, Institution, Program of Study, " .
            "Qualification (e.g B.ENG), Date of Birth and Platoon. <br/><br/>See the images below for guidance", $message);
    }
}
