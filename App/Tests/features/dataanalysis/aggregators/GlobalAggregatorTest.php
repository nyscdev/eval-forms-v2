<?php

namespace App\Tests\Features\DataAnalysis\Aggregators;


use App\Main\Features\DataAnalysis\Aggregators\GlobalAggregator;
use App\Main\Features\DataAnalysis\Models\Connection;
use App\Main\features\DataAnalysis\Models\Filter;
use App\Main\Libraries\Log;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class GlobalAggregatorTest extends TestCase
{

    public function testBuildQueryWillReturnCorrectQuery()
    {
        $connections = [
            new Connection('questions', 'section_id', 'sections', 'id')
        ];

        $filters = [
            new Filter('sections', 'form_id', 2, $connections)
        ];

        $aggregator = new GlobalAggregator('questions', $filters);
        $query = $aggregator->getQuery();
        Log::info($query);
        Assert::assertEquals('SELECT * FROM questions JOIN sections ON questions.section_id=sections.id WHERE sections.form_id = 2', $query);
    }
}