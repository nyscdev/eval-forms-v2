<?php

namespace App\Tests\Features\DataAnalysis\Aggregators;


use App\Main\Features\DataAnalysis\Aggregators\CountAggregator;
use App\Main\Features\DataAnalysis\Constants\CommonCountAggregators;
use App\Main\Features\DataAnalysis\Constants\CommonConnections;
use App\Main\Features\DataAnalysis\Constants\CommonGroupers;
use App\Main\features\DataAnalysis\Models\Filter;
use App\Main\Libraries\Log;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class CountAggregatorTest extends TestCase
{
    public function testGetQueryWillReturnCorrectQueryWithAllValuesPresent()
    {
        $aggregator = CommonCountAggregators::responsesForFormByUserState(2);
        $expectedResponse = "SELECT users.state, COUNT(*) AS number_of_responses FROM question_responses JOIN users ON question_responses.user_id=users.id JOIN questions ON question_responses.question_id=questions.id JOIN sections ON questions.section_id=sections.id WHERE sections.form_id = 2 GROUP BY users.state";

        $query = $aggregator->getQuery();

        Log::info($query);
        Assert::assertEquals($expectedResponse, $query);
    }

    public function testGetQueryWillReturnCorrectQueryWithoutAnyFiltersPresent()
    {
        $grouper = CommonGroupers::questionResponseByUserState();
        $aggregator = new CountAggregator("question_responses", $grouper, $filters = [], "number_of_responses");
        $expectedResponse = "SELECT users.state, COUNT(*) AS number_of_responses FROM question_responses JOIN users ON question_responses.user_id=users.id GROUP BY users.state";

        $query = $aggregator->getQuery();

        Log::info($query);
        Assert::assertEquals($expectedResponse, $query);
    }

    public function testWillNotJoinSameSourceMoreThanOnce()
    {
        $grouper = CommonGroupers::questionResponseByUserState();
        $filters = [new Filter('users', 'id', 2, CommonConnections::questionResponseToUser())];
        $aggregator = new CountAggregator("question_responses", $grouper, $filters, "number_of_responses");
        $expectedResponse = "SELECT users.state, COUNT(*) AS number_of_responses FROM question_responses JOIN users ON question_responses.user_id=users.id WHERE users.id = 2 GROUP BY users.state";

        $query = $aggregator->getQuery();

        Log::info($query);
        Assert::assertEquals($expectedResponse, $query);
    }

    public function testAggregatorWithMultipleFiltersWillReturnCorrectQueryWithUseParametersSetToTrue() {
        $filters = [
            new Filter('corpers', 'institution_name', "University of Lagos", CommonConnections::questionResponseToCorper(), true),
            new Filter('corpers', 'qualification', "BSc.", [], true),
            new Filter('users', 'state', "Lagos", CommonConnections::questionResponseToUser(), true),
            new Filter('questions', 'id', 9, CommonConnections::questionResponseToQuestion(), true)
        ];

        $aggregator = new CountAggregator('question_responses', null, $filters, 'number_of_responses');
        $query = $aggregator->getQuery(true);
        $expected =
            "SELECT ANY_VALUE(corpers.institution_name) AS institution_name, ANY_VALUE(corpers.qualification) AS qualification, ANY_VALUE(users.state) AS state, ANY_VALUE(questions.id) AS id, COUNT(*) AS number_of_responses FROM question_responses JOIN corpers ON question_responses.user_id=corpers.user_id JOIN users ON question_responses.user_id=users.id JOIN questions ON question_responses.question_id=questions.id WHERE corpers.institution_name = :corpers_institution_name AND corpers.qualification = :corpers_qualification AND users.state = :users_state AND questions.id = :questions_id";
        Log::info($query);
        Assert::assertEquals($expected, $query);
    }
}