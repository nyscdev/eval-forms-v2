<?php

namespace App\Tests\Features\DataAnalysis\Aggregators;


use App\Main\features\DataAnalysis\Aggregators\DistinctAggregator;
use App\Main\Libraries\Log;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class DistinctAggregatorTest extends TestCase
{
    public function testGetQueryWillReturnCorrectQuery()
    {
        $aggregator = new DistinctAggregator("users", "state");
        $query = $aggregator->getQuery();

        $expectedResponse = "SELECT DISTINCT(state) FROM users";

        Log::info($query);
        Assert::assertEquals($expectedResponse, $query);
    }
}