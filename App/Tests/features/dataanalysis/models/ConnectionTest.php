<?php

namespace App\Tests\Features\DataAnalysis\Models;


use App\Main\Features\DataAnalysis\Models\Connection;
use App\Main\Libraries\Log;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class ConnectionTest extends TestCase
{
    public function testGetQueryWillReturnCorrectQuery()
    {
        $connection = new Connection("question_responses", "user_id", "corpers", "user_id");
        $query = $connection->getQuery();
        Log::info($query);
        Assert::assertEquals("JOIN corpers ON question_responses.user_id=corpers.user_id", $query);
    }

    public function testToStringWillReturnQuery()
    {
        $connection = new Connection("question_responses", "user_id", "corpers", "user_id");
        $query = $connection->__toString();
        Log::info($query);
        Log::info($connection);
        Assert::assertEquals($connection->getQuery(), $query);
    }
}