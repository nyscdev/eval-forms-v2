<?php

namespace App\Tests\Features\DataManagement\Mocks;


use App\Main\Controllers\Base\Controller;
use App\Main\Features\DataManagement\CloudServiceProviders\DropBox\DropBoxApiController;
use App\Main\Libraries\Log;

class MockDropBoxAuthenticationApiController extends Controller
{
    public function index()
    {
        Log::info("Hit Mock Dropbox API");
        $file = fopen('mockFile.sql', 'w');
        fclose($file);
        $this->redirectTo(DropBoxApiController::class, 'dropBoxApiCallback', ["code" => "mockCode", "state" => "upload/mockFile.sql"]);
    }
}