<?php

namespace App\Tests\Features\DataManagement\Mocks;


use App\Main\Features\DataManagement\CloudServiceProviders\CloudServiceProvider;

class MockCloudServiceProvider implements CloudServiceProvider
{
    public function upload(string $filename, string $successUrl = null, bool $deleteFileOnCompletion = true)
    {
        return true;
    }

    public function download(string $filename, string $downloadDestination)
    {
        return true;
    }
}