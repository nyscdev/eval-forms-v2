<?php

namespace App\Tests\Features\DataManagement\Mocks;


use App\Main\Features\DataManagement\CloudServiceProviders\DropBox\Clients\DropBoxClient;

class MockDropBoxClient implements DropBoxClient
{
    private $accessToken = null;

    /**
     * MockDropBoxWrapper constructor.
     * @param null $accessToken
     */
    public function __construct($accessToken)
    {
        $this->accessToken = $accessToken;
    }


    public function upload($filename, $file)
    {
        return true;
    }

    public function getAuthUrl($callback, $params = [], $state = null)
    {
        return 'http://localhost/eval-forms-v2/test/dropbox';
    }

    /**
     * @param $code
     * @param null $state
     * @param null $redirectUri
     * @return string
     */
    public function getAccessToken($code = null, $state = null, $redirectUri = null)
    {
        return $this->accessToken;
    }
}