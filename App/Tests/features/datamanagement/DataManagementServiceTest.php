<?php

namespace App\Tests\Features\DataManagement;


use App\Main\Features\DataManagement\CloudServiceProviders\CloudServiceProvider;
use App\Main\Features\DataManagement\DataManagementService;
use App\Main\Libraries\ServiceLocator;
use App\Tests\Features\DataManagement\Mocks\MockCloudServiceProvider;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class DataManagementServiceTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        ServiceLocator::registerImplementations([
            CloudServiceProvider::class => MockCloudServiceProvider::class
        ]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        ServiceLocator::clearAll();
    }

    /**
     * @throws \App\Main\Exceptions\ServiceNotFoundException
     */
    function testBackupDatabase()
    {
        $service = ServiceLocator::get(DataManagementService::class);
        assert($service instanceof DataManagementService);
        $backedUp = $service->backUpDatabase();
        Assert::assertTrue($backedUp);
    }
}