<?php

namespace App\Tests\Features\DataManagement;


use App\Main\Features\DataManagement\CloudServiceProviders\CloudServiceProvider;
use App\Main\Features\DataManagement\CloudServiceProviders\FileCloudServiceProvider;
use App\Main\Features\DataManagement\DataManagementService;
use App\Main\Libraries\ServiceLocator;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;

class FileCloudServiceProviderTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        ServiceLocator::registerImplementations([
            CloudServiceProvider::class => FileCloudServiceProvider::class
        ]);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        ServiceLocator::clearAll();
    }

    /**
     * @throws \App\Main\Exceptions\ServiceNotFoundException
     */
    function testUpload()
    {
        $dataManagementService = ServiceLocator::get(DataManagementService::class);
        assert($dataManagementService instanceof DataManagementService);
        $isBackedUp = $dataManagementService->backUpDatabase();
        Assert::assertTrue($isBackedUp);
    }
}