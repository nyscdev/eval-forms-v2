<?php

namespace App\Tests\Features\DataManagement;


use App\Main\Features\DataManagement\CloudServiceProviders\CloudServiceProvider;
use App\Main\Features\DataManagement\CloudServiceProviders\DropBox\Clients\DropBoxClient;
use App\Main\Features\DataManagement\CloudServiceProviders\DropBox\DropBoxApiController;
use App\Main\Features\DataManagement\CloudServiceProviders\DropBox\DropBoxCloudServiceProvider;
use App\Main\Features\DataManagement\DataManagementService;
use App\Main\Http\Routing\Router;
use App\Main\Http\Session\MockSessionManager;
use App\Main\Http\Session\SessionManager;
use App\Main\Libraries\Navigation\NavigationBuilder;
use App\Main\Libraries\Navigation\XMLNavigation\XMLNavigationBuilder;
use App\Main\Libraries\Rendering\Renderer;
use App\Main\Libraries\Rendering\Twig\TwigRenderer;
use App\Main\Libraries\ServiceLocator;
use App\Tests\Features\DataManagement\Mocks\MockDropBoxAuthenticationApiController;
use App\Tests\Features\DataManagement\Mocks\MockDropBoxClient;
use PHPUnit\Framework\Assert;
use PHPUnit\Framework\TestCase;
use Twig_Loader_Filesystem;
use Twig_LoaderInterface;

class DropBoxCloudServiceProviderTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        ServiceLocator::registerImplementations([
            CloudServiceProvider::class => DropBoxCloudServiceProvider::class,
            SessionManager::class => MockSessionManager::class,
            Renderer::class => TwigRenderer::class,
            NavigationBuilder::class => XMLNavigationBuilder::class,
        ]);

        ServiceLocator::registerInstances([
            DropBoxClient::class => new MockDropBoxClient(null),
            Twig_LoaderInterface::class => new Twig_Loader_Filesystem(VIEW_ROOT),
        ]);

        Router::registerForController(DropBoxApiController::class, '/api/dropbox/auth/')
            ->addMapping('callback', 'dropBoxApiCallback');

        Router::registerForController(MockDropBoxAuthenticationApiController::class, '/test/dropbox/');

    }

    protected function tearDown(): void
    {
        parent::tearDown();
        ServiceLocator::clearAll();
    }

    /**
     * @throws \App\Main\Exceptions\ServiceNotFoundException
     */
    function testUploadWithNoAccessToken()
    {
        $dataManagementService = ServiceLocator::get(DataManagementService::class);
        assert($dataManagementService instanceof DataManagementService);
        $isBackedUp = $dataManagementService->backUpDatabase();
        Assert::assertTrue($isBackedUp);
    }

    /**
     * @throws \App\Main\Exceptions\ServiceNotFoundException
     */
    function testUploadWithAccessToken()
    {
        ServiceLocator::registerInstances([
            DropBoxClient::class => new MockDropBoxClient('mockAccessToken'),
        ]);

        $dataManagementService = ServiceLocator::get(DataManagementService::class);
        assert($dataManagementService instanceof DataManagementService);
        $isBackedUp = $dataManagementService->backUpDatabase();
        Assert::assertTrue($isBackedUp);
    }
}