# NYSC EVALUATIONS PROGRAMMER ONBOARDING DOC
Welcome! Here is a little bit of documentation to help you get started with building the NYSC evaluation platform. This platform is built with a mini-framework in core PHP that tries it's best to adhere to the MVC pattern
We will be deploying from the master branch. All development work should be done on feature branches and merged with the dev branch. 
## What exactly are we building

To get an overview of how the system we're building works, regularly check these docs (they'll be updated as time goes on and as the needs of the system change).

[Click here to view system wireframes](https://drive.google.com/open?id=1qO2v-yH-8su_e4Li3SN-Se81M0aWbFsa)

[Click here to view database schema](https://drive.google.com/open?id=1lhCjwhAaYtpDHas_Wrbyo_HTpubt5hkG)

## Coding Standards
We use the PSR2 conding standard for our code which you can read up on here: https://www.php-fig.org/psr/psr-2/

For more information about how we work in the team, the coding standards we use etc. [click here to read the welcome note](https://docs.google.com/document/d/1vbI5x10IGONbcZyd_IAj0j4DIxPqsQSrUwxRj4CexEc/edit?usp=sharing)

# Setting Up Your Development Environment
This section is particularly for the benefit of people who are new to PHP development. If you already have a handle of PHP, feel free to set things up however you want. 
This is an opinionated section. This means that some tools and extensions are recommended. E.g In this section Xampp is the recommended local server. You're free to use something else provided you can get it to work. But for simplicity sake, this section will discuss getting the project up and running with xampp.

## Downloads and Installs
- Download and install Xampp (local server). The version of Xampp used at the time of this writing is 7.0.31 (in case you get an updated version and it's not working, try 7.2.9). https://www.apachefriends.org/download.html
- Download and install Git https://git-scm.com/downloads
- Download and install composer https://getcomposer.org/download/

# Running the application
The instructions here are primarily for Windows users. If you're working in Linux, you should find your way around by yourself :) 
1. Navigate to the htdocs folder in Xampp.....if you installed on Windows using the default installation settings, this should typically be at C:\xampp\htdocs.

2. Open up the git bash terminal (right click and click Git Bash here). 

3. Clone the repository. There's a clone button which you can use to get the command for cloning. Cd into the repository after cloning.

4. Switch to the dev branch (or whatever branch you're working on). If you're implementing a feature, you'd need to do your implementations on a branch named after that feature. See the Welcome note for details. But since this is the first time you're working on the project, the dev branch will suffice.

5. Cd into the app directory and run `composer install` so that composer will install the necessary project dependencies from the composer.json file. 

6. Run Xampp (start Apache and MySQL). Go to http://localhost/phpmyadmin/. Depending on how things are on your system, your xampp might be setup on a different localhost port. 

7. In PhpMyAdmin create a database called **nysc_evaluation** . Click on the database you just created, then go to import. Upload the database_setup.sql file that is part of the repository. This should automatically install all the database tables and dummy data into your MySQL server for you. You'd need to repeat this process whenever an update is made to the database by the team. 

8. Ensure that the settings in your app/config/config.php file are like so: 

	    define("DB_USERNAME", "root"); //Enter database username
    
    	define("DB_PASSWORD", ""); // Enter database password
    
    	define("DB_NAME", "nysc_evaluation"); // Enter DB name


10. If you didn't change the name of the folder that Git created while cloning, you should be able to run your application by visiting http://localhost/eval-forms-v2/ . Make sure Apache and MySQL are running in the xampp control panel.  (And we recommend you don't change the name of the folder because changing the name would mean changing some of your configuration settings in the config.php file in app/config).

# Setting up visual studio code for development with extensions for code linting and intellisense.
Again, this is opinionated. You don't have to use visual studio code as your text editor. However, this section will focus on getting you up and running with Visual studio code if that's what you want to use. In this section you will install some tools that will help you automatically enforce the code style we're using in our coding standards (PSR2). 

1. Download Visual studio code https://code.visualstudio.com/

2. During installation, make sure that visual studio code is added to the explorer context menu (if you're working with windows). This will make things easier. 

3. Start VS Code and go to File -> Preferences -> Extensions. 

4. Search for phpcs, select the one by Ioannis Kappas and install. 

5. Search for PHP IntelliSense. Select the one by Felix Becker and install.

6. Search for PHP CS Fixer and install the one by Junstyle. 

7. Go to File -> Preferences -> Settings

8. Select Workspace settings or user settings. User settings will make these settings global for all the PHP work you do in VS Code. Workspace settings should be used if you've already opened the folder for this project in Visual studio code, so that your settings will apply only for this project. 

9. Click on Open settings.json. To know where to find this button, [click here to see an image](https://drive.google.com/file/d/1ti3v2XC17AMgDIB0pmljoHUFIAkljQCr/view?usp=sharing)

10. Add the following settings to the Json file (on the right side of the screen, you won't be able to edit the one on the left). 
[Click here to see an image](https://drive.google.com/file/d/1yJ3U4XN25WbXwXWGViKV74rfvUrO_aYB/view?usp=sharing)
	
    "terminal.integrated.shell.windows": "C:\\Program Files\\Git\\bin\\bash.exe",

	"php.suggest.basic": false,

	"phpcs.standard": "PSR2",
	
	"php-cs-fixer.onsave": true,

	"php.executablePath": "C:\\xampp\\php\\php.exe",

	"files.eol": "\n"

11. Ctrl + S to save the settings. Now your VS Code is setup for development. If while coding you do anything that violates the standard, e.g fail to use camelCasing for method names, you'd get errors in the VS Code output panel. The errors will also be underlined red in the code. It's important to note that linting errors won't cause your code to fail when you run it oh, the red will just be there so you know to fix those issues before trying to push to the repository even if your code works. **For now, you can ignore the vendor namespace error if you get any when saving your code with VS Code.** 

  

